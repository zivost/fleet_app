/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Dimensions,
    Image,
    TextInput,
    TouchableOpacity
} from 'react-native';
import {
    Colors,
} from '../../src/config'
import Button from '../Button';
import moment from "moment";

let height = Math.round(Dimensions.get('window').height);
let width = Math.round(Dimensions.get('window').width);

const DispatcherInfo: any = (props: any) => {
    return (
        <>
            <View style={local_styles.mainView}>
                <View style={local_styles.carBox}>
                    <Image source={require("assets/images/dispatchPmg.png")} style={local_styles.carStyle} />
                    <Text style={[local_styles.namTxt, { color: Colors.primary }]}>{props.data.name}</Text>


                    <View style={local_styles.contactBox}>
                        <View style={[local_styles.boxView, { backgroundColor: Colors.primary }]}>
                            <Image source={require("assets/images/call.png")} style={local_styles.callImg} />

                            <Text style={[local_styles.phoneTxt, { color: Colors.white }]}>{props.data.phone}</Text>
                            {/* <Text style={[local_styles.phoneTxt, { color: Colors.white }]}>{props.data.phone}</Text> */}
                        </View>

                        <View style={[local_styles.boxView, { backgroundColor: Colors.white }]}>
                            <Image source={require("assets/images/mail.png")} style={local_styles.callImg} />

                            <Text style={[local_styles.phoneTxt, { color: Colors.primary }]}>{props.data.email}</Text>
                        </View>
                    </View>

                </View>
            </View>
        </>
    );
};


const DriverInfo: any = (props: any) => {
    return (
        <>
            <View style={local_styles.mainView}>
                <View style={[local_styles.carBox, { marginBottom: 20 }]}>
                    <Image source={require("assets/images/driver.png")} style={local_styles.carStyle} />
                    <Text style={[local_styles.namTxt, { color: Colors.primary }]}>{props.data.name}</Text>

                    {/* <View style={[local_styles.horiView, { marginTop: 26 }]}>
                        <Text style={[local_styles.namTxt, { color: Colors.gray }]}>License Number:</Text>
                        <Text style={[local_styles.namTxt, { color: Colors.primary, }]}>XXX-XX-XXXXXX</Text>
                    </View> */}

                    <View style={[local_styles.horiView, { marginTop: 10 }]}>
                        <Text style={[local_styles.namTxt, { color: Colors.gray, paddingRight: 20 }]}>Hire Date</Text>
                        <Text style={[local_styles.namTxt, { color: Colors.primary, }]}>{props.data.hire_date}</Text>
                    </View>

                    <View style={[local_styles.horiView, { marginTop: 10 }]}>
                        <Text style={[local_styles.namTxt, { color: Colors.gray, paddingRight: 20 }]}>Tenure:</Text>
                        <Text style={[local_styles.namTxt, { color: Colors.primary, }]}>{props.data.tenure}</Text>
                    </View>

                    <View style={local_styles.contactBox}>
                        <View style={[local_styles.boxView, { backgroundColor: Colors.primary }]}>
                            <Image source={require("assets/images/call.png")} style={local_styles.callImg} />

                            <Text style={[local_styles.phoneTxt, { color: Colors.white }]}>{props.data.phone}</Text>
                            {/* <Text style={[local_styles.phoneTxt, { color: Colors.white }]}>{props.data.phone}</Text> */}
                        </View>

                        <View style={[local_styles.boxView, { backgroundColor: Colors.white }]}>
                            <Image source={require("assets/images/mail.png")} style={local_styles.callImg} />

                            <Text style={[local_styles.phoneTxt, { color: Colors.primary }]}>{props.data.email}</Text>
                            {/* <Text style={[local_styles.phoneTxt, { color: Colors.primary }]}>{props.data.email}</Text> */}
                        </View>
                    </View>
                </View>
                {/* <View style={local_styles.reportBox}>
                    <View style={[local_styles.innerBox, { backgroundColor: '#3CACB4' }]}>
                        <Text style={[local_styles.cosTxt, { color: Colors.gray_shade }]}>Parking Tickets</Text>
                        <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                            <Text style={[local_styles.amntTxt, { color: Colors.white }]}>0</Text>
                        </View>
                    </View>

                    <View style={[local_styles.innerBox, { backgroundColor: '#E05959' }]}>
                        <Text style={[local_styles.cosTxt, { color: Colors.gray_shade }]}>Vans Damaged</Text>
                        <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                            <Text style={[local_styles.amntTxt, { color: Colors.white }]}>0</Text>
                        </View>
                    </View>

                    <View style={[local_styles.innerBox, { backgroundColor: '#AEDA6C' }]}>
                        <Text style={[local_styles.cosTxt, { color: Colors.gray_shade }]}>Rescues Completed</Text>
                        <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                            <Text style={[local_styles.amntTxt, { color: Colors.white }]}>0</Text>
                        </View>
                    </View>
                </View> */}
                {/* <View style={local_styles.perfomanceBox}>
                    <Text style={[local_styles.cosTxt, { color: '#003830' }]}>Performance Summary</Text>
                    <TouchableOpacity>
                        <Image source={require('assets/images/Filter.png')} style={local_styles.filterImg} />
                    </TouchableOpacity>
                </View>

                <View style={local_styles.fliterBox}>
                    <Text style={[local_styles.menuTxt, { color: '#003830' }]}>GAS</Text>
                    <Text style={[local_styles.menuTxt, { color: '#003830' }]}>RESCUES</Text>
                    <Text style={[local_styles.menuTxt, { color: '#003830' }]}>HANDTRUCK</Text>
                    <Text style={[local_styles.menuTxt, { color: '#003830' }]}>PARKING TICKETS</Text>
                    <Text style={[local_styles.menuTxt, { color: '#003830' }]}>DAMAGES</Text>
                    <Text style={[local_styles.menuTxt, { color: '#003830' }]}>CLINLINESS</Text>
                    <Text style={[local_styles.menuTxt, { color: '#003830' }]}>SHIFTS COMPLETED</Text>
                </View> */}

            </View>
        </>
    );
};

const local_styles = StyleSheet.create({
    mainView: {
        marginTop: 28
    },
    carBox: {
        alignItems: 'center',
        marginBottom: 50
    },
    carStyle: {
        height: 100,
        width: 85,
        resizeMode: 'contain'
    },
    namTxt: {
        fontSize: 18,
        fontWeight: '500',
        paddingTop: 15
    },
    contactBox: {
        marginTop: 36,
        alignItems: 'center',
        flexDirection: 'row'
    },
    boxView: {
        height: 141,
        width: 148,
        borderRadius: 8,
        elevation: 2,
        paddingTop: 14,
        alignItems: 'center',
        margin: 10
    },
    callImg: {
        height: 44,
        width: 44,
        resizeMode: 'contain',
        marginBottom: 17
    },
    phoneTxt: {
        fontSize: 12,
        fontWeight: '400',
        paddingBottom: 9,
        textDecorationLine: 'underline'
    },
    horiView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: width - 80,
        // marginLeft: 30
        // flex:1
    },
    reportBox: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    innerBox: {
        height: 105,
        width: 100,
        borderRadius: 8,
        alignItems: 'center',
        paddingTop: 13,
        paddingBottom: 14
    },
    cosTxt: {
        fontSize: 15,
        fontWeight: '500',
        textAlign: 'center'
    },
    increportBox: {
        marginTop: 21,
        width: width - 45,
        height: 71,
        borderRadius: 8,
        backgroundColor: '#81BEC2',
        justifyContent: 'center',
        paddingLeft: 21,
        paddingRight: 31
    },
    amntTxt: {
        fontSize: 26,
        fontWeight: '900',
        // paddingTop: 13
    },
    perfomanceBox: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 53
    },
    filterImg: {
        height: 23,
        width: 23,
        resizeMode: 'contain'
    },
    fliterBox: {
        marginTop: 20
    },
    menuTxt: {
        fontSize: 14,
        fontWeight: '400',
        paddingBottom: 10
    },
});

export {
    DispatcherInfo,
    DriverInfo
};

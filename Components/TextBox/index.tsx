/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Dimensions
} from 'react-native';
import {
    Colors,
} from '../../src/config'

let height = Math.round(Dimensions.get('window').height);
let width = Math.round(Dimensions.get('window').width);

const TextBox: any = (props: any) => {
    return (
        <>
            <View style={[local_styles.mainView, { backgroundColor: props.value ? '#6EF07B' : props.boxName == 'rescue' ? '#FBE417' :Colors.dark_red }]}>
                <Text style={[local_styles.txtStyle, { color: Colors.black_shade }]}>{props.name}</Text>
            </View>
        </>
    );
};

const local_styles = StyleSheet.create({
    mainView: {
        height: 40,
        width: width - 20,
        borderRadius: 18,
        marginTop: 10,
        backgroundColor: '#6EF07B',
        paddingLeft: 17,
        justifyContent: 'center',
        marginBottom: 5
    },
    txtStyle: {
        fontSize: 16,
        fontWeight: '700'
    }
});

export default TextBox;

/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Dimensions,
    TouchableOpacity
} from 'react-native';
import {
    Colors,
} from '../../src/config'
import LinearGradient from 'react-native-linear-gradient';
let height = Math.round(Dimensions.get('window').height);
let width = Math.round(Dimensions.get('window').width);

const EditTextBox: any = (props: any) => {
    return (
        <>
            <View style={[local_styles.mainView]}>
                <LinearGradient
                    colors={['#FFFFFF', '#ABC8FF', '#4F8BFF']}
                    start={{ x: 0, y: 0.5 }}
                    end={{ x: 1, y: 0.5 }}
                    style={local_styles.mainInnerView}>
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between'
                    }}>
                        <Text style={[local_styles.txtStyle, { color: Colors.black_shade, width: 210 }]}>{props.name}</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <View>
                                <TouchableOpacity style={[local_styles.YesBtnView, { marginRight: 10, borderColor: '#6EF07B', backgroundColor: props.activeOpacity == '1' ? '#6EF07B' : 'transparent' }]} onPress={() => props.yesBtn(props.Boxname)}>
                                    <Text style={[local_styles.txtStyle, { color: props.activeOpacity == '1' ? Colors.white : '#6EF07B' }]}>Yes</Text>
                                </TouchableOpacity>
                            </View>
                            <View>
                                <TouchableOpacity style={[local_styles.YesBtnView, { marginRight: 10, borderColor: '#EE0202', backgroundColor: props.activeOpacity == '0' ? '#EE0202' : 'transparent' }]} onPress={() => props.noBtn(props.Boxname)}>
                                    <Text style={[local_styles.txtStyle, { color: props.activeOpacity == '0' ? Colors.white : '#EE0202' }]}>No</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    {props.answerTxt == true ?
                        <View style={{ marginTop: 8 }}>
                            <Text style={[local_styles.txtStyle1, { color: Colors.black_shade }]}>(Yes) Last Updated: {props.milegDate}</Text>
                        </View>
                        : props.answerTxt == false ?
                            <View style={{ marginTop: 8 }}>
                                <Text style={[local_styles.txtStyle1, { color: Colors.black_shade }]}>(No) Last Updated: {props.milegDate}</Text>
                            </View> : null}
                </LinearGradient>
            </View>
        </>
    );
};

const local_styles = StyleSheet.create({
    mainView: {
        height: 80,
        width: width - 20,
        borderRadius: 18,
        marginTop: 10,
        marginBottom: 5
    },
    mainInnerView: {
        height: 80,
        width: width - 20,
        borderRadius: 18,
        marginTop: 10,
        marginBottom: 5,
        paddingLeft: 17,
        paddingRight: 15,
        justifyContent: 'center'
    },
    txtStyle: {
        fontSize: 16,
        fontWeight: '700'
    },
    txtStyle1: {
        fontSize: 12,
        fontWeight: '400'
    },
    YesBtnView: {
        height: 32,
        width: 60,
        borderRadius: 8,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2
    }
});

export default EditTextBox;

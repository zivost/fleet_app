/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useState, useEffect } from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    Dimensions
} from 'react-native';

import {
    Colors,
    Typography,
    setI18nConfig,
    translate,
    Form
} from "../../src/config";

import { TextInput } from 'react-native-gesture-handler';
import { iOSUIKit, human } from 'react-native-typography'
import LinearGradient from 'react-native-linear-gradient';
import PieChart from 'react-native-pie-chart';

let height = Math.round(Dimensions.get('window').height);
let width = Math.round(Dimensions.get('window').width);
const widthAndHeight = 100
const series = [123, 321, 123, 789, 537]
const WeeklyReport: any = (props) => {
    return (
        <View style={local_styles.inspectionBox}>
            <View style={[local_styles.vertcleContainer, { elevation: 6 }]}>
                <LinearGradient
                    colors={['#4c669f', '#3b5998', '#192f6a']}
                    start={{ x: 0, y: 0.5 }}
                    end={{ x: 1, y: 0.5 }}
                    style={local_styles.vertcleContainer}>
                    <View style={{ paddingLeft: 9 }}>
                        <View style={{ marginTop: 19, marginBottom: 10 }}>
                            <Text style={[local_styles.weekTxt, { color: Colors.white, width: 86 }]}>{translate('weeklyInsp')}</Text>
                        </View>
                        <Text style={[local_styles.dateTxt, { color: Colors.white }]}>Till Sat, 31st Jul’ 21</Text>

                        <View style={{ marginTop: 6 }}>
                            <Image style={local_styles.calimage} source={require("assets/images/calender.png")} />
                        </View>

                        <Text style={[local_styles.bigTxt, { color: Colors.white }]}>177</Text>
                    </View>
                </LinearGradient>
            </View>

            <View style={[local_styles.vertcleContainer, { elevation: 6 }]}>
                <LinearGradient
                    colors={['#E9E9E9', '#FFFFFF']}
                    start={{ x: 1, y: 0.5 }}
                    end={{ x: 0, y: 0.5 }}
                    style={local_styles.vertcleContainer}>
                    <View style={{ paddingLeft: 9 }}>
                        <View style={{ marginTop: 19, marginBottom: 10 }}>
                            <Text style={[local_styles.weekTxt, { color: Colors.primary }]}>{translate('report')}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={[local_styles.dateTxt, { color: Colors.primary, fontSize: 12, marginRight: 5 }]}>30th Oct</Text>
                            <Image style={local_styles.calimage} source={require("assets/images/blue_calender.png")} />
                        </View>

                        <View style={{ marginTop: 25, alignItems: 'center' }}>
                            <Image style={local_styles.pdfimage} source={require("assets/images/pdf.png")} />

                            <TouchableOpacity>
                                <Text style={[local_styles.smallTxt, { color: Colors.primary }]}>{translate('download')}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <Text style={[local_styles.smallTxt, { color: Colors.primary }]}>{translate('emailMe')}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </LinearGradient>
            </View>

            <View style={[local_styles.vertcleContainer, { elevation: 5 }]}>
                <LinearGradient
                    colors={['#4c669f', '#3b5998', '#192f6a']}
                    start={{ x: 0, y: 0.5 }}
                    end={{ x: 1, y: 0.5 }}
                    style={local_styles.vertcleContainer}>
                    <View style={{ paddingLeft: 9 }}>
                        <View style={{ marginTop: 19, marginBottom: 10 }}>
                            <Text style={[local_styles.weekTxt, { color: Colors.white, width: 86 }]}>{translate('parkingTicket')}</Text>
                        </View>
                        <Text style={[local_styles.dateTxt, { color: Colors.white }]}>Till Sat, 31st Jul’ 21</Text>

                        <View style={{ marginTop: 6 }}>
                            <Image style={local_styles.calimage} source={require("assets/images/calender.png")} />
                        </View>

                        <Text style={[local_styles.bigTxt, { color: Colors.white, textAlign: 'center', paddingLeft: 0 }]}>25</Text>
                    </View>
                </LinearGradient>
            </View>
        </View>
    );
};


const SummaryBox: any = (props) => {
    let yellow = 0
    let red = 0
    let green = 0
    let black = 0
    let active = 0
    let inActive = 0
    if (props.statsData) {
        if (props.statsData.vehicle) {
            green = props.statsData.vehicle.green
            yellow = props.statsData.vehicle.yellow
            red = props.statsData.vehicle.red
            black = props.statsData.vehicle.black
        }
        if (props.statsData.driver) {
            active = props.statsData.driver.active
            inActive = props.statsData.driver.inactive
        }
    }
    return (
        <View style={local_styles.pirChartBox}>
            <View style={local_styles.boxView}>
                <Text style={[local_styles.statsTxt, { color: Colors.primary, textAlign: 'center' }]}>Total Vehicles</Text>
                <View style={{ alignItems: 'center', height: 100, marginTop: 10 }}>
                    {props.statsData.vehicle && props.statsData.vehicle.green >= 0 && props.statsData.vehicle.yellow >= 0 &&
                        props.statsData.vehicle.red >= 0 && props.statsData.vehicle.red >= 0 ?
                        <PieChart
                            widthAndHeight={widthAndHeight}
                            series={[green, yellow, red, black]}
                            sliceColor={['#86D06C', '#5555C9', '#CB5757', '#000000']}
                        /> :
                        <PieChart
                            widthAndHeight={widthAndHeight}
                            series={[1]}
                            sliceColor={['#86D06C']}
                        />}

                </View>
                <View style={{ marginTop: 10 }}>
                    <View style={local_styles.hori_Box}>
                        <Text style={[local_styles.flagTxt, { color: Colors.primary }]}>Active Vehicles</Text>
                        <Text style={[local_styles.smallTxt1, { color: Colors.primary }]}>{green}</Text>
                    </View>
                    <View style={local_styles.hori_Box}>
                        <Text style={[local_styles.flagTxt, { color: Colors.primary }]}>Moderately Damaged</Text>
                        <Text style={[local_styles.smallTxt1, { color: Colors.primary }]}>{yellow}</Text>
                    </View>
                    <View style={local_styles.hori_Box}>
                        <Text style={[local_styles.flagTxt, { color: Colors.primary }]}>Severely Damaged</Text>
                        <Text style={[local_styles.smallTxt1, { color: Colors.primary }]}>{red}</Text>
                    </View>
                    <View style={local_styles.hori_Box}>
                        <Text style={[local_styles.flagTxt, { color: Colors.primary }]}>In shop</Text>
                        <Text style={[local_styles.smallTxt1, { color: Colors.primary }]}>{black}</Text>
                    </View>
                </View>
            </View>
            <View style={local_styles.borderLine} />
            <View style={local_styles.boxView}>
                <Text style={[local_styles.statsTxt, { color: Colors.primary, textAlign: 'center' }]}>Total Drivers</Text>
                <View style={{ alignItems: 'center', height: 100, marginTop: 10 }}>
                    {props.statsData.driver && props.statsData.driver.active >= 0 && props.statsData.driver.inactive >= 0 ?
                        <PieChart
                            widthAndHeight={widthAndHeight}
                            series={[active, inActive]}
                            sliceColor={['#86D06C', '#CB5757']}
                        /> :
                        <PieChart
                            widthAndHeight={widthAndHeight}
                            series={[1]}
                            sliceColor={['#86D06C']}
                        />}
                </View>
                <View style={{ marginTop: 10 }}>
                    <View style={local_styles.hori_Box}>
                        <Text style={[local_styles.flagTxt, { color: Colors.primary }]}>Active</Text>
                        <Text style={[local_styles.smallTxt1, { color: Colors.primary }]}>{active}</Text>
                    </View>
                    <View style={local_styles.hori_Box}>
                        <Text style={[local_styles.flagTxt, { color: Colors.primary }]}>Inactive</Text>
                        <Text style={[local_styles.smallTxt1, { color: Colors.primary }]}>{inActive}</Text>
                    </View>
                </View>
            </View>
        </View>
    );
};

const local_styles = StyleSheet.create({
    vertcleContainer: {
        width: 100,
        height: 172,
        borderRadius: 10,
        shadowColor: "rgba(124, 124, 124, 0.16)",
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 10,
        shadowOpacity: 3,
        backgroundColor: 'red',
        elevation: 5
    },
    inspectionBox: {
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        // paddingRight:20
    },
    weekTxt: {
        fontWeight: '500',
        fontSize: 14,
        lineHeight: 19,
    },
    dateTxt: {
        fontSize: 8,
        fontWeight: '400'
    },
    flagTxt: {
        fontSize: 7,
        fontWeight: '500'
    },
    calimage: {
        height: 11,
        width: 11,
        resizeMode: 'contain'
    },
    bigTxt: {
        fontSize: 34,
        fontWeight: '500',
        paddingTop: 23,
        paddingLeft: 10
    },
    pdfimage: {
        height: 22,
        width: 18,
        marginBottom: 10
    },
    smallTxt: {
        fontSize: 12,
        fontWeight: '500',
        textDecorationLine: 'underline',
        marginBottom: 3
    },
    statsTxt: {
        fontSize: 12,
        fontWeight: '500'
    },
    smallTxt1: {
        fontSize: 10,
        fontWeight: '500'
    },
    pirChartBox: {
        height: 230,
        width: 360,
        borderRadius: 10,
        backgroundColor: '#fff',
        elevation: 3,
        marginTop: 10,
        flexDirection: 'row',
        // alignItems: 'center',
        justifyContent: 'space-between',
        padding: 10
    },
    boxView: {
        width: width * 156 / 414,
        // backgroundColor:'red',
        // alignItems: 'center',
        paddingRight: 10,
        paddingLeft: 10
    },
    borderLine: {
        borderWidth: 0.3,
        height: 205,
        borderColor: '#000000'
    },
    hori_Box: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 5
    }
});

export { WeeklyReport, SummaryBox };
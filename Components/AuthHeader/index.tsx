/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useState, useEffect } from "react";
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image,
    Dimensions,
    TouchableOpacity
} from 'react-native';

import { TextInput } from 'react-native-gesture-handler';
import { iOSUIKit, human } from 'react-native-typography'
import Button from '../Button/index'

const AuthHeader: any = (props) => {
    return (
        <View style={local_styles.profileHead}>
            <View style={local_styles.profileOval}>
                <Image style={local_styles.profileImage} source={require("assets/images/user.png")} />
            </View>
            <View>
                <Button styles={{ width: 139 }} name={props.name} />
            </View>
        </View>
    );
};

const local_styles = StyleSheet.create({
    profileHead: {
        flexDirection: "row",
        paddingTop: 30,
        paddingLeft: 28,
        paddingRight: 28,
        paddingBottom: 40,
        justifyContent: "space-between"
    },
    profileOval: {
        width: 40,
        height: 40,
        borderRadius: 40 / 2,
        overflow: "hidden"
    },
    profileImage: {
        resizeMode: "cover"
    },
});

export default AuthHeader;

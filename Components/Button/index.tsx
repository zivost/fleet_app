/**
 *
 * @format
 * @flow strict-local
 */

 import React from 'react';
 import type { FC } from 'react';
 import { useState, useEffect } from "react";
 import {
     SafeAreaView,
     ScrollView,
     StatusBar,
     StyleSheet,
     Text,
     useColorScheme,
     View,
     Image,
     Dimensions,
     TouchableOpacity
 } from 'react-native';

import { TextInput } from 'react-native-gesture-handler';
import { iOSUIKit, human } from 'react-native-typography'
import {Form} from "../../src/config/index"
 
const Button: any = (props) => {
     return (
        <View>
            <TouchableOpacity style={[Form.Button, props.styles]} onPress={()=> props.onPress(props.param)}>
                <Text style={local_styles.errText}>{props.name}</Text>
            </TouchableOpacity>
        </View>
     );
};

const local_styles = StyleSheet.create({
    errText:{
        ...human.subhead,
        color: "#fff",
        textAlign:"center"

    }
});

export default Button;
 
/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useState, useEffect } from "react";
import {
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    FlatList
} from 'react-native';
import { iOSUIKit, human } from 'react-native-typography'
import {
    Colors,
    Typography,
    setI18nConfig,
    translate,
    Form
} from "../../src/config";
import LinearGradient from 'react-native-linear-gradient';
import Button from "../../Components/Button/index"

let height = Math.round(Dimensions.get('window').height);
let width = Math.round(Dimensions.get('window').width);




const InspectBox: any = (props) => {
    return (
        <View style={local_styles.mainBox}>
            <LinearGradient
                colors={['#FFFFFF', '#ABC8FF', '#4F8BFF']}
                start={{ x: 0, y: 0.5 }}
                end={{ x: 1, y: 0.5 }}
                style={local_styles.innerBox}>
                <View>
                    <Text style={[local_styles.headTxt, { color: Colors.light_gray }]}>{props.name}</Text>
                </View>

                <View style={{ marginTop: 10 }}>
                    {props.imgData && props.imgData.length > 0 ?
                        <FlatList
                            data={props.imgData}
                            renderItem={
                                ({ item, index }) => {
                                    return (
                                        <View style={local_styles.imgStyle}>
                                            <Image style={local_styles.damagImg} source={{ uri: item }} />
                                        </View>
                                    )
                                }
                            }
                            horizontal={true}
                            showsHorizontalScrollIndicator={false} /> :
                        <View style={local_styles.imgStyle}>
                            {/* <Image style={local_styles.imgStyle} source={{ uri: props.imgData }} /> */}
                        </View>}
                </View>

                <View style={{ marginTop: 15 }}>
                    <Text style={[local_styles.smallTxt, { color: Colors.gray }]}>{translate('lstCapt')}</Text>
                </View>
            </LinearGradient>
        </View>
    );
};

const renderImg = ({ item, index }) => {
    return (
        <View style={local_styles.imgStyle}>
            <Image source={{ uri: props.imgData }} />
        </View>
    )
}

const local_styles = StyleSheet.create({
    mainBox: {
        width: width - 20,
        height: 229,
        borderRadius: 18,
        elevation: 5,
        marginTop: 20,
        marginBottom: 5
    },
    innerBox: {
        width: width - 20,
        height: 229,
        borderRadius: 18,
        padding: 17
    },
    headTxt: {
        fontSize: 16,
        fontWeight: '500'
    },
    imgStyle: {
        height: 132,
        width: 101,
        backgroundColor: '#D5D5D5',
        marginRight: 15
    },
    damagImg: {
        height: 132,
        width: 101,
        marginRight: 15
    },
    smallTxt: {
        fontSize: 11,
        fontWeight: '400'
    }
});

export default InspectBox;

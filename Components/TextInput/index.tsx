/**
 *
 * @format
 * @flow strict-local
 */

 import React from 'react';
 import type { FC } from 'react';
 import {
     StyleSheet,
     Text,
     View,
     Image,
 } from 'react-native';

import { TextInput } from 'react-native-gesture-handler';
import { iOSUIKit, human } from 'react-native-typography'
import { Colors } from '../../src/config';

const TextInputField: any = (props:any) => {
     return (
         <View>
            <TextInput
                placeholder={props.placeholder}
                placeholderTextColor={Colors.light_gray} 
                style={props.styles}
                defaultValue={props.value}
                keyboardType={props.keyboardType}
                textContentType={props.textContentType}
                secureTextEntry={props.secureTextEntry}
                onChangeText={(text)=> props.onChangeText(text)}
            />
            {props.error ? 
                <View style={local_styles.errView}>
                    <Image source={require("assets/images/errorIcon.png")}/>
                    <Text style={local_styles.errText}>{props.errorMsg}</Text>
                </View> : 
            null}
        </View>
     );
};

const local_styles = StyleSheet.create({
    errView:{
        flexDirection:"row",
        alignItems:"center",
        marginTop:4
    },
    errText:{
        ...human.caption2Object,
        color: "#E54343",
        marginLeft:8
    }
});

export default TextInputField;
 
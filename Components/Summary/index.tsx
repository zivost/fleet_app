/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useState, useEffect } from "react";
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    FlatList
} from 'react-native';

import {
    Colors,
    Typography,
    setI18nConfig,
    translate,
    Form
} from "../../src/config";

import { iOSUIKit, human } from 'react-native-typography'
import LinearGradient from 'react-native-linear-gradient';


let height = Math.round(Dimensions.get('window').height);
let width = Math.round(Dimensions.get('window').width);
const VehicleDamages: any = (props) => {
    let length = 0
    if (props.damageMetaData && props.damageMetaData.total) {
        length = props.damageMetaData.total
    }
    return (
        <View style={local_styles.inspectionBox}>
            <LinearGradient
                colors={['#E9E9E9', '#FFFFFF']}
                start={{ x: 1, y: 0.5 }}
                end={{ x: 0, y: 0.5 }}
                style={local_styles.vertcleContainer}>
                <View style={local_styles.rowView}>
                    <View style={local_styles.headView}>
                        <Text style={[local_styles.dateTxt, { color: Colors.primary, marginRight: 5 }]}>{translate('vehicleDamage')}</Text>
                        {/* <Image style={local_styles.calimage} source={require("assets/images/blue_calender.png")} /> */}
                        <Text style={[local_styles.smallTxt, { color: Colors.primary }]}>({length})</Text>
                    </View>
                    <Text style={[local_styles.smallTxt, { color: Colors.primary, textDecorationLine: 'underline', padding: 10 }]} onPress={props.navToSeeall}>{translate('seeAll')}</Text>
                </View>

                <View style={local_styles.vehicleBox}>
                    <FlatList
                        data={props.damageData}
                        showsHorizontalScrollIndicator={false}
                        renderItem={(item) => {
                            return (
                                <View key={'damage_' + item.index}>
                                    {item.index != (props.length - 1) ? (
                                        <TouchableOpacity style={local_styles.vehicleImage} onPress={() => props.navToCarProfile(item.item)}>
                                            <Image source={require("assets/images/car_img.png")} style={local_styles.carImg} />
                                            <View style={[local_styles.dotView, { backgroundColor: item.item.flag }]} />
                                            <View style={{ alignItems: 'center', marginTop: 9 }}>
                                                <Text style={[local_styles.userTxt, { color: Colors.primary, marginTop: 3, textAlign: 'center' }]}>{item.item.vehicle.name}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    ) : (
                                        <TouchableOpacity onPress={props.addDamage} >
                                            <View style={local_styles.addView}>
                                                <Image source={require("assets/images/plus.png")} />

                                            </View>
                                            <Text style={[local_styles.userTxt, { color: Colors.primary }]}>{translate('addVehicle')}</Text>
                                        </TouchableOpacity>
                                    )}
                                </View>
                            )
                        }}
                        horizontal={true} />
                </View>
            </LinearGradient>

        </View>
    );
};


const Vehicles: any = (props) => {
    let length = 0;
    if (props.vehicleMetaData && props.vehicleMetaData.total) {
        length = props.vehicleMetaData.total
    }
    return (
        <View style={local_styles.inspectionBox}>
            <LinearGradient
                colors={['#4c669f', '#3b5998', '#192f6a']}
                start={{ x: 0, y: 0.5 }}
                end={{ x: 1, y: 0.5 }}
                style={local_styles.vertcleContainer}>
                <View style={local_styles.rowView}>
                    <View style={local_styles.headView}>
                        <Text style={[local_styles.dateTxt, { color: Colors.white, marginRight: 5 }]}>{translate('vehicles')}</Text>
                        <Text style={[local_styles.smallTxt, { color: Colors.white }]}>({length})</Text>
                    </View>
                    <Text style={[local_styles.smallTxt, { color: Colors.white, textDecorationLine: 'underline', padding: 10 }]} onPress={props.navToSeeall}>{translate('seeAll')}</Text>
                </View>


                <View style={local_styles.vehicleBox}>
                    <FlatList
                        data={props.vehicleData}
                        showsHorizontalScrollIndicator={false}
                        renderItem={(item) => {
                            let flags = item.item.flags;
                            let flag = ''
                            if (item.item.flags) {
                                flag = (flags.indexOf('red') >= 0 ? 'red' : flags.indexOf('yellow') >= 0 ? 'yellow' : 'green');
                            }
                            return (
                                <View >
                                    {item.index != (props.length - 1) ? (
                                        <TouchableOpacity key={'vehicle_' + item.index} style={local_styles.vehicleImage} onPress={() => props.navToCarProfile(item.item, "Vehicle")}>
                                            <Image source={require("assets/images/car_img.png")} style={local_styles.carImg} />
                                            <View style={[local_styles.dotView, { backgroundColor: flag }]} />
                                            <View style={{ alignItems: 'center', marginTop: 9 }}>
                                                <Text style={[local_styles.userTxt, { color: Colors.white }]}>{item.item.name}</Text>
                                                {/* <Text style={[local_styles.userTxt, { color: Colors.primary, marginTop: 3 }]}>Front Damage</Text> */}
                                            </View>
                                        </TouchableOpacity>
                                    ) : (
                                        <TouchableOpacity key={'vehicle_' + item.index} style={{ alignItems: 'center' }} onPress={props.addVehicle}>
                                            <View style={local_styles.addView}>
                                                <Image source={require("assets/images/plus.png")} />

                                            </View>
                                            <Text style={[local_styles.userTxt, { color: Colors.white, textAlign: 'center' }]}>Add vehicle</Text>
                                        </TouchableOpacity>
                                    )}
                                </View>
                            )
                        }}

                        horizontal={true} />
                </View>
            </LinearGradient>

        </View>
    );
};

// dispather list
const Dispatcher: any = (props) => {
    let role = props.role
    return (
        <View style={local_styles.inspectionBox}>
            <LinearGradient
                colors={['#E9E9E9', '#FFFFFF']}
                start={{ x: 1, y: 0.5 }}
                end={{ x: 0, y: 0.5 }}
                style={local_styles.vertcleContainer}>
                <View style={local_styles.rowView}>
                    <View style={local_styles.headView}>
                        <Text style={[local_styles.dateTxt, { color: Colors.primary, marginRight: 5 }]}>{translate('dispatcher')}</Text>
                        <Text style={[local_styles.smallTxt, { color: Colors.primary }]}>({props.length - 1})</Text>
                    </View>
                    <Text style={[local_styles.smallTxt, { color: Colors.primary, textDecorationLine: 'underline', padding: 10 }]} onPress={props.navToSeeall}>{translate('seeAll')}</Text>
                </View>

                <View style={local_styles.vehicleBox}>
                    <FlatList
                        data={props.dispatchData}
                        showsHorizontalScrollIndicator={false}
                        renderItem={(item) => {
                            return (
                                <View key={'dispatcher_' + item.index}>
                                    {item.index != (props.length - 1) ? (
                                        <TouchableOpacity style={local_styles.vehicleImage} onPress={() => props.navToCarProfile(item.item, "Dispatchers")}>
                                            <Image source={require("assets/images/user.png")} style={local_styles.carImg} />

                                            <View style={{ alignItems: 'center', marginTop: 12 }}>
                                                <Text style={[local_styles.userTxt, { color: Colors.primary }]}>{item.item.name}</Text>
                                                {/* <Text style={[local_styles.userTxt, { color: Colors.primary, marginTop: 3 }]}>Front Damage</Text> */}
                                            </View>
                                        </TouchableOpacity>
                                    ) : (
                                        role === 'dispatcher' ? null :
                                            (
                                                <TouchableOpacity style={{ alignItems: 'center' }} onPress={props.addDispatcher}>
                                                    <View style={local_styles.addView}>
                                                        <Image source={require("assets/images/plus.png")} />

                                                    </View>
                                                    <Text style={[local_styles.userTxt, { color: Colors.primary, textAlign: 'center' }]}>{translate('addDispatcher')}</Text>
                                                </TouchableOpacity>
                                            )
                                    )}
                                </View>
                            )
                        }}
                        horizontal={true} />
                </View>
            </LinearGradient>

        </View>
    );
};

//Driver List
const Drivers: any = (props) => {
    let length = 0
    if (props.driverMetaData && props.driverMetaData.total) {
        length = props.driverMetaData.total
    }
    return (
        <View style={[local_styles.inspectionBox, { marginBottom: 20 }]}>
            <LinearGradient
                colors={['#4c669f', '#3b5998', '#192f6a']}
                start={{ x: 0, y: 0.5 }}
                end={{ x: 1, y: 0.5 }}
                style={local_styles.vertcleContainer}>
                <View style={local_styles.rowView}>
                    <View style={local_styles.headView}>
                        <Text style={[local_styles.dateTxt, { color: Colors.white, marginRight: 5 }]}>Drivers </Text>
                        <Text style={[local_styles.smallTxt, { color: Colors.white }]}>({length})</Text>
                    </View>
                    <Text style={[local_styles.smallTxt, { color: Colors.white, textDecorationLine: 'underline' }]} onPress={props.navToSeeall}>{translate('seeAll')}</Text>
                </View>

                <View style={local_styles.vehicleBox}>
                    <FlatList
                        data={props.driverData}
                        showsHorizontalScrollIndicator={false}
                        renderItem={(item) => {
                            return (
                                <View key={'driver_' + item.index}>
                                    {item.index != (props.length - 1) ? (
                                        <TouchableOpacity style={local_styles.vehicleImage} onPress={() => props.navToCarProfile(item.item, "Drivers")}>
                                            <Image source={require("assets/images/user.png")} style={local_styles.carImg} />

                                            <View style={{ alignItems: 'center', marginTop: 9, width: 40 }}>
                                                <Text style={[local_styles.userTxt, { color: Colors.white }]}>{item.item.name}</Text>
                                                {/* <Text style={[local_styles.userTxt, { color: Colors.primary, marginTop: 3 }]}>Front Damage</Text> */}
                                            </View>
                                        </TouchableOpacity>
                                    ) : (
                                        <TouchableOpacity style={{ alignItems: 'center', marginRight: 10 }} onPress={props.addDriver}>
                                            <View style={local_styles.addView}>
                                                <Image source={require("assets/images/plus.png")} />

                                            </View>
                                            <Text style={[local_styles.userTxt, { color: Colors.white, textAlign: 'center' }]}>Add Driver</Text>
                                        </TouchableOpacity>
                                    )}
                                </View>
                            )
                        }}
                        horizontal={true} />
                </View>
            </LinearGradient>

        </View>
    );
};


const local_styles = StyleSheet.create({
    vertcleContainer: {
        width: width - 15,
        height: 172,
        borderRadius: 10,
        shadowColor: "rgba(124, 124, 124, 0.16)",
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 10,
        shadowOpacity: 3,
        marginTop: 24,
        paddingLeft: 20,
        paddingRight: 20
    },
    inspectionBox: {
        alignItems: 'center',
        elevation: 5,
        shadowColor: "rgba(124, 124, 124, 0.16)",
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 10,
        shadowOpacity: 3,
    },
    rowView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 17,
        marginBottom: 6
    },
    headView: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    dateTxt: {
        fontSize: 15,
        fontWeight: '500'
    },
    calimage: {
        height: 14,
        width: 13,
        marginRight: 10
    },
    smallTxt: {
        fontSize: 12,
        fontWeight: '500'
    },
    vehicleBox: {
        marginTop: 25,
        marginLeft: 10
    },
    vehicleImage: {
        alignItems: 'center',
        marginRight: 14,
        marginLeft: 10
    },
    carImg: {
        height: 40,
        width: 40,
        resizeMode: 'contain'
    },
    userTxt: {
        fontSize: 9,
        fontWeight: '500',
        textAlign: 'center'
    },
    addView: {
        height: 40,
        width: 40,
        borderRadius: 20,
        backgroundColor: '#C4C4C4',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 10
    },
    dotView: {
        height: 8,
        width: 8,
        borderRadius: 8 / 2,
        position: 'absolute',
        top: 0,
        right: 0
    }
});

export {
    VehicleDamages,
    Vehicles,
    Dispatcher,
    Drivers
};

/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useState, useEffect } from "react";
import {
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    FlatList,
    Alert
} from 'react-native';
import { iOSUIKit, human } from 'react-native-typography'
import sortBy from 'lodash/sortBy';
import {
    Colors,
    Typography,
    setI18nConfig,
    translate,
    Form
} from "../../src/config";
import LinearGradient from 'react-native-linear-gradient';
import Button from "../../Components/Button/index"

let height = Math.round(Dimensions.get('window').height);
let width = Math.round(Dimensions.get('window').width);
const data = ['car', 'dispatch', 'accident', 'damage']

const renderReportList = (item: any, props: any) => {
    let sel_driver: any = {};
    let inspection_status = "Not yet started";
    let flags = item.item.flags;
    let flag = (flags.indexOf('red') >= 0 ? 'red' : flags.indexOf('yellow') >= 0 ? 'yellow' : 'green');
    let forms: any = {};
    for (let i in props.inspection_forms) {
        forms[props.inspection_forms[i].vehicle] = props.inspection_forms[i];
    }
    let percentage = item.item.completion || 0
    sel_driver = item.item.driver || 0
    for (let i in props.drivers) {
        if (props.drivers[i].id == item.item.selected_driver && item.item.selected_driver) {
            sel_driver = props.drivers[i]
        }
        if (forms[parseInt(item.item.id)]) {
            if (props.drivers[i].id == item.item.driver && item.item.driver) {
                sel_driver = props.drivers[i]
            }
        }
    }
    if (percentage > 0) {
        inspection_status = "In progress"
    }
    if (percentage >= 100) {
        inspection_status = "Completed"
    }
    return (
        <View key={'inspect_' + item.index} style={local_styles.mainView}>
            <View style={[local_styles.horiContainer, { width: `${percentage}%` }]}>

            </View>
            <View style={local_styles.rowView}>
                <View style={{ flexDirection: 'row', marginLeft: 0 }}>
                    <Image source={require("assets/images/car_img.png")} style={local_styles.carImg} />
                    <View style={[local_styles.dotColor, { backgroundColor: flag }]} />
                </View>

                <View style={[local_styles.nameBox, { marginLeft: 40 }]}>
                    <Text style={[local_styles.nameTxt, { color: percentage < 30 ? Colors.gray : Colors.white }]}>{item.item.name}</Text>
                    <TouchableOpacity style={local_styles.assignBox} onPress={() => {
                        props.setActiveVehicle(item.item.id)
                        console.log("setting vehicle - ", item.item.id)
                        props.opnDriverModal()
                    }}>
                        <Text style={local_styles.assignTxt}>{sel_driver.name ? sel_driver.name : "Assign Driver"}</Text>
                        <Image source={require("assets/images/downArrow.png")} style={local_styles.arrowImg} />
                    </TouchableOpacity>
                </View>

                <View style={{ alignItems: 'center', marginLeft: 40 }}>
                    <TouchableOpacity style={local_styles.btnView} onPress={() => {
                        if (item.item.selected_driver) {
                            if (flag != 'red' || props.is_checkedout === true) {
                                if (props.is_checkedout === true) {
                                    props.navToInspectReport(props.inspection_forms, item.item.selected_driver, item.item.name)
                                } else {
                                    props.navToinspect(item.item.id, item.item.selected_driver, item.item.name,props.inspection_forms,)
                                }
                            } else {
                                Alert.alert(
                                    '',
                                    'This vehicle is damaged, please resolve the damage before dispatching. ',
                                    [
                                        {
                                            text: 'Ok', onPress: () =>
                                            (
                                                console.log
                                            )
                                        },
                                    ],
                                    { cancelable: false },
                                );
                            }
                        } else {
                            Alert.alert(
                                '',
                                'Please select a driver.',
                                [
                                    {
                                        text: 'Ok', onPress: () =>
                                        (
                                            console.log
                                        )
                                    },
                                ],
                                { cancelable: false },
                            );
                        }
                    }}>
                        <Text style={{ color: Colors.white }}>{props.is_checkedout === true ? 'View' : 'Inspect'}</Text>
                    </TouchableOpacity>
                    <Text style={local_styles.progressTxt}>{inspection_status}</Text>
                </View>
            </View>
        </View>
    )
}


const ReportCard: any = (props: any) => {
    let limit_data = [];
    let c = 1;
    // sort
    let clist = sortBy(props.data, (o) => { return o.completion })
    // let vlist = sortBy(action.payload.data, (o) => { return sort_index.indexOf(o.flag) })
    for (let i in clist) {
        // if (c <= props.van_count) {
            limit_data.push(clist[i])
        // }
        // c++
    }
    return (
        <View>
            <FlatList
                data={limit_data}
                renderItem={(item) => renderReportList(item, props)} />
        </View>
    );
};

const local_styles = StyleSheet.create({
    mainView: {
        width: width - 14,
        height: 80,
        borderRadius: 17,
        backgroundColor: '#fff',
        elevation: 5,
        marginBottom: 10,
        overflow: 'hidden'
        // shadowColor: "rgba(0, 0, 0, 0.16)",
        // shadowOffset: {
        //     width: 0,
        //     height: 3
        // },
        // shadowRadius: 10,
        // shadowOpacity: 3,
    },
    horiContainer: {
        width: "50%",
        height: 80,
        backgroundColor: 'rgba(44, 171, 152, 0.86)',
    },
    rowView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 10,
        paddingBottom: 10,
        position: 'absolute',
        zIndex: 999
    },
    carImg: {
        height: 40,
        width: 40
    },
    dotColor: {
        height: 7,
        width: 7,
        borderRadius: 5,
        backgroundColor: 'red',
        marginLeft: -4
    },
    nameBox: {
        // flexDirection:'row'
    },
    dropBox: {
        // flexDirection:'row'
        height: 30,
        width: 200
    },
    nameTxt: {
        fontSize: 13,
        fontWeight: '700'
    },
    assignBox: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: 9,
        paddingRight: 9,
        marginTop: 5,
        width: 118,
        height: 24,
        borderRadius: 14,
        borderWidth: 1,
        borderColor: '#5B5B5B'
    },
    arrowImg: {
        height: 5,
        width: 9,
        resizeMode: 'contain'
    },
    assignTxt: {
        fontSize: 8,
        fontWeight: '400',
        color: '#5E5E5E'
    },
    btnStyle: {
        width: 86
    },
    progressTxt: {
        fontSize: 12,
        fontWeight: '500',
        color: '#5E5E5E'
    },
    dropdown: {
        backgroundColor: 'white',
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        marginTop: 20,
        width: 200
    },
    shadow: {
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,
        elevation: 2,
    },
    item: {
        // paddingVertical: 17,
        alignItems: 'center',
    },
    textItem: {
        flex: 1,
        fontSize: 16,
    },
    btnView: {
        height: 40,
        width: 80,
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#4F8BFF'
    }
});

export default ReportCard;

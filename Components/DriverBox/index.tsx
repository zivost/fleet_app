/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Dimensions,
    Image,
    TouchableOpacity
} from 'react-native';
import {
    Colors,
} from '../../src/config'

let height = Math.round(Dimensions.get('window').height);
let width = Math.round(Dimensions.get('window').width);

const DriverBox: any = (props: any) => {
    let name = props.data.name
    return (
        <>
            <TouchableOpacity style={local_styles.mainView} onPress={() => props.navToprofile(props.data)}>
                {props.screen == 'Drivers' ? <Image source={require("assets/images/driver.png")} style={local_styles.driverImg} />
                    : props.screen == 'Vehicle' ? (<Image source={require("assets/images/car_img.png")} style={[local_styles.driverImg, {}]} />) :
                        props.screen == 'Dispatchers' ? <Image source={require("assets/images/dispatchPmg.png")} style={[local_styles.driverImg, {}]} /> : null}
                <View style={local_styles.middleBox}>
                    <Text style={[local_styles.driName, { color: Colors.primary, width: 78, textAlign: 'center' }]} numberOfLines={1}>{name}</Text>

                    <View style={local_styles.horiContainer}>
                        <Text style={[local_styles.cllTxt, { color: Colors.gray }]}>Call</Text>
                        <Text style={[local_styles.cllTxt, { color: Colors.gray }]}>Email</Text>
                    </View>
                </View>
            </TouchableOpacity>
        </>
    );
};

const DamageBox: any = (props: any) => {
    let name = props.data.vehicle.name
    let details = props.data.detail
    let backgroundColor = props.data.flag
    return (
        <>
            <TouchableOpacity style={local_styles.mainView} onPress={() => props.navToDetails(props.data)}>
                <Image source={require("assets/images/car_img.png")} style={local_styles.carImg} />
                <View style={[local_styles.dotView, { backgroundColor: backgroundColor }]} />
                <View style={local_styles.middleBox}>
                    <Text style={[local_styles.driName, { color: Colors.primary, width: 40, textAlign: 'center' }]} numberOfLines={1}>{name}</Text>

                    <View style={local_styles.horiContainer}>
                        <Text style={[local_styles.damgTxt, { color: Colors.primary }]}>{details}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        </>
    );
};

const local_styles = StyleSheet.create({
    mainView: {
        alignItems: 'center',
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 26
    },
    driverImg: {
        height: 100,
        width: 82,
        resizeMode: 'contain'
    },
    middleBox: {
        marginTop: 13,
        alignItems: 'center'
    },
    horiContainer: {
        marginTop: 3,
        // flexDirection: 'row',
        alignItems: 'center',
        // justifyContent: 'space-between',
        width: 40
    },
    driName: {
        fontSize: 12,
        fontWeight: '400'
    },
    cllTxt: {
        fontSize: 8,
        fontWeight: '500',
        textDecorationLine: 'underline'
    },
    carImg: {
        height: 40,
        width: 40,
        resizeMode: 'contain'
    },
    damgTxt: {
        fontWeight: '400',
        fontSize: 8,
        textAlign: 'center'
    },
    dotView: {
        height: 8,
        width: 8,
        borderRadius: 8 / 2,
        position: 'absolute',
        top: 0,
        right: 0
    }
});

export { DriverBox, DamageBox };

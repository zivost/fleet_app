/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useState, useEffect } from "react";
import {
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    FlatList,
    ActivityIndicator
} from 'react-native';
import { iOSUIKit, human } from 'react-native-typography'
import {
    Colors,
    Typography,
    setI18nConfig,
    translate,
    Form
} from "../../src/config";
import LinearGradient from 'react-native-linear-gradient';
import Button from "../../Components/Button/index"

let height = Math.round(Dimensions.get('window').height);
let width = Math.round(Dimensions.get('window').width);




const EditInspectBox: any = (props) => {
    let image = props.frontView ? props.frontView : '';
    let prevImage = props.prevView ? props.prevView : '';
    return (
        <View style={local_styles.mainBox}>
            <LinearGradient
                colors={['#FFFFFF', '#ABC8FF', '#4F8BFF']}
                start={{ x: 0, y: 0.5 }}
                end={{ x: 1, y: 0.5 }}
                style={local_styles.innerBox}>
                <View style={local_styles.topHoriView}>
                    <Text style={[local_styles.headTxt, { color: Colors.light_gray }]}>{props.name}</Text>

                    <TouchableOpacity style={local_styles.btnView} onPress={() => props.openDamage(props.boxName, props.name, image)}>
                        <Text style={[local_styles.txtStyle, { color: Colors.white }]}>{image ? '1 Image Added' : 'Add new damage'}</Text>
                    </TouchableOpacity>
                </View>

                <View style={{ marginTop: 22, flexDirection: 'row' }}>
                    {/* <FlatList
                        data={[1, 2]}
                        renderItem={renderImg}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false} /> */}
                    <View>
                        {prevImage ?
                            <View>
                                <Image
                                    source={{ uri: prevImage }}
                                    style={local_styles.imgStyle} />
                                <View style={{ marginTop: 12 }}>
                                    <Text style={local_styles.smallTxt}>Last captured :</Text>
                                </View>
                                <View style={{ marginTop: 5 }}>
                                    <Text style={local_styles.smallTxt}>{props.milegDate}</Text>
                                </View>
                            </View> :
                            null}

                    </View>
                    {image ?
                        <Image
                            source={{ uri: image }}
                            style={{ width: 101, height: 132, borderWidth: 1 }} /> :
                        <TouchableOpacity style={local_styles.imgStyle} onPress={() => props.selectImage(props.boxName)}>

                        </TouchableOpacity>
                    }

                </View>


                <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'flex-end' }}>
                    {props.loading && props.indicatorName == props.boxName ?
                        (<View style={[local_styles.btnView, { backgroundColor: '#4F8BFF' }]}>
                            <ActivityIndicator size="small" color={Colors.white} />
                        </View>) : <TouchableOpacity style={[local_styles.btnView, { backgroundColor: '#4F8BFF' }]} onPress={() => { props.updateInspection(); props.setIndicator(props.boxName) }}>
                            <Text style={{ color: Colors.white, fontSize: 12, fontWeight: '500' }}>Save</Text>
                        </TouchableOpacity>}
                </View>
            </LinearGradient>
        </View>
    );
};

const renderImg = ({ item, index }) => {
    return (
        <View style={local_styles.imgStyle}></View>
    )
}


const EditHandtruckBox: any = (props) => {
    let image = props.frontView ? props.frontView : '';
    let prevImage = props.prevView ? props.prevView : '';
    return (
        <View style={local_styles.mainBox}>
            <LinearGradient
                colors={['#FFFFFF', '#ABC8FF', '#4F8BFF']}
                start={{ x: 0, y: 0.5 }}
                end={{ x: 1, y: 0.5 }}
                style={local_styles.innerBox}>
                <View style={local_styles.topHoriView}>
                    <Text style={[local_styles.headTxt, { color: Colors.light_gray, width: 180, height: 40 }]}>{props.name}</Text>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <TouchableOpacity style={[local_styles.YesBtnView, { marginRight: 10, borderColor: '#6EF07B', backgroundColor: props.selctBox == '1' ? '#6EF07B' : 'transparent' }]} onPress={() => props.yesBtn(props.boxName)}>
                            <Text style={[local_styles.txtStyle, { color: props.selctBox == '1' ? Colors.white : '#6EF07B' }]}>Yes</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[local_styles.YesBtnView, { borderColor: '#EE0202', backgroundColor: props.selctBox == '0' ? '#EE0202' : 'transparent' }]} onPress={() => props.noBtn(props.boxName)}>
                            <Text style={[local_styles.txtStyle, { color: props.selctBox == '0' ? Colors.white : '#EE0202' }]}>No</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={{ marginTop: 15, flexDirection: 'row' }}>
                    {/* <FlatList
                        data={[1, 2]}
                        renderItem={renderImg}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false} /> */}
                    <View>
                        {prevImage ?
                            <View>
                                <Image
                                    source={{ uri: prevImage }}
                                    style={local_styles.imgStyle} />
                                <View style={{ marginTop: 12 }}>
                                    <Text style={local_styles.smallTxt}>Last captured :</Text>
                                </View>
                                <View style={{ marginTop: 5 }}>
                                    <Text style={local_styles.smallTxt}>{props.milegDate}</Text>
                                </View>
                            </View>
                            :
                            null}

                    </View>
                    {image || props.selctBox != '1' ?
                        <Image
                            source={{ uri: image }}
                            style={{ width: 101, height: 132, borderWidth: 1 }} /> :
                        <TouchableOpacity style={local_styles.imgStyle} onPress={() => props.selectImage(props.boxName)}>

                        </TouchableOpacity>
                    }
                </View>

                {/* <View style={{ marginTop: 10 }}>
                    <Text style={local_styles.smallTxt}>Last captured</Text>
                </View> */}
                <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'flex-end' }}>
                    {props.loading && props.indicatorName == props.boxName ?
                        (<View style={[local_styles.btnView, { backgroundColor: '#4F8BFF' }]}>
                            <ActivityIndicator size="small" color={Colors.white} />
                        </View>) : <TouchableOpacity style={[local_styles.btnView, { backgroundColor: '#4F8BFF' }]} onPress={() => { props.updateInspection(); props.setIndicator(props.boxName) }}>
                            <Text style={{ color: Colors.white, fontSize: 12, fontWeight: '500' }}>Save</Text>
                        </TouchableOpacity>}
                </View>
            </LinearGradient>
        </View >
    );
};

const local_styles = StyleSheet.create({
    mainBox: {
        width: width - 20,
        height: 285,
        borderRadius: 18,
        elevation: 5,
        marginTop: 20,
        marginBottom: 5
    },
    innerBox: {
        width: width - 20,
        height: 285,
        borderRadius: 18,
        padding: 17
    },
    headTxt: {
        fontSize: 16,
        fontWeight: '500'
    },
    imgStyle: {
        height: 132,
        width: 101,
        backgroundColor: '#D5D5D5',
        marginRight: 15
    },
    smallTxt: {
        fontSize: 10,
        fontWeight: '400',
        color: '#161616'
    },
    topHoriView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    btnView: {
        height: 30,
        width: 103,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 20,
        backgroundColor: '#EE0202'
    },
    txtStyle: {
        fontSize: 10,
        fontWeight: '500'
    },
    YesBtnView: {
        height: 32,
        width: 60,
        borderRadius: 8,
        borderWidth: 2,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export {
    EditInspectBox,
    EditHandtruckBox
};

/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Modal,
    TouchableOpacity,
    TextInput
} from 'react-native';
import {
    Colors,
    Typography,
    setI18nConfig,
    translate,
    Form
} from '../../src/config'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import Button from '../Button';
import TextInputField from '../TextInput';
const SubInspectModal: any = (props: any) => {
    return (
        <>
            <View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={props.checkoutModal}>
                    <View style={local_styles.ModalStyle}>
                        <View style={[local_styles.warnmainModalView]}>
                            <View style={{ marginTop: 31, marginBottom: 34 }}>
                                <Text style={local_styles.headTxt}>Submit the Inspection?</Text>
                            </View>

                            {/* <TouchableOpacity style={local_styles.checkouTBox}>
                                <Text style={[local_styles.headTxt, { color: '#4F8BFF' }]}>Checkout & Next inspection</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={local_styles.checkouTBox}>
                                <Text style={[local_styles.headTxt, { color: '#4F8BFF' }]}>Checkout & Report creation</Text>
                            </TouchableOpacity> */}

                            <TouchableOpacity style={local_styles.checkouTBox} onPress={() => props.checkoutInspection('checkout')}>
                                <Text style={[local_styles.headTxt, { color: '#A7A8A9' }]}>Checkout & Exit </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
            </View>
        </>
    );
};


const GasModal: any = (props: any) => {
    return (
        <>
            <View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={props.gasModal}>
                    <View style={local_styles.ModalStyle}>
                        <View style={[local_styles.warnmainModalView]}>
                            {props.gasList.map((item, index) => {
                                return (
                                    <TouchableOpacity key={"gas_" + index} style={local_styles.gasSection} onPress={() => props.setGasTank(item)}>
                                        <Text>{item}</Text>
                                    </TouchableOpacity>
                                )
                            })}
                        </View>
                    </View>
                </Modal>
            </View>
        </>
    );
};

const CameraModal: any = (props: any) => {
    return (
        <>
            <View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={props.camModal}>
                    <View style={local_styles.ModalStyle}>
                        <View style={[local_styles.warnmainModalView]}>
                            <TouchableOpacity style={local_styles.gasSection} onPress={() => props.setCamera('camera')}>
                                <Text>Open Camera</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={local_styles.gasSection} onPress={() => props.setCamera('library')}>
                                <Text>Chooes From Library</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
            </View>
        </>
    );
};

const ParkingModal: any = (props: any) => {
    return (
        <>
            <View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={props.parkingModal}>
                    <View style={local_styles.ModalStyle}>
                        {/* <TouchableOpacity onPress={props.closeParkingModal}> */}
                        <View style={[local_styles.warnmainModalView]}>
                            <TouchableOpacity style={local_styles.camSection} onPress={() => props.setCamera('parkingTicket')}>
                                <Image source={require("assets/images/camera.png")} />
                                <Text style={{ paddingLeft: 10 }}>Click Photo</Text>
                            </TouchableOpacity>
                            <TextInput
                                placeholder='$$'
                                style={local_styles.textInputStyle}
                                onChangeText={(text) => props.setParkingTicket(text)} />
                            <View style={{ alignItems: 'center' }}>
                                <TouchableOpacity style={local_styles.rescueBtn} onPress={() => props.subTicket('parking')}>
                                    <Text style={{ paddingLeft: 10 }}>submit</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        {/* </TouchableOpacity> */}
                    </View>
                </Modal>
            </View>
        </>
    );
};


const RescueModal: any = (props: any) => {
    return (
        <>
            <View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={props.rescueModal}>
                    <View style={local_styles.ModalStyle}>
                        <View style={[local_styles.warnmainModalView,]}>
                            <TextInput
                                placeholder='Resone why?'
                                style={local_styles.textInputStyle}
                                onChangeText={(text) => props.setRescueTxt(text)} />
                            <View style={{ alignItems: 'center' }}>
                                <TouchableOpacity style={local_styles.rescueBtn} onPress={() => props.subTicket('rescue')}>
                                    <Text style={{ paddingLeft: 10 }}>submit</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        </>
    );
};

const WorkInProgressModal: any = (props: any) => {
    return (
        <>
            <View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={props.workProgrssModal}
                    onRequestClose={props.closeModal}>
                    <View style={local_styles.ModalStyle}>
                        <View style={[local_styles.warnmainModalView, { alignItems: 'center' }]}>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 26, fontWeight: '500', color: Colors.red }}>Under construction</Text>
                            </View>

                            <TouchableOpacity style={local_styles.okBtn} onPress={props.closeModal}>
                                <Text>
                                    Ok
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
            </View>
        </>
    );
};

const MenuModal: any = (props: any) => {
    return (
        <>
            <View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={props.menuModal}>
                    <TouchableOpacity style={local_styles.editStyle} onPress={props.closeMenuModal}>
                        <View style={[local_styles.editmainModalView]}>
                            <TouchableOpacity style={{ alignItems: 'center' }} onPress={props.navToEdit}>
                                <Text style={{ fontSize: 14, fontWeight: '400', color: Colors.gray }}>Edit</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ alignItems: 'center', marginTop: 15 }}>
                                <Text style={{ fontSize: 14, fontWeight: '400', color: Colors.error }}>Delete</Text>
                            </TouchableOpacity>
                        </View>
                    </TouchableOpacity>
                </Modal>
            </View>
        </>
    );
};

const EmailWarnModal: any = (props: any) => {
    return (
        <>
            <View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={props.warnModal}>
                    <View style={local_styles.ModalStyle}>
                        <View style={[local_styles.warnmainModalView, { alignItems: 'center' }]}>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 26, fontWeight: '500', color: Colors.red, textAlign: 'center' }}>Please Eneter Email Id and Password</Text>
                            </View>
                            <TouchableOpacity style={[local_styles.okBtn, { marginTop: 20 }]} onPress={props.closeModal}>
                                <Text>
                                    Ok
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
            </View>
        </>
    );
};

const DamageModal: any = (props: any) => {
    return (
        <>
            <View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={props.openDamageModal}>
                    <View style={local_styles.ModalStyle}>
                        <View style={local_styles.damagemainModalView}>
                            <TouchableOpacity style={{ alignItems: 'flex-end' }} onPress={props.closeDamageModal}>
                                <FontAwesomeIcon icon={faTimes} size={20} color={Colors.primary} />
                            </TouchableOpacity>
                            <View style={{ alignItems: 'center', marginTop: 10 }}>
                                {props.damageImg ? (
                                    <TouchableOpacity onPress={() => props.imgUpload('damagePic')}>
                                        <Image source={{ uri: props.damageImg }} style={local_styles.imgBox} />
                                    </TouchableOpacity>
                                ) : (<TouchableOpacity style={local_styles.imgBox} onPress={() => props.imgUpload('damagePic')}>
                                    <Image source={require("assets/images/plus.png")} style={local_styles.plusImg} />
                                </TouchableOpacity>)}
                            </View>
                            <View style={{ width: "80%" }}>
                                <TextInputField
                                    placeholder={'Damage details'}
                                    styles={[Form.TextInput, local_styles.textInput]}
                                    onChangeText={(text: string) => {
                                        props.setDamageDetails(text)
                                    }}
                                />
                            </View>
                            <View style={local_styles.damageBox}>
                                <Text style={[local_styles.damgTxt, { color: Colors.light_gray }]}>Choose damage severity</Text>
                            </View>
                            <View style={local_styles.damageInnerBox}>
                                {props.damageList.map((item, index) => {
                                    return (
                                        <TouchableOpacity key={'damage_' + index} style={local_styles.boxView} onPress={() => props.selctGamage(item.backgroungColor)}>
                                            <View style={[local_styles.ovalView, { backgroundColor: item.backgroungColor }]}>
                                                {props.damageFlag == item.backgroungColor ?
                                                    <Image source={require("assets/images/check.png")} style={local_styles.checkImg} /> : null}
                                            </View>

                                            <View style={{ marginTop: 10, width: 50 }}>
                                                <Text style={[local_styles.smallTxt, { color: Colors.gray }]}>{item.name}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    )
                                })}
                            </View>
                            <View style={local_styles.bottomBoxInner}>
                                <Button styles={local_styles.btnStyle} name={'Add Damage'} onPress={props.uploadDamage} />
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        </>
    )
}

const DamageFilterModal: any = (props: any) => {
    return (
        <>
            <View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={props.filterModal}>
                    <TouchableOpacity onPress={props.closeMenuModal} style={local_styles.editStyle}>
                        <View style={[local_styles.damagefilter]}>
                            {props.damges.map((item, index) => {
                                return (
                                    <TouchableOpacity key={'damag_flag' + index} style={local_styles.filterBox} onPress={() => props.selctFlag(item.color, item.damage_name)}>
                                        <View style={[local_styles.flagView, { backgroundColor: item.color }]} />
                                        <Text style={[local_styles.damgTxt, { color: Colors.gray_txt }]}>{item.damage_name}</Text>
                                    </TouchableOpacity>
                                )
                            })}
                        </View>
                    </TouchableOpacity>
                </Modal>
            </View>
        </>
    );
};

const local_styles = StyleSheet.create({
    ModalViewView: {
        position: "absolute",
        width: "100%",
        height: "100%",
        top: 0,
        bottom: 0,
        backgroundColor: "#ffffff91",
        zIndex: 2,
        justifyContent: "center"
    },
    ModalStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)'
    },
    editStyle: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)',
        alignItems: 'flex-end',
    },
    editmainModalView: {
        backgroundColor: "#fff",
        width: "20%",
        marginRight: 35,
        marginTop: 10,
        borderRadius: 10,
        padding: 10,
    },
    damagefilter: {
        backgroundColor: "#fff",
        width: "50%",
        // height:200,
        marginRight: 35,
        marginTop: 60,
        borderRadius: 10,
        padding: 10,
        // alignItems: 'center'
    },
    warnmainModalView: {
        backgroundColor: "#fff",
        width: "80%",
        borderRadius: 10,
        // padding: 20,
        justifyContent: 'center',
        alignSelf: "center",
        padding: 10,
        paddingBottom: 40
    },
    checkouTBox: {
        alignItems: 'flex-end',
        marginBottom: 18
    },
    headTxt: {
        fontSize: 16,
        fontWeight: '400',
        color: '#383838'
    },
    gasSection: {
        alignItems: 'center',
        margin: 5
    },
    gasTxt: {
        fontSize: 16,
        fontWeight: '500',
        color: Colors.gray_txt
    },
    camSection: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    textInputStyle: {
        borderBottomWidth: 1,
        borderColor: '#D5D5D5'
    },
    rescueBtn: {
        height: 40,
        width: 160,
        borderRadius: 20,
        backgroundColor: '#4F8BFF',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20
    },
    okBtn: {
        marginTop: 10,
        height: 40,
        width: 80,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 20,
        backgroundColor: '#4F8BFF',

    },
    damagemainModalView: {
        backgroundColor: "#fff",
        width: "80%",
        borderRadius: 10,
        padding: 20,
        paddingBottom: 40,
        height: 570,
        justifyContent: 'flex-start'
    },
    dmgTxt: {
        fontSize: 15,
        fontWeight: '400'
    },
    imgBox: {
        height: 122,
        width: 122,
        borderRadius: 122 / 2,
        backgroundColor: '#D5D5D5',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 14
    },
    plusImg: {
        height: 31,
        width: 31,
        resizeMode: 'contain'
    },
    textInput: {
        marginTop: 10,
    },
    damageInnerBox: {
        marginTop: 31,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 59
    },
    boxView: {
        height: 95,
        width: 75,
        borderRadius: 9,
        backgroundColor: '#C4C4C4',
        alignItems: 'center'
    },
    ovalView: {
        height: 29,
        width: 29,
        borderRadius: 29 / 2,
        marginTop: 18,
        alignItems: 'center',
        justifyContent: 'center'
    },
    checkImg: {
        height: 10,
        width: 14,
        resizeMode: 'contain'
    },
    damageBox: {
        marginTop: 42
    },
    damgTxt: {
        fontSize: 14,
        fontWeight: '400'
    },
    bottomBoxInner: {
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        // marginTop: 50,
        width: "100%"
    },
    btnStyle: {
        width: 138
    },
    smallTxt: {
        fontSize: 10,
        fontWeight: '300',
        paddingBottom: 3,
        textAlign:'center'
    },
    filterBox: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingBottom: 10
    },
    flagView: {
        height: 13,
        width: 13,
        borderRadius: 13 / 2,
        marginRight: 12
    }
});

export {
    SubInspectModal,
    GasModal,
    CameraModal,
    ParkingModal,
    RescueModal,
    WorkInProgressModal,
    MenuModal,
    EmailWarnModal,
    DamageModal,
    DamageFilterModal
};

/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Dimensions,
    Image,
    TextInput
} from 'react-native';
import {
    Colors,
} from '../../src/config'
import Button from '../Button';
import moment from "moment";

let height = Math.round(Dimensions.get('window').height);
let width = Math.round(Dimensions.get('window').width);

const VehicleInfo: any = (props: any) => {
    let dateRec = ''
    let dateInsur = ''
    let dateInsurExpy = ''

    if (props.data) {
        dateRec = moment(props.data.recieved).format("YYYY-MM-DD")
        dateInsur = moment(props.data.insured).format("YYYY-MM-DD")
        dateInsurExpy = moment(props.data.insurance_expires).format("YYYY-MM-DD")
    }

    return (
        <>
            <View style={local_styles.mainView}>
                <View style={local_styles.carBox}>
                    <Image source={require("assets/images/car_img.png")} style={local_styles.carStyle} />
                </View>
                <View style={local_styles.reportBox}>
                    <View style={[local_styles.innerBox, { backgroundColor: '#3CACB4' }]}>
                        <Text style={[local_styles.cosTxt, { color: Colors.gray_shade }]}>Cost of Repairs</Text>
                        <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                            <Text style={[local_styles.amntTxt, { color: Colors.white }]}>$0</Text>
                        </View>
                    </View>

                    <View style={[local_styles.innerBox, { backgroundColor: '#E05959' }]}>
                        <Text style={[local_styles.cosTxt, { color: Colors.gray_shade }]}>Damages</Text>
                        <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                            <Text style={[local_styles.amntTxt, { color: Colors.white }]}>0</Text>
                        </View>
                    </View>

                    <View style={[local_styles.innerBox, { backgroundColor: '#AEDA6C' }]}>
                        <Text style={[local_styles.cosTxt, { color: Colors.gray_shade }]}>Preventive Maintenance</Text>
                        <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                            <Text style={[local_styles.amntTxt, { color: Colors.white }]}>0</Text>
                        </View>
                    </View>
                </View>

                {/* <View style={local_styles.increportBox}>
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between'
                    }}>
                        <View style={local_styles.horiView}>
                            <Image source={require("assets/images/pdf.png")} style={local_styles.pdfImg} />
                            <Text style={local_styles.reportTxt}>Incident Report Download</Text>
                        </View>
                        <View style={{ alignItems: 'flex-end' }}>
                            <Text style={[local_styles.uploadTxt, { color: Colors.white }]}>Upload</Text>
                            <Text style={[local_styles.uploadTxt, { color: Colors.white }]}>Download</Text>
                        </View>
                    </View>
                </View>

                <View style={local_styles.inputBox}>
                    <Text style={local_styles.BigTxt}>Estimated Repair Cost </Text>
                    <TextInput
                        placeholder='$ 500.69'
                        style={local_styles.inputStyle} />
                </View>

                <View style={[local_styles.inputBox, { marginTop: 15 }]}>
                    <Text style={local_styles.BigTxt}>Actual Repair Cost</Text>
                    <TextInput
                        placeholder='$ 500.69'
                        style={local_styles.inputStyle} />
                </View> */}

                <View style={{ marginTop: 73 }}>
                    <View style={local_styles.menuBox}>
                        <Text style={local_styles.menuTxt}>Van Number </Text>
                        <Text style={[local_styles.ansTxt, { color: Colors.primary }]}>{props.data.name}</Text>
                    </View>

                    <View style={local_styles.menuBox}>
                        <Text style={local_styles.menuTxt}>VIN-17</Text>
                        <Text style={[local_styles.ansTxt, { color: Colors.primary }]}>{props.data.vin}</Text>
                    </View>

                    <View style={local_styles.menuBox}>
                        <Text style={local_styles.menuTxt}>License No. </Text>
                        <Text style={[local_styles.ansTxt, { color: Colors.primary }]}>{props.data.plate}</Text>
                    </View>

                    <View style={local_styles.menuBox}>
                        <Text style={local_styles.menuTxt}>Registered State</Text>
                        <Text style={[local_styles.ansTxt, { color: Colors.primary }]}>{props.data.state}</Text>
                    </View>

                    <View style={local_styles.menuBox}>
                        <Text style={local_styles.menuTxt}>Gas Card </Text>
                        <Text style={[local_styles.ansTxt, { color: Colors.primary }]}>{props.data.gas_card}</Text>
                    </View>

                    <View style={local_styles.menuBox}>
                        <Text style={local_styles.menuTxt}>Make</Text>
                        <Text style={[local_styles.ansTxt, { color: Colors.primary }]}>{props.data.make}</Text>
                    </View>

                    <View style={local_styles.menuBox}>
                        <Text style={local_styles.menuTxt}>Trim</Text>
                        <Text style={[local_styles.ansTxt, { color: Colors.primary }]}>{props.data.trim}</Text>
                    </View>

                    <View style={local_styles.menuBox}>
                        <Text style={local_styles.menuTxt}>Year</Text>
                        <Text style={[local_styles.ansTxt, { color: Colors.primary }]}>{props.data.year}</Text>
                    </View>

                    <View style={local_styles.menuBox}>
                        <Text style={local_styles.menuTxt}>Date Received</Text>
                        <Text style={[local_styles.ansTxt, { color: Colors.primary }]}>{dateRec}</Text>
                    </View>

                    <View style={local_styles.menuBox}>
                        <Text style={local_styles.menuTxt}>Date Insured</Text>
                        <Text style={[local_styles.ansTxt, { color: Colors.primary }]}>{dateInsur}</Text>
                    </View>

                    <View style={local_styles.menuBox}>
                        <Text style={local_styles.menuTxt}>Insurance Expires</Text>
                        <Text style={[local_styles.ansTxt, { color: Colors.primary }]}>{dateInsurExpy}</Text>
                    </View>

                    <View style={local_styles.menuBox}>
                        <Text style={local_styles.menuTxt}>Flag</Text>
                        <Text style={[local_styles.ansTxt, { color: Colors.primary }]}>{props.data.flag}</Text>
                    </View>
                </View>
                {/* <View style={local_styles.bottomBoxInner}>
                    <Button styles={local_styles.btnStyle} name={"Add Damage"} onPress={props.uploadDamage} />
                </View> */}
            </View>
        </>
    );
};

const local_styles = StyleSheet.create({
    mainView: {
        marginTop: 28
    },
    carBox: {
        alignItems: 'center',
        marginBottom: 50
    },
    carStyle: {
        height: 116,
        width: 154,
        resizeMode: 'contain'
    },
    reportBox: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    innerBox: {
        height: 105,
        width: 100,
        borderRadius: 8,
        alignItems: 'center',
        paddingTop: 13,
        paddingBottom: 14
    },
    cosTxt: {
        fontSize: 15,
        fontWeight: '500',
        textAlign: 'center'
    },
    increportBox: {
        marginTop: 21,
        width: width - 45,
        height: 71,
        borderRadius: 8,
        backgroundColor: '#81BEC2',
        justifyContent: 'center',
        paddingLeft: 21,
        paddingRight: 31
    },
    pdfImg: {
        height: 30,
        width: 37,
        resizeMode: 'contain',
        marginRight: 13
    },
    horiView: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    reportTxt: {
        width: 150,
        fontWeight: '500',
        fontSize: 13,
        color: '#003830'
    },
    uploadTxt: {
        fontSize: 12,
        fontWeight: '500',
        textDecorationLine: 'underline',
        textAlign: 'left',
        paddingTop: 7
    },
    amntTxt: {
        fontSize: 26,
        fontWeight: '900',
        // paddingTop: 13
    },
    inputBox: {
        marginTop: 49,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    inputStyle: {
        borderBottomWidth: 1,
        borderColor: '#A7A8A9',
        width: 150,
        height: 40
    },
    BigTxt: {
        fontSize: 16,
        fontWeight: '500',
        color: '#3E4347'
    },
    menuBox: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 38
    },
    menuTxt: {
        fontSize: 16,
        fontWeight: '400',
        color: '#3E4347'
    },
    ansTxt: {
        fontSize: 16,
        fontWeight: '500'
    },
    bottomBoxInner: {
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        // marginTop: 50,
        width: "100%",
        marginBottom: 20
    },
    btnStyle: {
        width: 138
    },
});

export default VehicleInfo;

/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useState, useEffect } from "react";
import moment from "moment";
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image,
    Dimensions,
    TouchableOpacity
} from 'react-native';

import { TextInput } from 'react-native-gesture-handler';
import { iOSUIKit, human } from 'react-native-typography'
import {
    Colors,
    Typography,
    setI18nConfig,
    translate,
    Form
} from "../../src/config";
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faPen } from '@fortawesome/free-solid-svg-icons'
import Button from '../Button';
const Header: any = (props: any) => {
    return (
        <View style={local_styles.mainView}>
            <TouchableOpacity style={local_styles.headerView} onPress={() => props.goBack(props.param)}>
                <Image source={require("assets/images/backbtn.png")} />
                <Text style={local_styles.headerTitle}>{props.title}</Text>
                {props.damageCount ? <Text style={[local_styles.nameTxt, { color: Colors.primary, paddingLeft: 10 }]}>({props.damageCount})</Text> : null}
            </TouchableOpacity>
            {props.dateView ? (
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View>
                        <Text style={[local_styles.dateTxt, { color: Colors.gray_txt }]}>{moment(props.date, 'YYYY-MM-DD').format("Do MMM YY")}</Text>
                        <Text style={[local_styles.dayTxt, { color: Colors.gray_txt }]}>{moment(props.date, 'YYYY-MM-DD').format("dddd")}</Text>
                    </View>
                    <View>
                        {props.filterValue ?
                            <TouchableOpacity onPress={props.openFilterModal}>
                                <Image source={require("assets/images/filter_fill.png")} style={local_styles.filterImg} />
                            </TouchableOpacity> :
                            <TouchableOpacity onPress={props.openFilterModal}>
                                <Image source={require("assets/images/filter.png")} style={local_styles.filterImg} />
                            </TouchableOpacity>}
                    </View>
                </View>
            ) : null}
            {props.inspectForm ? (
                <View style={local_styles.rightView}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 10 }}>
                        <Image style={local_styles.carStyle} source={require('assets/images/inspectCar.png')} />
                        <Text style={[local_styles.nameTxt, { color: Colors.primary, width: 50 }]}>{props.VehicleName}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image style={local_styles.carStyle} source={require('assets/images/user.png')} />
                        <Text style={[local_styles.nameTxt, { color: Colors.primary, fontSize: 10, width: 50, fontWeight: '300' }]}>{props.sel_driver.name}</Text>
                    </View>
                </View>
            ) : null}
            {props.profileMenu ? (
                <TouchableOpacity style={local_styles.rightView} onPress={props.navToEditScreen}>
                    <FontAwesomeIcon icon={faPen} size={20} color={Colors.gray} />
                </TouchableOpacity>
            ) : null}
            {props.addBtn ? (
                props.role != 'dispatcher' ?
                    <TouchableOpacity style={local_styles.rightView} onPress={props.navToEditScreen}>
                        <Button styles={local_styles.btnStyle} name={`${'Add'} ${props.btnTxt}`} onPress={props.navScree} />
                    </TouchableOpacity>
                    : null) : null}
            {props.editMenu ? (
                <TouchableOpacity style={local_styles.rightView} onPress={props.navToEditScreen}>
                    <Image style={local_styles.dotStyle} source={require('assets/images/dot_3.png')} />
                </TouchableOpacity>
            ) : null}
        </View>
    );
};

const local_styles = StyleSheet.create({
    headerView: {
        flexDirection: "row",
        alignItems: "center"
    },
    headerTitle: {
        ...human.title3Object,
        color: "#4F8BFF",
        marginLeft: 13
    },
    rightView: {
        // flex: 1,
        // alignItems: 'flex-end'
    },
    mainView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    dateTxt: {
        fontSize: 13,
        fontWeight: '400'
    },
    dayTxt: {
        fontSize: 9,
        fontWeight: '400'
    },
    // rightView: {
    //     flex:1,
    //     alignItems:'flex-end'
    // },
    // mainView: {
    //     flexDirection:'row',
    //     alignItems:'center',
    //     marginTop:31,
    //     marginRight:31,
    //     marginLeft:17
    // },
    carStyle: {
        height: 14,
        width: 21,
        resizeMode: 'contain',
        marginRight: 12
    },
    nameTxt: {
        fontSize: 12,
        fontWeight: '500'
    },
    btnStyle: {
        paddingLeft: 10,
        paddingRight: 10
    },
    dotStyle: {
        height: 20,
        width: 20,
        resizeMode: 'contain'
    },
    filterImg: {
        height: 16,
        width: 17,
        marginLeft: 11
    }
});

export default Header;

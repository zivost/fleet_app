/**
 *
 * @format
 * @flow strict-local
 */

 import React from 'react';
 import type { FC } from 'react';
 import {
     StyleSheet,
     Text,
     View,
     Image,
     ActivityIndicator
} from 'react-native';
import {
    Colors,
} from '../../src/config'
const Loader: any = (props:any) => {
     return (
         <>
            {props.loading ?
                <View style={local_styles.loaderView}>
                    <ActivityIndicator size="large" color={Colors.primary} />
                </View> : 
                null
            }
        </>
     );
};

const local_styles = StyleSheet.create({
    loaderView:{
        position:"absolute",
        width:"100%",
        height:"100%",
        top:0,
        bottom:0,
        backgroundColor:"#ffffff91",
        zIndex:2,
        justifyContent:"center"
    },
});

export default Loader;
 
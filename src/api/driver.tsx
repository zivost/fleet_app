// import Libraries
import axios from 'axios';
import Environment from "../../environment";

let instance = axios.create({
    baseURL: Environment.USER_URL,
    headers: { 'Content-Type': 'application/json' }
});

const log_prefix = "[API_AUTH_ZIVOST]"

const VERBOSE = Environment.LOGS;

const messages = {
    start: "Starting request.",
    end: "End request.",
    non200x: "The request was made and the server responded with a status code that falls out of the range of 2xx.",
    noResp: "The request was made but no response was received.",
    badConfig: "Something happened in setting up the request that triggered an Error.",
}

const genericError = {
    message: "Something Went Wrong",
    status: 500
}

function logger(message: any, verbose: any, type: any) {
    if (verbose) {
        if (type === "error") {
            message = `[ERROR] ${message}`;
            type = "info"
        }
        if (process.env.NODE_ENV !== "production") {
            // console[type](message);
        }
    }
}

function defaultCatch(error: any, resolve: any) {
    if (error.response) {
        logger(`${log_prefix} ${messages.non200x}`, VERBOSE, "error");
        logger(`${log_prefix} evaluating(error.response) ${error.response}`, VERBOSE, "error");

        resolve(error.response)
    } else if (error.request) {
        logger(`${log_prefix} ${messages.noResp}`, VERBOSE, "error");
        logger(`${log_prefix} evaluating(http.ClientRequest) ${error.request}`, VERBOSE, "error");

        resolve(genericError)
    } else {
        logger(`${log_prefix} ${messages.badConfig}`, VERBOSE, "error");
        logger(`${log_prefix} evaluating(config) ${error.config}`, VERBOSE, "error");
        logger(`${log_prefix} evaluating(axios.instance) ${instance}`, VERBOSE, "error");

        resolve(genericError)
    }
}


export default class Driver {

    static getDriverList(values: any) {
        let payload = values;
        instance.defaults.headers.common['x-firebase-token'] = payload.sessionToken;
        logger(`${log_prefix} ${messages.start} login`, VERBOSE, "info");
        return new Promise(resolve => {
            instance.get('drivers/v1/drivers?' + payload.data)
                .then(function (response) {
                    resolve(response)
                })
                .catch(function (error) {
                    defaultCatch(error, resolve)
                });
        });
    }

    static getDispatchersList(values: any) {
        let payload = values;
        instance.defaults.headers.common['x-firebase-token'] = payload.sessionToken;
        logger(`${log_prefix} ${messages.start} login`, VERBOSE, "info");
        return new Promise(resolve => {
            instance.get('users/v1/dispatchers?' + payload.data)
                .then(function (response) {
                    resolve(response)
                })
                .catch(function (error) {
                    defaultCatch(error, resolve)
                });
        });
    }

    static getDamagesList(values: any) {
        let payload = values;
        instance.defaults.headers.common['x-firebase-token'] = payload.sessionToken;
        logger(`${log_prefix} ${messages.start} login`, VERBOSE, "info");
        return new Promise(resolve => {
            instance.get('damages/v1/damages?' + payload.data)
                .then(function (response) {
                    resolve(response)
                })
                .catch(function (error) {
                    defaultCatch(error, resolve)
                });
        });
    }

    static getVehiclesList(values: any) {
        let payload = values;
        instance.defaults.headers.common['x-firebase-token'] = payload.sessionToken;
        logger(`${log_prefix} ${messages.start} login`, VERBOSE, "info");
        return new Promise(resolve => {
            instance.get('vehicles/v1/vehicles?' + payload.data)
                .then(function (response) {
                    resolve(response)
                    console.log(response)
                })
                .catch(function (error) {
                    defaultCatch(error, resolve)
                });
        });
    }

    static uploadDamageImg(values: any) {
        let payload = values;
        instance.defaults.headers.common['x-firebase-token'] = payload.sessionToken;
        instance.defaults.headers.common['Content-Type'] = "multipart/form-data";
        logger(`${log_prefix} ${messages.start} login`, VERBOSE, "info");

        let photo = {};
        if (payload.data.file.uri) {
            let splitURI = payload.data.file.uri.split('/');
            photo = {
                uri: payload.data.file.uri,
                name: splitURI[splitURI.length - 1],
                type: payload.data.file.type
            }
        }
        let formData = new FormData();
        formData.append("file", photo);
        return new Promise(resolve => {
            instance.put('damages/v1/images', formData)
                .then(function (response) {
                    resolve(response)
                })
                .catch(function (error) {
                    defaultCatch(error, resolve)
                });
        });

    }

    static setInspectionCount(values: any) {
        let payload = values;
        instance.defaults.headers.common['x-firebase-token'] = payload.sessionToken;
        logger(`${log_prefix} ${messages.start} login`, VERBOSE, "info");
        return new Promise(resolve => {
            instance.post('inspections/v1/inspection', payload.data)
                .then(function (response) {
                    resolve(response)
                })
                .catch(function (error) {
                    defaultCatch(error, resolve)
                });
        });
    }
    
    static getInspectionCount(values: any) {
        let payload = values;
        instance.defaults.headers.common['x-firebase-token'] = payload.sessionToken;
        logger(`${log_prefix} ${messages.start} login`, VERBOSE, "info");
        return new Promise(resolve => {
            instance.get(`inspections/v1/inspection/date/${payload.data.date}`)
                .then(function (response) {
                    resolve(response)
                })
                .catch(function (error) {
                    defaultCatch(error, resolve)
                });
        });
    }
    
    static getInspectionForms(values: any) {
        let payload = values;
        instance.defaults.headers.common['x-firebase-token'] = payload.sessionToken;
        logger(`${log_prefix} ${messages.start} login`, VERBOSE, "info");
        return new Promise(resolve => {
            instance.get(`inspections/v1/inspection_forms/date/${payload.data.date}`)
                .then(function (response) {
                    resolve(response)
                })
                .catch(function (error) {
                    defaultCatch(error, resolve)
                });
        });
    }

    static updateInspection(values: any) {
        let payload = values;
        instance.defaults.headers.common['x-firebase-token'] = payload.sessionToken;
        logger(`${log_prefix} ${messages.start} login`, VERBOSE, "info");
        return new Promise(resolve => {
            instance.post('inspections/v1/inspection_forms/date/' + payload.date, payload.data)
                .then(function (response) {
                    resolve(response)
                })
                .catch(function (error) {
                    defaultCatch(error, resolve)
                });
        });
    }
    
    static inspectionCheckout(values: any) {
        let payload = values;
        instance.defaults.headers.common['x-firebase-token'] = payload.sessionToken;
        logger(`${log_prefix} ${messages.start} login`, VERBOSE, "info");
        console.log(`inspections/v1/inspection/date/${payload.date}/checkout`,payload)
        return new Promise(resolve => {
            instance.patch(`inspections/v1/inspection/date/${payload.data.date}/checkout`)
                .then(function (response) {
                    resolve(response)
                })
                .catch(function (error) {
                    defaultCatch(error, resolve)
                });
        });
    }

    static addDamage(values: any) {
        let payload = values;
        instance.defaults.headers.common['x-firebase-token'] = payload.sessionToken;
        logger(`${log_prefix} ${messages.start} login`, VERBOSE, "info");
        if (!payload.id) {
            return new Promise(resolve => {
                instance.post('damages/v1/damage', payload.data).then(function (response) {
                    resolve(response)
                }).catch(function (error) {
                    defaultCatch(error, resolve)
                });
            });
        }else{
            return new Promise(resolve => {
                instance.patch(`damages/v1/damage/${payload.id}`, payload.data).then(function (response) {
                    resolve(response)
                }).catch(function (error) {
                    defaultCatch(error, resolve)
                });
            });
        }
    }

    // create new driver
    static createDriver(values: any) {
        let payload = values;
        instance.defaults.headers.common['x-firebase-token'] = payload.sessionToken;
        logger(`${log_prefix} ${messages.start} login`, VERBOSE, "info");
        if (!payload.id) {
            return new Promise(resolve => {
                instance.post('drivers/v1/driver', payload.data).then(function (response) {
                    resolve(response)
                }).catch(function (error) {
                    defaultCatch(error, resolve)
                });
            });
        }else{
            return new Promise(resolve => {
                instance.patch(`drivers/v1/driver/${payload.id}`, payload.data).then(function (response) {
                    resolve(response)
                }).catch(function (error) {
                    defaultCatch(error, resolve)
                });
            });
        }
    }

    // create dispatcher setDispatcherPic
    static createDispatcher(values: any) {
        let payload = values;
        instance.defaults.headers.common['x-firebase-token'] = payload.sessionToken;
        logger(`${log_prefix} ${messages.start} login`, VERBOSE, "info");
        if (!payload.id) {
            return new Promise(resolve => {
                instance.post('users/v1/dispatcher', payload.data).then(function (response) {
                    resolve(response)
                }).catch(function (error) {
                    defaultCatch(error, resolve)
                });
            });
        }else{
            return new Promise(resolve => {
                instance.patch(`users/v1/dispatcher/${payload.id}`, payload.data).then(function (response) {
                    resolve(response)
                }).catch(function (error) {
                    defaultCatch(error, resolve)
                });
            });
        }
    }

    // create vehicle setDispatcherPic
    static createVehicleprof(values: any) {
        let payload = values;
        instance.defaults.headers.common['x-firebase-token'] = payload.sessionToken;
        logger(`${log_prefix} ${messages.start} login`, VERBOSE, "info");
        if (!payload.id) {
            return new Promise(resolve => {
                instance.post('vehicles/v1/vehicle', payload.data).then(function (response) {
                    resolve(response)
                }).catch(function (error) {
                    defaultCatch(error, resolve)
                });
            });
        }else{
            return new Promise(resolve => {
                instance.patch(`vehicles/v1/vehicle/${payload.id}`, payload.data).then(function (response) {
                    resolve(response)
                }).catch(function (error) {
                    defaultCatch(error, resolve)
                });
            });
        }
    }

    // get specific damag car details
    static getDamageSpecificCar(values: any) {
        let payload = values;
        instance.defaults.headers.common['x-firebase-token'] = payload.sessionToken;
        logger(`${log_prefix} ${messages.start} login`, VERBOSE, "info");
        return new Promise(resolve => {
            instance.get('damages/v1/vehicle/' + payload.id)
                .then(function (response) {
                    resolve(response)
                })
                .catch(function (error) {
                    defaultCatch(error, resolve)
                });
        });
    }

    // Fetch Previos Inspection by Vehicle
    static fetchPrevInspection(values: any) {
        let payload = values;
        instance.defaults.headers.common['x-firebase-token'] = payload.sessionToken;
        logger(`${log_prefix} ${messages.start} login`, VERBOSE, "info");
        return new Promise(resolve => {
            instance.get('inspections/v1/inspection_form/previous/vehicle/' + payload.id + '?today_date=' + payload.date)
                .then(function (response) {
                    resolve(response)
                })
                .catch(function (error) {
                    defaultCatch(error, resolve)
                });
        });
    }

    // get stats
    static getStats(values: any) {
        let payload = values;
        instance.defaults.headers.common['x-firebase-token'] = payload.sessionToken;
        logger(`${log_prefix} ${messages.start} login`, VERBOSE, "info");
        return new Promise(resolve => {
            instance.get('reports/v1/report/dashboard/stats')
                .then(function (response) {
                    resolve(response)
                })
                .catch(function (error) {
                    defaultCatch(error, resolve)
                });
        });
    }
}
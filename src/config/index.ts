import Colors from "./Colors";
import { Form, Typography} from "./GlobalStyles";
import { setI18nConfig, translate } from "./Lang";
import { navigate, goBack } from "./navigator";
// console.log("Form", Form)
export {
    Colors,
    Typography,
    Form,
    setI18nConfig,
    translate,
    navigate,
    goBack,
}
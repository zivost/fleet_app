
import { Alert, StyleSheet, useColorScheme } from 'react-native';
import { iOSUIKit } from 'react-native-typography';
import { Colors } from "./index"

// const isDark = (useColorScheme() === 'dark');
// StyleSheet.flatten()
// const systemWeights = StyleSheet.create({
//     thin: {
//         fontWeight: "100"
//     },
//     light: {
//         fontWeight: "300"
//     },
//     regular: {
//         fontWeight: "400"
//     },
//     semibold: {
//         fontWeight: "600"
//     },
//     bold: {
//         fontWeight: "700"
//     }
// });

export const Typography:any = StyleSheet.create({
    // root_view: {
    //     backgroundColor: isDark ? Colors.black : Colors.white
    // },
    // title1: isDark ? iOSUIKit.largeTitleEmphasizedWhiteObject : iOSUIKit.largeTitleEmphasizedObject,
    // title3: isDark ? iOSUIKit.title3EmphasizedWhiteObject : iOSUIKit.title3EmphasizedObject,
    // subheading: isDark ? iOSUIKit.subheadEmphasizedWhiteObject : iOSUIKit.subheadEmphasizedObject,
    // body: isDark ? iOSUIKit.bodyWhiteObject : iOSUIKit.bodyObject,
    // caption: isDark ? iOSUIKit.caption2WhiteObject : iOSUIKit.caption2Object,
    // ...StyleSheet.flatten(systemWeights)
});

export const Form:any =  StyleSheet.create({
    TextInput:{
        width:"100%",
        height:50,
        borderBottomColor: Colors.bottom_grey,
        borderBottomWidth:1,
        padding:0,
        color: Colors.black
    },
    Button:{
        width:"100%",
        height:40,
        backgroundColor:Colors.primary,
        padding:0,
        color: Colors.white,
        justifyContent:"center",
        borderRadius:20
    }
})
/**
 *
 * @flow strict-local
 * @format
 */

 import { useColorScheme } from 'react-native';

const color_codes = {
    light_blue: '#4F8BFF',
    light_grey: '#A7A8A9',
    red: "#E54343",
    black: "#000000",
    white: "#ffffff",
    oil_black: '#222',
    gray:'#5B5B5B',
    gray_txt:"#848484",
    red_txt:"#F90000",
    dark_red:'#EE0202',
    light_black:'#303030',
    dark_chocklet:"#38101B",
    dark_blue:"#153575",
    light_gray:'#5E5E5E',
    black_shade:'#444444',
    gray_shade:'#434343'
};

export default {
    ...color_codes,
    primary: color_codes.light_blue,
    bottom_grey: color_codes.light_grey,
    // success: color_codes.emerald_green,
    // warning: color_codes.apricot_yellow,
    error: color_codes.red,
    wall: (useColorScheme() === "dark") ? color_codes.oil_black : color_codes.white
};
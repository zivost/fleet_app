/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useEffect, useState } from "react";
import {
    StyleSheet,
    useColorScheme,
    View,
    Image,
    Text,
    BackHandler
} from 'react-native';
import {
    Colors,
    Typography,
    setI18nConfig,
    translate,
    Form,
    goBack
} from "../../config";
import type { StackParamList } from "../../routes.types";
import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import { connect, useStore, useDispatch } from "react-redux";
import Header from "../../../Components/Header/index"
import constants from '../../constants';
import Loader from '../../../Components/Loader/index'
import { ScrollView } from 'react-native-gesture-handler';
import moment from "moment";
import VehicleInfo from '../../../Components/VehicleInfo/index'
import { DispatcherInfo, DriverInfo } from '../../../Components/DispatcherInfo/index'
import { MenuModal } from '../../../Components/ModalView/index'


type Props = NativeStackScreenProps<StackParamList, 'Profile'>;
const Profile: FC<Props> = ({ navigation, auth, route }) => {

    const data = route?.params?.data || 'no data';
    const headerTxt = route?.params?.screen || 'no data';
    console.log('//////',data)

    const store = useStore();
    const dispatch = useDispatch();
    const [userData, setuserData] = useState(data);
    const [headTxt, setHeadTxt] = useState(headerTxt);
    const [setDetails, setsetDetails] = useState('');
    const [flag, setFlag] = useState('');
    const [userImg, setUserImg] = useState('');
    const [name, setName] = useState('');
    const [emailId, setemailId] = useState('');
    const [phoneNo, setPhoneNo] = useState('');
    const [hireDate,setHireDate] = useState('');

    const [vanNo, setVanNo] = useState('');
    const [vin, setVin] = useState('');
    const [licence, setLicencee] = useState('');
    const [liecenState, setLicenceState] = useState('');
    const [make, setMake] = useState('');
    const [vanModel, setVanModel] = useState('');
    const [trim, setTrim] = useState('');
    const [Year, setYear] = useState('');
    const [dateRec, setDateRec] = useState('');
    const [dateInsur, setDateInsur] = useState('');
    const [insurExp, setInsurExp] = useState('');
    const [gasCard, setGasCard] = useState('');
    const [id, setid] = useState('');
    const [menuModal, seMenuModal] = useState(false);


    useEffect(() => {
        if (headerTxt == 'Damage' && data) {
            setsetDetails(data.detail)
            setFlag(data.flag)
            setUserImg(data.image[0])
            setName(data.vehicle.name)
        } else if (headerTxt == 'Drivers' && data) {
            setemailId(data.email)
            setPhoneNo(data.phone)
            setName(data.name)
            setHireDate(data.hire_date)
        } else if (headerTxt == 'Vehicle' && data) {
            setVin(data.vin)
            setLicenceState(data.state)
            setName(data.name)
            setMake(data.make)
            setVanModel(data.model)
            setTrim(data.trim)
            setYear(data.year)
            setDateRec(moment(data.recieved).format("YYYY-MM-DD"))
            setDateInsur(moment(data.insured).format("YYYY-MM-DD"))
            setInsurExp(moment(data.insurance_expires).format("YYYY-MM-DD"))
            setGasCard(data.gas_card)
        } else if (headerTxt == 'Dispatchers' && data) {
            setid(data.id)
            setPhoneNo(data.phone)
            setName(data.name)
        }
    }, [])

    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };
    }, []);

    const handleBackButtonClick = () => {
        navigation.goBack(null);
        return true;
    }

    const navToEdit = () => {
        if (headerTxt == 'Damage') {
            navigation.navigate('AddDamage', { data })
        } else if (headerTxt == 'Drivers') {
            seMenuModal(!menuModal)
            // navigation.navigate('AddDriver', { data })
        } else if (headerTxt == 'Vehicle') {
            // navigation.navigate('AddVehicle', { data })
            seMenuModal(!menuModal)
        } else if (headerTxt == 'Dispatchers') {
            // navigation.navigate('AddDispatcher', { data })
            seMenuModal(!menuModal)
        }
    }

    const navToEditProfile = () => {
        console.log('///////edit')
        if (headerTxt == 'Damage') {
            navigation.navigate('AddDamage', { data })
        } else if (headerTxt == 'Drivers') {
            seMenuModal(!menuModal)
            navigation.navigate('AddDriver', { data })
        } else if (headerTxt == 'Vehicle') {
            navigation.navigate('AddVehicle', { data })
            seMenuModal(!menuModal)
        } else if (headerTxt == 'Dispatchers') {
            navigation.navigate('AddDispatcher', { data })
            seMenuModal(!menuModal)
        }
    }


    let { loading } = auth;
    const isDarkMode = useColorScheme() === 'dark';
    return (
        <View style={[local_styles.mainContainer, { backgroundColor: (isDarkMode) ? "#fff" : "#fff" }]}>
            <Loader loading={loading} />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={local_styles.container}>
                    {headTxt == 'Damage' ?
                        <View style={local_styles.infoBox}>
                            <View style={local_styles.addrssBar}>
                                <View style={{ width: 120 }}>
                                    <Text style={[local_styles.infoTxt, { color: Colors.gray }]}>{translate('damageDetails')}</Text>
                                </View>
                                <Text style={[local_styles.noTxt, { color: Colors.primary }]}>{setDetails}</Text>
                            </View>

                            <View style={local_styles.addrssBar}>
                                <View style={{ width: 120 }}>
                                    <Text style={[local_styles.infoTxt, { color: Colors.gray }]}>{translate('flag')}</Text>
                                </View>
                                <Text style={[local_styles.noTxt, { color: Colors.primary }]}>{flag}</Text>
                            </View>
                        </View> : null}
                    {headTxt == 'Vehicle' ?
                        <View>
                            <Header
                                title={translate('vehiclInfo')}
                                editMenu={true}
                                goBack={navigation.goBack}
                                navToEditScreen={navToEdit}
                            />

                            <VehicleInfo data={data} />
                        </View>
                        : null}
                    {headTxt == 'Dispatchers' ?
                        <View>
                            <Header
                                title={translate('dispatcherInfo')}
                                editMenu={true}
                                goBack={navigation.goBack}
                                navToEditScreen={navToEdit}
                            />

                            <DispatcherInfo data={data} />
                        </View> : null}
                    {headTxt == 'Drivers' ?
                        <View>
                            <Header
                                title={translate('driverInfo')}
                                editMenu={true}
                                goBack={navigation.goBack}
                                navToEditScreen={navToEdit}
                            />
                            <DriverInfo data={data} />
                        </View> : null}
                </View>
            </ScrollView>
            <MenuModal menuModal={menuModal} closeMenuModal={() => seMenuModal(!menuModal)} navToEdit={navToEditProfile} />
        </View>
    );
};


const local_styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },
    container: {
        marginLeft: 20,
        marginRight: 25,
        marginTop: 20
    },
    middleBox: {
        marginTop: 30,
        alignItems: 'center'
    },
    imgBox: {
        height: 145,
        width: 133,
        borderRadius: 5,
        resizeMode: 'contain'
    },
    nameTxt: {
        fontSize: 18,
        fontWeight: '500',
        paddingTop: 20
    },
    infoBox: {
        marginTop: 41
    },
    addrssBar: {
        flexDirection: 'row',
        alignItems: 'center',
        // justifyContent: 'space-between',
        marginLeft: 28,
        marginRight: 50,
        marginBottom: 23
    },
    infoTxt: {
        fontSize: 14,
        fontWeight: '500'
    },
    noTxt: {
        fontSize: 12,
        fontWeight: '400'
    }
});

const mapStateToProps = (state: any) => ({
    auth: state.auth,
})
export default connect(mapStateToProps)(Profile);
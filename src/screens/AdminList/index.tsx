/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useEffect, useState } from "react";
import {
    StyleSheet,
    useColorScheme,
    View,
    Image,
    FlatList,
    Text,
    TouchableOpacity,
    ActivityIndicator,
    ScrollView,
    BackHandler,
    Dimensions
} from 'react-native';
import {
    Colors,
    Typography,
    setI18nConfig,
    translate,
    Form
} from "../../config";
import Header from "../../../Components/Header/index"
import { useDispatch, useSelector, connect } from 'react-redux'
import constants from '../../constants';
let width = Dimensions.get('window').width;
let height = Dimensions.get('window').height;
const AdminList: FC<Props> = ({ navigation, driver, auth }) => {

    const dispatch = useDispatch();
    const [adminData, setAdminData] = useState<any[]>([]);
    const [offset, setOffset] = useState(0);
    const [currLength, setCurrLength] = useState(0);
    const [loader, setLoading] = useState(false)

    useEffect(() => {
        if (auth.getAdminList && auth.getAdminList.length > 0) {
            setAdminData(auth.getAdminList)
        }
    }, [auth.getAdminList])


    useEffect(() => {
        let paramsTrending = '&limit=5&offset=0'
        dispatch({
            type: constants("auth").sagas.getAdminList,
            payload: {
                data: paramsTrending,
                sessionToken: auth.user.token
            }
        })

    }, [])

    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };
    }, []);

    const handleBackButtonClick = () => {
        dispatch({
            type: constants('auth').reducers.getAdminList.set
        })
        navigation.goBack(null);
        return true;
    }


    const _AdminList = ({ item, index }) => {
        return (
            <TouchableOpacity style={local_styles.mainView}>
                <Image source={require("assets/images/driver.png")} style={local_styles.driverImg} />
                <View style={local_styles.middleBox}>
                    {item.name ? <Text style={[local_styles.driName, { color: Colors.primary, width: 78, textAlign: 'center' }]} numberOfLines={1}>{item.name}</Text> : null}

                    <View style={local_styles.horiContainer}>
                        <Text style={[local_styles.cllTxt, { color: Colors.gray }]}>Email</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }


    const navToDetails = (data) => {
        navigation.navigate('DamageDetails', { data })
    }


    const fetchCategory = () => {
        let oldOffset = offset;
        let currLen = driver.getAllData.length;
        if (currLen > currLength) {
            let offset = parseInt(oldOffset) + 5;
            let paramsTrending = 'limit=5&offset=' + offset
            dispatch({
                type: constants("auth").sagas.getAdminList,
                payload: {
                    data: paramsTrending,
                    sessionToken: auth.user.token,
                }
            })
            setOffset(oldOffset + 20)
            setLoading(true)
            setCurrLength(currLen)
        }

        setTimeout(() => { setLoading(false) }, 4000)
    }

    const _footerLoader = () => {
        if (loading) {
            return (
                <View style={local_styles.loaderView}>
                    <ActivityIndicator size="large" color={Colors.primary} />
                    <Text style={[local_styles.loadingText, { color: Colors.primary }]}>Loading…</Text>
                </View>
            )
        } else {
            return (
                <View style={local_styles.loaderView}>

                </View>
            )
        }
    }

    const _loadingOval = () => {
        if (!loading) {
            return (
                <View>
                    <Text style={local_styles.emptMsg}>No Data found!</Text>
                </View>
            )
        } else {
            return (<View />)
        }
    }


    const navScree = () => {
        navigation.navigate('Admin')
    }

    let { loading } = driver;
    const isDarkMode = useColorScheme() === 'dark';
    return (
        <View style={[local_styles.mainContainer, { backgroundColor: (isDarkMode) ? "#fff" : "#fff" }]}>
            <View>
                <View style={local_styles.topBox}>
                    <Header
                        title={'Admin List'}
                        goBack={handleBackButtonClick}
                        addBtn={auth.user.role === 'owner' ? true : false}
                        btnTxt={'Admin'}
                        navScree={navScree}
                    />
                </View>
                <View style={local_styles.listView}>
                    <FlatList
                        data={adminData}
                        renderItem={_AdminList}
                        numColumns={3}
                        keyExtractor={(item, index) => index.toString()}
                        onEndReached={fetchCategory}
                        onEndReachedThreshold={0.9}
                        initialNumToRender={5}
                        ListFooterComponent={_footerLoader}
                        scrollEnabled={true}
                        style={{ paddingTop: 20, height: height - 100 }}
                        showsVerticalScrollIndicator={false}
                        ListEmptyComponent={_loadingOval} />
                </View>
            </View>
        </View>
    );
};

const local_styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },
    listView: {
        // alignItems: 'center'
    },
    topBox: {
        marginTop: 15,
        marginLeft: 15,
        marginBottom: 10,
        marginRight: 24
    },
    loaderView: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center"
    },
    loadingText: {
        fontSize: 18,
        fontWeight: "500",
        fontStyle: "normal",
        textAlign: "left",
        marginLeft: 9
    },
    emptMsg: {
        fontSize: 10,
        fontWeight: "600",
        fontStyle: "normal",
        lineHeight: 18,
        letterSpacing: 0,
        textAlign: "center",
        color: "#000",
    },
    middleBox: {
        marginTop: 13,
        alignItems: 'center'
    },
    horiContainer: {
        marginTop: 3,
        alignItems: 'center',
        width: 40
    },
    mainView: {
        alignItems: 'center',
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 26
    },
    driverImg: {
        height: 100,
        width: 82,
        resizeMode: 'contain'
    },
    driName: {
        fontSize: 12,
        fontWeight: '400'
    },
    cllTxt: {
        fontSize: 8,
        fontWeight: '500',
        textDecorationLine: 'underline'
    },
});

const mapStateToProps = (state: any) => ({
    auth: state.auth,
    driver: state.driver
})
export default connect(mapStateToProps)(AdminList);

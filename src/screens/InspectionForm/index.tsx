/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useEffect, useState } from "react";
import {
    StyleSheet,
    useColorScheme,
    View,
    Image,
    Text,
    TouchableOpacity,
    ScrollView,
    Dimensions,
    TextInput,
    PermissionsAndroid,
    BackHandler,
    Modal,
    ActivityIndicator
} from 'react-native';
import { human } from 'react-native-typography'
import Button from "../../../Components/Button/index"
import {
    Colors,
    Typography,
    setI18nConfig,
    translate,
    Form
} from "../../config";
import Header from "../../../Components/Header/index"
import LinearGradient from 'react-native-linear-gradient';
import { EditInspectBox, EditHandtruckBox } from '../../../Components/EditInspectBox/index'
import EditTextBox from '../../../Components/EditTextBox/index'
import { SubInspectModal, GasModal, CameraModal, ParkingModal, RescueModal } from '../../../Components/ModalView/index'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { useDispatch, useSelector, connect } from 'react-redux'
import constants from '../../constants';
import TextInputField from '../../../Components/TextInput/index'
import Loader from '../../../Components/Loader/index'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import moment from "moment";
let width = Dimensions.get('window').width;
let height = Dimensions.get('window').height;

const InspectionForm: FC<Props> = ({ navigation, auth, route, driver }) => {

    const date = route?.params?.date || "no date";
    const date_text = route?.params?.date_text || "no date";
    const vehicl_id = route?.params?.vehicleId || "";
    const sel_driver = route?.params?.sel_driver || {};
    const driverId = route?.params?.driverId || "";
    const VehicleName = route?.params?.vehicleName || "";
    const inspectobj = route?.params?.inspectobj || [];
    const dispatch = useDispatch();

    const damageList = [{ "backgroungColor": 'red', "name": 'Severely Damaged' },
    { "backgroungColor": 'yellow', "name": 'Moderately Damaged' },
    { "backgroungColor": 'green', "name": 'Good Condition' }]
    const [flag, setFlag] = useState('')
    const gasList = ['0', '1/4', '2/4', '3/4', '4/4']
    const [gasModal, SetgasModal] = useState(false);
    const [camModal, SetCamModal] = useState(false);
    const [parkingModal, setparkingModal] = useState(false);
    const [rescueModal, setRescueModal] = useState(false);
    const [vehicleClean, setVehicleClean] = useState(undefined);
    const [wiper, setWiper] = useState(undefined);
    const [engineLight, setEngineLight] = useState(undefined);
    const [gasCard, setGascard] = useState(undefined);
    const [charger, setCharger] = useState(undefined);
    const [phone, setPhone] = useState(undefined);
    const [rescue, setRescue] = useState(undefined);
    const [isparkingTicket, setIsParkingTicket] = useState(undefined);
    const [checkoutModal, setCheckoutModal] = useState(false);
    const [mileage, SetMilege] = useState('');
    const [gas, setGas] = useState(0)
    const [comment, setComment] = useState('')
    const [parkingTic, setParkingTic] = useState('')
    const [rescueRsn, setrescueRsn] = useState('')
    const [frontView, setFrontView] = useState('')
    const [rearView, setRearView] = useState('')
    const [driverView, setDriverView] = useState('')
    const [parkingTckImg, setparkingTckImg] = useState('')
    const [passengerView, setPassengerView] = useState('')
    const [handtruckView, setHandtruckView] = useState('')
    const [prevFrontView, setPrevFrontView] = useState('')
    const [prevRearView, setPrevRearView] = useState('')
    const [prevDriverView, setPrevDriverView] = useState('')
    const [prevPassengerView, setPrevPassengerView] = useState('')
    const [prevHandtruckView, setPrevHandtruckView] = useState('')
    const [prevVehicleClean, setPrevVehicleClean] = useState('');
    const [prevParking, setPrevParkin] = useState('');
    const [prevWiper, setPrevWiper] = useState('');
    const [prevEngineLight, setPrevEngineLight] = useState('');
    const [prevGasCard, setPrevGascard] = useState('');
    const [prevCharger, setPrevCharger] = useState('');
    const [prevPhone, setPrevPhone] = useState('');
    const [prevRescue, setPrevRescue] = useState('');
    const [prevMilege, setPrevMilege] = useState('');
    const [prevGas, setPrevGas] = useState('');
    const [inspectionData, setInspectionData] = useState('')
    const [wiperOpacity, setWiperOpacity] = useState('');
    const [vehicleCleanOpacity, setVehicleCleanOpacity] = useState('');
    const [parkingOpacity, setParkingOpacity] = useState('');
    const [phoneOpacity, setPhoneOpacity] = useState('');
    const [rescueOpacity, setRecueOpacity] = useState('');
    const [gasCardOpacity, setGasCardOpacity] = useState('');
    const [engineOpacity, setEngineOpacity] = useState('');
    const [chargerOpacity, setChargerOpacity] = useState('');
    const [isHandtruck, setIsHandtruck] = useState('');
    const [name, setName] = useState('')
    const [headerTxt, setHeaderTxt] = useState('')
    const [openDamageModal, setOpenDamageModal] = useState(false)
    const [damageDetails, setDamageDetails] = useState('')
    const [imageModal, setImageModal] = useState('')
    const [indicatorName, setIndicatorName] = useState('')
    const [milegDate, setMilegeDate] = useState('')

    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };
    }, []);

    useEffect(() => {
        dispatch({
            type: constants("driver").sagas.fetchPrevInspection,
            payload: {
                date: date,
                id: vehicl_id,
                sessionToken: auth.user.token
            }
        })
    }, []);

    useEffect(() => {
        if (inspectobj && inspectobj.length > 0) {
            if (inspectobj[0].inspection_form) {
                // setMilegeDate(moment(inspectobj[0].date).format('DD MMM YYYY'))
                if (inspectobj[0].inspection_form.mileage) {
                    SetMilege(inspectobj[0].inspection_form.mileage)
                }
                if (inspectobj[0].inspection_form.gas) {
                    let gasTank = ''
                    if (inspectobj[0].inspection_form.gas === 0) {
                        gasTank = '0'
                    } else if (inspectobj[0].inspection_form.gas === 25) {
                        gasTank = '1/4'
                    } else if (inspectobj[0].inspection_form.gas === 50) {
                        gasTank = '2/4'
                    } else if (inspectobj[0].inspection_form.gas === 75) {
                        gasTank = '3/4'
                    } else if (inspectobj[0].inspection_form.gas === 100) {
                        gasTank = '4/4'
                    }
                    setGas(gasTank)
                }
                if (inspectobj[0].inspection_form.front_view) {
                    setFrontView(inspectobj[0].inspection_form.front_view.attachment[0])
                }
                if (inspectobj[0].inspection_form.rear_view) {
                    setRearView(inspectobj[0].inspection_form.rear_view.attachment[0])
                }
                if (inspectobj[0].inspection_form.driver_side_view) {
                    setDriverView(inspectobj[0].inspection_form.driver_side_view.attachment[0])
                }
                if (inspectobj[0].inspection_form.passenger_side_view) {
                    setPassengerView(inspectobj[0].inspection_form.passenger_side_view.attachment[0])
                }
                if (inspectobj[0].inspection_form.handtruck) {
                    setHandtruckView(inspectobj[0].inspection_form.handtruck.attachment[0])
                    setIsHandtruck(inspectobj[0].inspection_form.handtruck.attachment[0] ? '1' : '0')
                }
                if (inspectobj[0].inspection_form.clean) {
                    setVehicleClean(inspectobj[0].inspection_form.clean.answer)
                    setVehicleCleanOpacity(inspectobj[0].inspection_form.clean.answer ? '1' : '0')
                }
                if (inspectobj[0].inspection_form.parking_ticket) {
                    setIsParkingTicket(inspectobj[0].inspection_form.parking_ticket.answer)
                    // setparkingTckImg(inspectobj[0].inspection_form.parking_ticket.attachment[0])
                    // setParkingTic(inspectobj[0].inspection_form.parking_ticket.metadata.amount)
                    setParkingOpacity(inspectobj[0].inspection_form.parking_ticket.answer ? '1' : '0')
                }
                if (inspectobj[0].inspection_form.phone) {
                    setPhone(inspectobj[0].inspection_form.phone.answer)
                    setPhoneOpacity(inspectobj[0].inspection_form.phone.answer ? '1' : '0')
                }
                if (inspectobj[0].inspection_form.phone_charger) {
                    setCharger(inspectobj[0].inspection_form.phone_charger.answer)
                    setChargerOpacity(inspectobj[0].inspection_form.phone_charger.answer ? '1' : '0')
                }
                if (inspectobj[0].inspection_form.rescue) {
                    setRescue(inspectobj[0].inspection_form.rescue.answer)
                    // setrescueRsn(inspectobj[0].inspection_form.rescue.metadata.amount)
                    setRecueOpacity(inspectobj[0].inspection_form.rescue.answer ? '1' : '0')
                }
                if (inspectobj[0].inspection_form.gas_card) {
                    setGascard(inspectobj[0].inspection_form.gas_card.answer)
                    setGasCardOpacity(inspectobj[0].inspection_form.gas_card.answer ? '1' : '0')
                }
                if (inspectobj[0].inspection_form.engine_light) {
                    setEngineLight(inspectobj[0].inspection_form.engine_light.answer)
                    setEngineOpacity(inspectobj[0].inspection_form.engine_light.answer ? '1' : '0')
                }
                if (inspectobj[0].inspection_form.windshield_fluid) {
                    setWiper(inspectobj[0].inspection_form.windshield_fluid.answer)
                    setWiperOpacity(inspectobj[0].inspection_form.windshield_fluid.answer ? '1' : '0')
                }
            }
        }
    }, [inspectobj]);


    useEffect(() => {
        if (driver.prevInspection_form && driver.prevInspection_form.length > 0) {
            if (driver.prevInspection_form[0].inspection_form) {
                setMilegeDate(moment(driver.prevInspection_form[0].date).format('DD MMM YYYY'))
                if (driver.prevInspection_form[0].inspection_form.mileage) {
                    setPrevMilege(driver.prevInspection_form[0].inspection_form.mileage)
                }
                if (driver.prevInspection_form[0].inspection_form.gas) {
                    let gasTank = ''
                    if (driver.prevInspection_form[0].inspection_form.gas === 0) {
                        gasTank = '0'
                    } else if (driver.prevInspection_form[0].inspection_form.gas === 25) {
                        gasTank = '1/4'
                    } else if (driver.prevInspection_form[0].inspection_form.gas === 50) {
                        gasTank = '2/4'
                    } else if (driver.prevInspection_form[0].inspection_form.gas === 75) {
                        gasTank = '3/4'
                    } else if (driver.prevInspection_form[0].inspection_form.gas === 100) {
                        gasTank = '4/4'
                    }
                    setPrevGas(gasTank)
                }
                if (driver.prevInspection_form[0].inspection_form.front_view) {
                    setPrevFrontView(driver.prevInspection_form[0].inspection_form.front_view.attachment[0])
                }
                if (driver.prevInspection_form[0].inspection_form.rear_view) {
                    setPrevRearView(driver.prevInspection_form[0].inspection_form.rear_view.attachment[0])
                }
                if (driver.prevInspection_form[0].inspection_form.driver_side_view) {
                    setPrevDriverView(driver.prevInspection_form[0].inspection_form.driver_side_view.attachment[0])
                }
                if (driver.prevInspection_form[0].inspection_form.passenger_side_view) {
                    setPrevPassengerView(driver.prevInspection_form[0].inspection_form.passenger_side_view.attachment[0])
                }
                if (driver.prevInspection_form[0].inspection_form.handtruck) {
                    setPrevHandtruckView(driver.prevInspection_form[0].inspection_form.handtruck.attachment[0])
                }
                if (driver.prevInspection_form[0].inspection_form.clean) {
                    setPrevVehicleClean(driver.prevInspection_form[0].inspection_form.clean.answer)
                    // setVehicleCleanOpacity(driver.prevInspection_form[0].inspection_form.clean.answer ? '1' : '0')
                }
                if (driver.prevInspection_form[0].inspection_form.parking_ticket) {
                    setPrevParkin(driver.prevInspection_form[0].inspection_form.parking_ticket.answer)
                    // setparkingTckImg(driver.prevInspection_form[0].inspection_form.parking_ticket.attachment[0])
                    // setParkingTic(driver.prevInspection_form[0].inspection_form.parking_ticket.metadata.amount)
                    // setParkingOpacity(driver.prevInspection_form[0].inspection_form.parking_ticket.answer ? '1' : '0')
                }
                if (driver.prevInspection_form[0].inspection_form.phone) {
                    setPrevPhone(driver.prevInspection_form[0].inspection_form.phone.answer)
                    // setPhoneOpacity(driver.prevInspection_form[0].inspection_form.phone.answer ? '1' : '0')
                }
                if (driver.prevInspection_form[0].inspection_form.phone_charger) {
                    setPrevCharger(driver.prevInspection_form[0].inspection_form.phone_charger.answer)
                    // setChargerOpacity(driver.prevInspection_form[0].inspection_form.phone_charger.answer ? '1' : '0')
                }
                if (driver.prevInspection_form[0].inspection_form.rescue) {
                    setPrevRescue(driver.prevInspection_form[0].inspection_form.rescue.answer)
                    // setrescueRsn(driver.prevInspection_form[0].inspection_form.rescue.metadata.amount)
                    // setRecueOpacity(driver.prevInspection_form[0].inspection_form.rescue.answer ? '1' : '0')
                }
                if (driver.prevInspection_form[0].inspection_form.gas_card) {
                    setPrevGascard(driver.prevInspection_form[0].inspection_form.gas_card.answer)
                    // setGasCardOpacity(driver.prevInspection_form[0].inspection_form.gas_card.answer ? '1' : '0')
                }
                if (driver.prevInspection_form[0].inspection_form.engine_light) {
                    setPrevEngineLight(driver.prevInspection_form[0].inspection_form.engine_light.answer)
                    // setEngineOpacity(driver.prevInspection_form[0].inspection_form.engine_light.answer ? '1' : '0')
                }
                if (driver.prevInspection_form[0].inspection_form.windshield_fluid) {
                    setPrevWiper(driver.prevInspection_form[0].inspection_form.windshield_fluid.answer)
                    // setWiperOpacity(driver.prevInspection_form[0].inspection_form.windshield_fluid.answer ? '1' : '0')
                }
            }
        }
    }, [driver.prevInspection_form]);

    useEffect(() => {
        if (auth.user && auth.user.company && auth.user.company.inspection) {
            setInspectionData(auth.user.company.inspection)
        }
    }, [auth.user]);

    const handleBackButtonClick = () => {
        navigation.goBack(null);
        return true;
    }

    const setGasTank = (data: any) => {
        setGas(data),
            SetgasModal(!gasModal)
    }

    const updateInspection = (data: any) => {
        let checkout = false
        if (data == 'checkout') {
            setCheckoutModal(!checkoutModal)
            checkout = true
        }
        let gasTank = ''
        if (gas == '0') {
            gasTank = 0
        } else if (gas === '1/4') {
            gasTank = 25
        } else if (gas === '2/4') {
            gasTank = 50
        } else if (gas === '3/4') {
            gasTank = 75
        } else if (gas === '4/4') {
            gasTank = 100
        }

        // let inspection_object: any = {
        //     "vehicle": vehicl_id,
        //     "inspection_form": {
        //         "phone": {
        //             "answer": phone,
        //         },
        //         "rescue": {
        //             "answer": rescue,
        //             "metadata": {
        //                 "amount": rescueRsn
        //             }
        //         },
        //         "phone_charger": {
        //             "answer": charger,
        //         },
        //         "clean": {
        //             "answer": vehicleClean,
        //         },
        //         "parking_ticket": {
        //             "answer": true,
        //             "attachment": [
        //                 parkingTckImg
        //             ],
        //             "metadata": {
        //                 "amount": parkingTic
        //             }
        //         },
        //         "gas_card": {
        //             "answer": gasCard,
        //         },
        //         "windshield_fluid": {
        //             "answer": wiper,
        //         },
        //         "engine_light": {
        //             "answer": engineLight,
        //         },
        //         "driver_side_view": {
        //             "answer": driverView == '' ? false : true,
        //             "attachment": [
        //                 driverView
        //             ],
        //         },
        //         "passenger_side_view": {
        //             "answer": passengerView == '' ? false : true,
        //             "attachment": [
        //                 passengerView
        //             ],
        //         },
        //         "rear_view": {
        //             "answer": rearView == '' ? false : true,
        //             "attachment": [
        //                 rearView
        //             ],
        //         },
        //         "front_view": {
        //             "answer": frontView == '' ? false : true,
        //             "attachment": [
        //                 frontView
        //             ],
        //         },
        //         "mileage": mileage,
        //         "gas": gasTank,
        //         "handtruck": {
        //             "answer": true,
        //             "attachment": [
        //                 handtruckView
        //             ],
        //         }
        //     },
        // };
        let inspection_object = {
            "comments": comment,
            "driver": driverId,
            "checkout": checkout,
            "vehicle": vehicl_id,
            "inspection_form": {}
        }
        if (phone !== undefined) {
            inspection_object.inspection_form.phone = {
                "answer": phone
            };
        }
        if (rescue != undefined) {
            inspection_object.inspection_form.rescue = {
                "answer": rescue,
                "metadata": {
                    "amount": rescueRsn
                }
            };
        }
        if (wiper != undefined) {
            inspection_object.inspection_form.windshield_fluid = {
                "answer": wiper
            }
        }
        if (engineLight != undefined) {
            inspection_object.inspection_form.engine_light = {
                "answer": engineLight
            }
        }
        if (gasCard != undefined) {
            inspection_object.inspection_form.gas_card = {
                "answer": gasCard,
            }
        }
        if (charger != undefined) {
            inspection_object.inspection_form.phone_charger = {
                "answer": charger
            }
        }
        if (isparkingTicket != undefined) {
            if (isparkingTicket == false) {
                inspection_object.inspection_form.parking_ticket = {
                    "answer": isparkingTicket
                }
            } else {
                inspection_object.inspection_form.parking_ticket = {
                    "answer": true,
                    "attachment": [
                        parkingTckImg
                    ],
                    "metadata": {
                        "amount": parkingTic
                    }
                }
            }

        }
        if (vehicleClean != undefined) {
            inspection_object.inspection_form.clean = {
                "answer": vehicleClean
            }
        }
        if (handtruckView != '') {
            inspection_object.inspection_form.handtruck = {
                "answer": true,
                "attachment": [
                    handtruckView
                ],
            }
        }
        if (rearView != '') {
            inspection_object.inspection_form.rear_view = {
                "answer": true,
                "attachment": [
                    rearView
                ],
            }
        }
        if (passengerView != '') {
            inspection_object.inspection_form.passenger_side_view = {
                "answer": true,
                "attachment": [
                    passengerView
                ],
            }
        }
        if (driverView != '') {
            inspection_object.inspection_form.driver_side_view = {
                "answer": true,
                "attachment": [
                    driverView
                ],
            }
        }
        if (frontView != '') {
            inspection_object.inspection_form.front_view = {
                "answer": true,
                "attachment": [
                    frontView
                ],
            }
        }
        if (gasTank != '') {
            inspection_object.inspection_form.gas = gasTank
        }
        if (mileage != '') {
            inspection_object.inspection_form.mileage = mileage
        }
        console.log('inspection_object::', inspection_object);

        dispatch({
            type: constants("driver").sagas.updateInspection,
            payload: {
                data: inspection_object,
                date: date,
                id: vehicl_id,
                sessionToken: auth.user.token,
            }
        })
    }

    const setCamera = async (data) => {
        setIndicatorName('')
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                    title: "App Camera Permission",
                    message: "App needs access to your camera ",
                    buttonNeutral: "Ask Me Later",
                    buttonNegative: "Cancel",
                    buttonPositive: "OK"
                }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                let options = {
                    maxWidth: 1000, //speeds up compressImage to almost no time
                    maxHeight: 1000,
                };
                if (data == 'frontView') {
                    launchCamera(options, (response) => {
                        if (response && response.assets && response.assets.length > 0) {
                            let img = response.assets[0];
                            dispatch({
                                type: constants("driver").sagas.uploadDamageImg,
                                payload: {
                                    data: {
                                        file: img,
                                    },
                                    type: 'frontView',
                                    sessionToken: auth.user.token,
                                },
                                setImg: setImg
                            })
                        }
                    })
                } else if (data == 'driverView') {
                    launchCamera(options, (response) => {
                        if (response && response.assets && response.assets.length > 0) {
                            let img = response.assets[0];
                            dispatch({
                                type: constants("driver").sagas.uploadDamageImg,
                                payload: {
                                    data: {
                                        file: img,
                                    },
                                    type: 'driverView',
                                    sessionToken: auth.user.token,
                                },
                                setImg: setImg
                            })
                        }
                    })
                } else if (data == 'passengerView') {
                    launchCamera(options, (response) => {
                        if (response && response.assets && response.assets.length > 0) {
                            let img = response.assets[0];
                            dispatch({
                                type: constants("driver").sagas.uploadDamageImg,
                                payload: {
                                    data: {
                                        file: img,
                                    },
                                    type: 'passengerView',
                                    sessionToken: auth.user.token,
                                },
                                setImg: setImg
                            })
                        }
                    })
                } else if (data == 'rearView') {
                    launchCamera(options, (response) => {
                        if (response && response.assets && response.assets.length > 0) {
                            let img = response.assets[0];
                            dispatch({
                                type: constants("driver").sagas.uploadDamageImg,
                                payload: {
                                    data: {
                                        file: img,
                                    },
                                    type: 'rearView',
                                    sessionToken: auth.user.token,
                                },
                                setImg: setImg
                            })
                        }
                    })
                } else if (data == 'handtruckView') {
                    launchCamera(options, (response) => {
                        if (response && response.assets && response.assets.length > 0) {
                            let img = response.assets[0];
                            dispatch({
                                type: constants("driver").sagas.uploadDamageImg,
                                payload: {
                                    data: {
                                        file: img,
                                    },
                                    type: 'handtruckView',
                                    sessionToken: auth.user.token,
                                },
                                setImg: setImg
                            })
                        }
                    })
                } else if (data == 'parkingTicket') {
                    setparkingModal(false)
                    launchCamera(options, (response) => {
                        if (response && response.assets && response.assets.length > 0) {
                            let img = response.assets[0];
                            // setparkingTckImg(img)
                            dispatch({
                                type: constants("driver").sagas.uploadDamageImg,
                                payload: {
                                    data: {
                                        file: img,
                                    },
                                    type: 'parkingTicket',
                                    sessionToken: auth.user.token,
                                },
                                setImg: setImg
                            })
                        }
                    })
                }
            } else {
                console.log("Camera permission denied");
            }
        } catch (err) {
            console.warn(err);
        }
    }

    const setImg = (data, type) => {
        if (type == 'frontView') {
            setFrontView(data.data)
            setImageModal(data.data)
        } else if (type == 'driverView') {
            setDriverView(data.data)
            setImageModal(data.data)
        } else if (type == 'passengerView') {
            setPassengerView(data.data)
            setImageModal(data.data)
        } else if (type == 'rearView') {
            setRearView(data.data)
            setImageModal(data.data)
        } else if (type == 'handtruckView') {
            setHandtruckView(data.data)
        } else if (type == 'parkingTicket') {
            setparkingTckImg(data.data)
            setIsParkingTicket(true)
        }
    }


    const yesBtn = (data) => {
        if (data == 'parkingTicket') {
            setParkingOpacity('1')
            setparkingModal(!parkingModal)
        } else if (data == 'rescue') {
            setRecueOpacity('1')
            setRescue(true)
            setRescueModal(!rescueModal)
        } else if (data == 'vehicleClean') {
            setVehicleCleanOpacity('1')
            setVehicleClean(true)
        } else if (data == 'phone') {
            setPhoneOpacity('1')
            setPhone(true)
        } else if (data == 'charger') {
            setChargerOpacity('1')
            setCharger(true)
        } else if (data == 'gasCard') {
            setGasCardOpacity('1')
            setGascard(true)
        } else if (data == 'engineLight') {
            setEngineOpacity('1')
            setEngineLight(true)
        } else if (data == 'wiper') {
            setWiperOpacity('1')
            setWiper(true)
        } else if (data == 'handtruckView') {
            setIsHandtruck('1')
        }
    }

    const noBtn = (data) => {
        if (data == 'rescue') {
            setRecueOpacity('0')
            setRescue(false)
        } else if (data == 'vehicleClean') {
            setVehicleCleanOpacity('0')
            setVehicleClean(false)
        } else if (data == 'phone') {
            setPhoneOpacity('0')
            setPhone(false)
        } else if (data == 'charger') {
            setChargerOpacity('0')
            setCharger(false)
        } else if (data == 'gasCard') {
            setGasCardOpacity('0')
            setGascard(false)
        } else if (data == 'engineLight') {
            setEngineOpacity('0')
            setEngineLight(false)
        } else if (data == 'wiper') {
            setWiperOpacity('0')
            setWiper(false)
        } else if (data == 'parkingTicket') {
            setParkingOpacity('0')
            setIsParkingTicket(false)
        } else if (data == 'handtruckView') {
            setIsHandtruck('0')
        }
    }

    const closeParkingModal = () => {
        setparkingModal(!parkingModal)
    }

    const setParkingTicket = (data) => {
        setParkingTic(data)
    }

    const subTicket = (name) => {
        if (name == 'rescue') {
            setRescueModal(!rescueModal)
        } else if (name == 'parking') {
            setIsParkingTicket(true)
            setparkingModal(!parkingModal)
        }
    }

    const setRescueTxt = (data) => {
        setrescueRsn(data)
    }

    const openDamage = (headerBox, name, image) => {
        setHeaderTxt(headerBox)
        setName(name)
        setFlag('')
        setImageModal(image)
        setDamageDetails('')
        setOpenDamageModal(!openDamageModal)
    }

    const selctGamage = (data) => {
        setFlag(data)
    }

    const uploadDamage = () => {
        setOpenDamageModal(!openDamageModal)
        dispatch({
            type: constants("driver").sagas.addDamage,
            payload: {
                data: {
                    "vehicle": vehicl_id,
                    "flag": flag,
                    "detail": damageDetails,
                    "images": [
                        imageModal
                    ],
                    "inspection": true
                },
                // id:carid,
                sessionToken: auth.user.token,
            }
        })
    }

    const closeDamageModal = () => {
        setOpenDamageModal(!openDamageModal)
    }

    const setIndicator = (data: any) => {
        setIndicatorName(data)
    }

    let { loading } = driver;
    const isDarkMode = useColorScheme() === 'dark';
    return (
        <View style={[local_styles.mainContainer, { backgroundColor: (isDarkMode) ? "#fff" : "#fff" }]}>
            {/* <Loader loading={loading} /> */}
            <ScrollView style={{ flex: 1 }}>
                <View style={local_styles.topBox}>
                    <Header
                        title={translate('chooseInsp')}
                        inspectForm={true}
                        goBack={navigation.goBack}
                        sel_driver={sel_driver}
                        VehicleName={VehicleName}
                    />
                </View>



                <View style={{ alignItems: 'center', marginTop: 31, marginBottom: 20 }}>
                    <View style={local_styles.barStyle}>
                        {inspectionData.mileage ? <View style={[local_styles.progressStyle, { backgroundColor: mileage ? Colors.primary : '#fff' }]} /> : null}
                        {inspectionData.gas ? <View style={[local_styles.progressStyle, { backgroundColor: gas ? Colors.primary : '#fff' }]} /> : null}
                        {inspectionData.front_view ? <View style={[local_styles.progressStyle, { backgroundColor: frontView ? Colors.primary : '#fff' }]} /> : null}
                        {inspectionData.driver_side_view ? <View style={[local_styles.progressStyle, { backgroundColor: driverView ? Colors.primary : '#fff' }]} /> : null}
                        {inspectionData.passenger_side_view ? <View style={[local_styles.progressStyle, { backgroundColor: passengerView ? Colors.primary : '#fff' }]} /> : null}
                        {inspectionData.rear_view ? <View style={[local_styles.progressStyle, { backgroundColor: rearView ? Colors.primary : '#fff' }]} /> : null}
                        {inspectionData.handtruck ? <View style={[local_styles.progressStyle, { backgroundColor: handtruckView ? Colors.primary : '#fff' }]} /> : null}
                        {inspectionData.clean ? <View style={[local_styles.progressStyle, { backgroundColor: vehicleClean ? Colors.primary : '#fff' }]} /> : null}
                        {inspectionData.parking_ticket ? <View style={[local_styles.progressStyle, { backgroundColor: parkingOpacity == '1' ? Colors.primary : parkingOpacity == '0' ? Colors.primary : '#fff' }]} /> : null}
                        {inspectionData.gas_card ? <View style={[local_styles.progressStyle, { backgroundColor: gasCard ? Colors.primary : '#fff' }]} /> : null}
                        {inspectionData.engine_light ? <View style={[local_styles.progressStyle, { backgroundColor: engineLight ? Colors.primary : '#fff' }]} /> : null}
                        {inspectionData.windshield_fluid ? <View style={[local_styles.progressStyle, { backgroundColor: wiper ? Colors.primary : '#fff' }]} /> : null}
                        {inspectionData.rescue ? <View style={[local_styles.progressStyle, { backgroundColor: rescue ? Colors.primary : '#fff' }]} /> : null}
                        {inspectionData.phone ? <View style={[local_styles.progressStyle, { backgroundColor: phone ? Colors.primary : '#fff' }]} /> : null}
                        {inspectionData.phone_charger ? <View style={[local_styles.progressStyle, { backgroundColor: charger ? Colors.primary : '#fff' }]} /> : null}
                        <View style={[local_styles.progressStyle, { backgroundColor: comment ? Colors.primary : '#fff' }]} />
                    </View>
                    {inspectionData.mileage ?
                        (<View style={local_styles.milegeBox}>
                            <LinearGradient
                                colors={['#FFFFFF', '#ABC8FF', '#4F8BFF']}
                                start={{ x: 0, y: 0.5 }}
                                end={{ x: 1, y: 0.5 }}
                                style={local_styles.milegeinnerBox}>
                                <View>
                                    <Text style={[local_styles.milegeTxt, { color: Colors.light_black }]}>Mileage</Text>
                                    <TextInput
                                        style={[local_styles.melegeContainer, { color: Colors.gray }]}
                                        placeholder='000000'
                                        value={mileage}
                                        onChangeText={(text) => SetMilege(text)} />
                                </View>
                                <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                    {prevMilege ?
                                        <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                            <Text style={[local_styles.smallTxt, { color: Colors.white }]}>Last input</Text>
                                            <Text style={[local_styles.smallTxt1, { color: Colors.dark_chocklet }]}>Recorded on: {milegDate}</Text>
                                            <Text style={[local_styles.smallTxt, { color: Colors.dark_blue }]}>{prevMilege} m</Text>
                                        </View> : null}
                                    <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'flex-end' }}>
                                        {loading && indicatorName == 'milege' ? <View style={local_styles.btnView}>
                                            <ActivityIndicator size="small" color={Colors.white} />
                                        </View> :
                                            <TouchableOpacity style={local_styles.btnView} onPress={() => { updateInspection(); setIndicatorName('milege'); }}>
                                                <Text style={{ color: Colors.white, fontSize: 12, fontWeight: '500' }}>Save</Text>
                                            </TouchableOpacity>}
                                    </View>
                                </View>

                            </LinearGradient>
                        </View>) : null}

                    {inspectionData.gas ?
                        (
                            <View style={local_styles.gasBox}>
                                <LinearGradient
                                    colors={['#FFFFFF', '#ABC8FF', '#4F8BFF']}
                                    start={{ x: 0, y: 0.5 }}
                                    end={{ x: 1, y: 0.5 }}
                                    style={local_styles.gasInnerBox}>
                                    <View>
                                        <Text style={[local_styles.milegeTxt, { color: Colors.light_black }]}>Gas</Text>
                                        <TouchableOpacity style={local_styles.fuelView} onPress={() => SetgasModal(!gasModal)}>
                                            <Text style={[local_styles.fuelTxt, { color: Colors.gray }]}>{gas}</Text>
                                            <Image source={require("assets/images/downArrow.png")} />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                        {prevGas ?
                                            <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                                <Text style={[local_styles.smallTxt, { color: Colors.white }]}>Last input</Text>
                                                <Text style={[local_styles.smallTxt, { color: Colors.dark_blue }]}>{prevGas}</Text>
                                            </View> : null}

                                        <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'flex-end' }}>
                                            {loading && indicatorName == 'gas' ? <View style={local_styles.btnView}>
                                                <ActivityIndicator size="small" color={Colors.white} />
                                            </View> :
                                                <TouchableOpacity style={local_styles.btnView} onPress={() => { updateInspection(); setIndicatorName('gas'); }}>
                                                    <Text style={{ color: Colors.white, fontSize: 12, fontWeight: '500' }}>Save</Text>
                                                </TouchableOpacity>}
                                        </View>
                                    </View>

                                </LinearGradient>
                            </View>
                        ) : null}

                    {inspectionData.front_view ? <EditInspectBox
                        name={'Front View of vehicle'}
                        boxName={'frontView'}
                        // selectImage={() => SetCamModal(!camModal)}
                        selectImage={setCamera}
                        frontView={frontView}
                        prevView={prevFrontView}
                        milegDate={milegDate}
                        openDamage={openDamage}
                        loading={loading}
                        setIndicator={setIndicator}
                        indicatorName={indicatorName}
                        updateInspection={updateInspection} /> : null}

                    {inspectionData.driver_side_view ? <EditInspectBox
                        boxName={'driverView'}
                        name={'Driver’s Side view'}
                        frontView={driverView}
                        selectImage={setCamera}
                        openDamage={openDamage}
                        prevView={prevDriverView}
                        milegDate={milegDate}
                        loading={loading}
                        setIndicator={setIndicator}
                        indicatorName={indicatorName}
                        updateInspection={updateInspection} /> : null}

                    {inspectionData.passenger_side_view ? <EditInspectBox
                        boxName={'passengerView'}
                        name={'Passenger’s Side view'}
                        frontView={passengerView}
                        prevView={prevPassengerView}
                        milegDate={milegDate}
                        selectImage={setCamera}
                        openDamage={openDamage}
                        loading={loading}
                        setIndicator={setIndicator}
                        indicatorName={indicatorName}
                        updateInspection={updateInspection} /> : null}

                    {inspectionData.rear_view ? <EditInspectBox
                        boxName={'rearView'}
                        name={'Rear view'}
                        frontView={rearView}
                        prevView={prevRearView}
                        milegDate={milegDate}
                        selectImage={setCamera}
                        openDamage={openDamage}
                        loading={loading}
                        setIndicator={setIndicator}
                        indicatorName={indicatorName}
                        updateInspection={updateInspection} /> : null}

                    {inspectionData.handtruck ? <EditHandtruckBox
                        boxName={'handtruckView'}
                        name={'Do you have a working Handtruck?'}
                        frontView={handtruckView}
                        prevView={prevHandtruckView}
                        milegDate={milegDate}
                        selectImage={setCamera}
                        openDamage={openDamage}
                        loading={loading}
                        selctBox={isHandtruck}
                        yesBtn={yesBtn}
                        noBtn={noBtn}
                        setIndicator={setIndicator}
                        indicatorName={indicatorName}
                        updateInspection={updateInspection} /> : null}

                    {inspectionData.clean ? <EditTextBox
                        name={'Is the vehicle clean?'}
                        Boxname={'vehicleClean'}
                        activeOpacity={vehicleCleanOpacity}
                        milegDate={milegDate}
                        answerTxt={prevVehicleClean}
                        yesBtn={yesBtn}
                        noBtn={noBtn} /> : null}

                    {inspectionData.parking_ticket ? <EditTextBox
                        name={'Did you receive a Parking Ticket?'}
                        Boxname={'parkingTicket'}
                        activeOpacity={parkingOpacity}
                        milegDate={milegDate}
                        answerTxt={prevParking}
                        yesBtn={yesBtn}
                        noBtn={noBtn} /> : null}

                    {inspectionData.phone ? <EditTextBox
                        name={'Phone'}
                        Boxname={'phone'}
                        activeOpacity={phoneOpacity}
                        milegDate={milegDate}
                        answerTxt={prevPhone}
                        yesBtn={yesBtn}
                        noBtn={noBtn} /> : null}

                    {inspectionData.phone_charger ? <EditTextBox
                        name={'Phone Charger'}
                        Boxname={'charger'}
                        activeOpacity={chargerOpacity}
                        milegDate={milegDate}
                        answerTxt={prevCharger}
                        noBtn={noBtn}
                        yesBtn={yesBtn} /> : null}

                    {inspectionData.rescue ? <EditTextBox
                        name={'Asked to rescue?'}
                        Boxname={'rescue'}
                        activeOpacity={rescueOpacity}
                        milegDate={milegDate}
                        answerTxt={prevRescue}
                        yesBtn={yesBtn}
                        noBtn={noBtn} /> : null}

                    {inspectionData.gas_card ? <EditTextBox
                        name={'Gas Card'}
                        Boxname={'gasCard'}
                        activeOpacity={gasCardOpacity}
                        milegDate={milegDate}
                        answerTxt={prevGasCard}
                        yesBtn={yesBtn}
                        noBtn={noBtn} /> : null}

                    {inspectionData.engine_light ? <EditTextBox
                        name={'Engine Light'}
                        Boxname={'engineLight'}
                        activeOpacity={engineOpacity}
                        milegDate={milegDate}
                        answerTxt={prevEngineLight}
                        yesBtn={yesBtn}
                        noBtn={noBtn} /> : null}

                    {inspectionData.windshield_fluid ? <EditTextBox
                        name={'Is Windshield Fluid sufficient?'}
                        Boxname={'wiper'}
                        activeOpacity={wiperOpacity}
                        milegDate={milegDate}
                        answerTxt={prevWiper}
                        yesBtn={yesBtn}
                        noBtn={noBtn} /> : null}
                </View>
                <SubInspectModal checkoutModal={checkoutModal} checkoutInspection={updateInspection} />
                <GasModal gasList={gasList} setGasTank={setGasTank} gasModal={gasModal} />
                <CameraModal camModal={camModal} setCamera={setCamera} />
                <ParkingModal parkingModal={parkingModal} setParkingTicket={setParkingTicket} closeParkingModal={closeParkingModal} subTicket={subTicket} setCamera={setCamera} />
                <RescueModal rescueModal={rescueModal} subTicket={subTicket} setRescueTxt={setRescueTxt} />
                {/* <DamageModal headTxt={headerTxt} name={name} openDamageModal={openDamageModal}/> */}
                <View style={{ padding: 20 }}>
                    <TextInputField
                        placeholder={'Additional comments (optional)'}
                        styles={[Form.TextInput]}
                        onChangeText={(text: string) => {
                            setComment(text)
                        }}
                    />
                </View>

                <View style={{ alignItems: 'center', marginTop: 46, marginBottom: 20 }}>
                    <Button styles={{ width: 286 }} name={'Submit Inspection'} onPress={() => setCheckoutModal(!checkoutModal)} />
                </View>


                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={openDamageModal}>
                    <View style={local_styles.ModalStyle}>
                        <Loader loading={loading} />
                        <View style={local_styles.warnmainModalView}>
                            <TouchableOpacity style={{ alignItems: 'flex-end' }} onPress={closeDamageModal}>
                                <FontAwesomeIcon icon={faTimes} size={20} color={Colors.primary} />
                            </TouchableOpacity>
                            <Text style={[local_styles.dmgTxt, { color: Colors.primary }]}>{name}</Text>
                            <View style={{ alignItems: 'center', marginTop: 10 }}>
                                {imageModal ? (
                                    <TouchableOpacity onPress={() => setCamera(headerTxt)}>
                                        <Image source={{ uri: imageModal }} style={local_styles.imgBox} />
                                    </TouchableOpacity>
                                ) : (<TouchableOpacity style={local_styles.imgBox} onPress={() => setCamera(headerTxt)}>
                                    <Image source={require("assets/images/plus.png")} style={local_styles.plusImg} />
                                </TouchableOpacity>)}
                            </View>
                            <View style={{ width: "80%" }}>
                                <TextInputField
                                    placeholder={translate('damageDetails')}
                                    styles={[Form.TextInput, local_styles.textInput]}
                                    // value={damageDetails}
                                    onChangeText={(text: string) => {
                                        setDamageDetails(text)
                                    }}
                                />
                            </View>
                            <View style={local_styles.damageBox}>
                                <Text style={[local_styles.damgTxt, { color: Colors.light_gray }]}>{translate('chooseSeverity')}</Text>
                            </View>
                            <View style={local_styles.damageInnerBox}>
                                {damageList.map((item, index) => {
                                    return (
                                        <TouchableOpacity key={'damage_' + index} style={local_styles.boxView} onPress={() => selctGamage(item.backgroungColor)}>
                                            <View style={[local_styles.ovalView, { backgroundColor: item.backgroungColor }]}>
                                                {flag == item.backgroungColor ?
                                                    <Image source={require("assets/images/check.png")} style={local_styles.checkImg} /> : null}
                                            </View>

                                            <View style={{ marginTop: 10, width: 50 }}>
                                                <Text style={[local_styles.smallTxt, { color: Colors.gray, textAlign:'center' }]}>{item.name}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    )
                                })}
                            </View>
                            <View style={local_styles.bottomBoxInner}>
                                <Button styles={local_styles.btnStyle} name={translate('subDamage')} onPress={uploadDamage} />
                            </View>
                        </View>
                    </View>
                </Modal>

            </ScrollView>
        </View>
    );
};

const local_styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },
    topBox: {
        marginTop: 20,
        marginRight: 31,
        marginLeft: 17
    },
    milegeBox: {
        height: 129,
        width: width - 14,
        // marginTop: 20,
        elevation: 5
    },
    milegeinnerBox: {
        height: 129,
        width: width - 14,
        borderRadius: 18,
        flexDirection: 'row',
        // alignItems: 'center',
        justifyContent: 'space-between',
        // paddingRight: 10,
        padding: 17
    },
    milegeTxt: {
        fontSize: 16,
        fontWeight: '500'
    },
    milegeTxt1: {
        fontSize: 12,
        fontWeight: '500',
        paddingTop: 8
    },
    smallTxt: {
        fontSize: 10,
        fontWeight: '300',
        paddingBottom: 3
        // textAlign:'left'
    },
    smallTxt1: {
        fontSize: 8,
        fontWeight: '300',
        paddingBottom: 5
    },
    gasBox: {
        width: width - 14,
        height: 115,
        borderRadius: 17,
        marginTop: 20,
        elevation: 5
    },
    gasInnerBox: {
        width: width - 14,
        height: 115,
        borderRadius: 17,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 17
    },
    barStyle: {
        height: 19,
        width: width - 20,
        borderWidth: 1,
        borderRadius: 20,
        marginBottom: 20,
        borderColor: '#4F8BFF',
        overflow: 'hidden',
        flexDirection: 'row',
        alignItems: 'center'
    },
    melegeContainer: {
        height: 40,
        width: 88,
        borderRadius: 8,
        borderWidth: 1,
        borderColor: '#858585',
        marginTop: 12,
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 20
    },
    btnView: {
        height: 30,
        width: 70,
        borderRadius: 20,
        backgroundColor: '#4F8BFF',
        marginTop: 24,
        marginRight: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    fuelView: {
        height: 40,
        width: 88,
        borderRadius: 8,
        borderWidth: 1,
        borderColor: '#858585',
        marginTop: 12,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        // padding: 5,
        paddingLeft: 15,
        paddingRight: 15
    },
    fuelTxt: {
        fontSize: 16,
        fontWeight: '500',
        color: '#5B5B5B'
    },
    ModalStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)'
    },
    warnmainModalView: {
        backgroundColor: "#fff",
        width: "80%",
        borderRadius: 10,
        padding: 20,
        paddingBottom: 40,
        height: 570,
        justifyContent: 'flex-start'
    },
    dmgTxt: {
        fontSize: 15,
        fontWeight: '400'
    },
    imgBox: {
        height: 122,
        width: 122,
        borderRadius: 122 / 2,
        backgroundColor: '#D5D5D5',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 14
    },
    plusImg: {
        height: 31,
        width: 31,
        resizeMode: 'contain'
    },
    textInput: {
        marginTop: 10,
    },
    damageInnerBox: {
        marginTop: 31,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 59
    },
    boxView: {
        height: 95,
        width: 75,
        borderRadius: 9,
        backgroundColor: '#C4C4C4',
        alignItems: 'center'
    },
    ovalView: {
        height: 29,
        width: 29,
        borderRadius: 29 / 2,
        marginTop: 18,
        alignItems: 'center',
        justifyContent: 'center'
    },
    checkImg: {
        height: 10,
        width: 14,
        resizeMode: 'contain'
    },
    damageBox: {
        marginTop: 42
    },
    damgTxt: {
        fontSize: 14,
        fontWeight: '400'
    },
    bottomBoxInner: {
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        // marginTop: 50,
        width: "100%"
    },
    btnStyle: {
        width: 138
    },
    progressStyle: {
        borderWidth: 1,
        borderColor: '#4F8BFF',
        height: 19,
        flex: 1,
        // width: width * 26 / 414
    }
});

const mapStateToProps = (state: any) => ({
    auth: state.auth,
    driver: state.driver
})
export default connect(mapStateToProps)(InspectionForm);

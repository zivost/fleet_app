/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useEffect, useState, useRef } from "react";
import moment from "moment";
import type { StackParamList } from "../../routes.types";
import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import auth from '@react-native-firebase/auth';
import { useFocusEffect } from '@react-navigation/native';
// import {useRoute} from '@react-navigation/native';
import {
    StyleSheet,
    useColorScheme,
    View,
    Image,
    Text,
    TouchableOpacity,
    BackHandler,
    Alert,
    RefreshControl,
    ScrollView
} from 'react-native';
import { human } from 'react-native-typography'
import Button from "../../../Components/Button/index"
import {
    Colors,
    Typography,
    setI18nConfig,
    translate,
    Form
} from "../../config";
import DatePicker from 'react-native-datepicker'
import { WeeklyReport, SummaryBox } from '../../../Components/WeeklyReport/index'
import { VehicleDamages, Vehicles, Dispatcher, Drivers } from '../../../Components/Summary/index'
import Loader from '../../../Components/Loader/index'
import { useDispatch, useSelector, connect } from 'react-redux'
import constants from '../../constants';

const Auth = auth;

type Props = NativeStackScreenProps<StackParamList, 'Dashboard'>;
const Dashboard: FC<Props> = ({ navigation, auth, driver, route }) => {
    const today = moment().format("YYYY-MM-DD");
    let myDatepicker: { onPressDate: () => void; };

    const dispatch = useDispatch();
    const [date, setDate] = useState(today);
    const [driverData, setDriverData] = useState<any[]>([]);
    const [dispatchData, setDispatchData] = useState<any[]>([]);
    const [damageData, setDamageData] = useState<any[]>([]);
    const [vehicleData, setVehicleData] = useState<any[]>([]);
    const [vehicleMetaData, setVehicleMetaData] = useState();
    const [driverMetaData, setDriverMetaData] = useState<any[]>([]);
    const [dispatchMetaData, setDispatchMetaData] = useState<any[]>([]);
    const [damageMetaData, setDamageMetaData] = useState<any[]>([]);
    const [statsData, setStatsData] = useState({});
    const [pageRefresh, setPageRefresh] = useState(false)
    const datepicker = useRef(null);

    useEffect(
        () => {
            let { logged_in } = auth;
            console.log("check login");
            if (!Auth().currentUser) {
                logged_in = false;
            }
            if (!logged_in) {
                navigation.navigate('SignIn');
            }
        },
        []
    );

    useEffect(() => {
        getDriverList()
        const backHandler = BackHandler.addEventListener(
            "hardwareBackPress", navBack
        );

        return () => backHandler.remove();
    }, []);
    useEffect(() => {
        // if (navigation.isFocused()) {
            getDriverList()
        // }
    }, [driver.last_update]);

    useEffect(() => {
        let arr = { name: '' }
        let secArr = []
        if (driver.driverList && driver.driverList.length > 0) {
            secArr = driver.driverList
            secArr.push(arr)
            setDriverData(secArr)
            setDriverMetaData(driver.driverListMetaData)
        } else {
            secArr.push(arr)
            setDriverData(secArr)
        }
    }, [driver.driverList])

    useEffect(() => {
        let arr = {}
        let secArr = []
        if (driver.dispatcherList && driver.dispatcherList.length > 0) {
            secArr = driver.dispatcherList
            secArr.push(arr)
            setDispatchData(secArr)
            setDispatchMetaData(driver.dispatcherListmetaData)
        } else {
            secArr.push(arr)
            setDispatchData(secArr)
        }
    }, [driver.dispatcherList])

    useEffect(() => {
        let arr = { "vehicle": { "name": '' } }
        let secArr = []
        if (driver.damagesList && driver.damagesList.length > 0) {
            secArr = driver.damagesList
            secArr.push(arr)
            setDamageData(secArr)
            setDamageMetaData(driver.damageListmetaData)
        } else {
            secArr.push(arr)
            setDamageData(secArr)
        }
    }, [driver.damagesList])

    useEffect(() => {
        let arr = { name: '' }
        let secArr = []
        if (driver.vehicleList && driver.vehicleList.length > 0) {
            secArr = driver.vehicleList
            secArr.push(arr)
            setVehicleData(secArr)
            setVehicleMetaData(driver.vehicleListmetaData)
        } else {
            secArr.push(arr)
            setVehicleData(secArr)
        }
    }, [driver.vehicleList])

    useEffect(() => {
        if (driver.statsData) {
            setStatsData(driver.statsData)
        }
    }, [driver.statsData])

    const getDriverList = () => {
        let params = '&limit=20&offset=0';
        dispatch({
            type: constants("driver").sagas.getDriverList,
            payload: {
                data: params,
                sessionToken: auth.user.token,
            }
        })
        dispatch({
            type: constants("driver").sagas.getDispatchersList,
            payload: {
                data: params,
                sessionToken: auth.user.token,
            }
        })
        dispatch({
            type: constants("driver").sagas.getDamagesList,
            payload: {
                data: params,
                sessionToken: auth.user.token,
            }
        })
        dispatch({
            type: constants("driver").sagas.getVehiclesList,
            payload: {
                data: params,
                sessionToken: auth.user.token,
            }
        })
        dispatch({
            type: constants("driver").sagas.getStats,
            payload: {
                sessionToken: auth.user.token,
            }
        })
    }

    const onRefresh = () => {
        setPageRefresh(true)
        dispatch({
            type: constants("driver").reducers.setDashboardData.set
        });
        getDriverList();
        setTimeout(() => {
            setPageRefresh(false)
        }, 4000);
    }

    const navBack = () => {
        Alert.alert(
            ' Exit From App ',
            ' Do you want to exit From App ?',
            [
                { text: 'Yes', onPress: () => BackHandler.exitApp() },
                { text: 'No', onPress: () => console.log('NO Pressed') }
            ],
            { cancelable: false },
        );

        // Return true to enable back button over ride.
        return true;
    }

    // const openCalender = () => {
    //     setDatepicker(!datepiker)
    // }

    // console.log(datepiker,"datepiker")
    const isDarkMode = useColorScheme() === 'dark';
    let date_text = translate('today');
    if (today !== date) {
        date_text = moment(date).format('Do MMM YY');
    }

    const navToDamageDetails = (data: any) => {
        navigation.navigate('DamageDetails', { data })
    }

    const navToCarProfile = (data, screen) => {
        navigation.navigate('Profile', { data, screen })
    }

    let { loading } = driver;
    return (
        <View style={[local_styles.mainContainer, { backgroundColor: (isDarkMode) ? "#fff" : "#fff" }]}>
            <Loader loading={loading} />
            <ScrollView
                refreshControl={
                    <RefreshControl
                        refreshing={pageRefresh}
                        onRefresh={onRefresh}
                    />
                }
                showsVerticalScrollIndicator={false}>
                <View style={local_styles.topView}>
                    <View style={local_styles.topBox}>
                        <TouchableOpacity onPress={() => navigation.navigate('Menu')}>
                            <Image style={local_styles.bckimage} source={require("assets/images/menu.png")} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => myDatepicker.onPressDate()}>
                            <View style={local_styles.datePicker}>
                                <Text style={[local_styles.inspectionTxt, { color: Colors.gray }]}>{date_text}</Text>
                                <Image style={local_styles.arrimage} source={require("assets/images/downArrow.png")} />
                            </View>
                        </TouchableOpacity>

                        <DatePicker
                            ref={(datepicker: any) => myDatepicker = datepicker}
                            style={{ width: 0, height: 0 }}
                            // date={birthday.value}
                            mode="date"
                            duration={300}
                            format="YYYY-MM-DD"
                            showIcon={false}
                            hideText={true}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateInput: {
                                    borderWidth: 0,
                                    alignItems: "flex-start",
                                },
                                dateText: {
                                    // ...styles.dateFieldText,
                                },
                                btnTextConfirm: {
                                    color: "#000000",
                                },
                            }}
                            onDateChange={(_date: string) => { setDate(_date) }}
                        />
                    </View>

                    <View>
                        <Button styles={local_styles.btnStyle} name={`${date_text}'s ${translate('inspection')}`} onPress={() => navigation.navigate('InspectCar', { date: date, date_text: date_text })} />
                    </View>
                </View>

                <View style={local_styles.summaryBox}>
                    <Text style={[local_styles.summaryTxt, { color: Colors.primary }]}>{translate('summary')}</Text>
                    <SummaryBox statsData={statsData} />
                    {/* <WeeklyReport /> */}
                    <VehicleDamages
                        damageData={damageData}
                        length={damageData.length}
                        damageMetaData={damageMetaData}
                        navToCarProfile={navToDamageDetails}
                        addDamage={() => navigation.navigate('AddDamage')}
                        navToSeeall={() => navigation.navigate('Vehiclelist', { params: 'Damage' })} />
                    <Vehicles
                        vehicleData={vehicleData}
                        length={vehicleData.length}
                        vehicleMetaData={vehicleMetaData}
                        navToCarProfile={navToCarProfile}
                        addVehicle={() => navigation.navigate('AddVehicle')}
                        navToSeeall={() => navigation.navigate('Vehiclelist', { params: 'Vehicle' })}
                    />
                    <Dispatcher
                        dispatchData={dispatchData}
                        length={dispatchData.length}
                        role={auth.user.role}
                        navToCarProfile={navToCarProfile}
                        addDispatcher={() => navigation.navigate('AddDispatcher')}
                        navToSeeall={() => navigation.navigate('Vehiclelist', { params: 'Dispatchers' })}
                    />
                    <Drivers
                        driverData={driverData}
                        length={driverData.length}
                        driverMetaData={driverMetaData}
                        navToCarProfile={navToCarProfile}
                        addDriver={() => navigation.navigate('AddDriver')}
                        navToSeeall={() => navigation.navigate('Vehiclelist', { params: 'Drivers' })} />

                </View>
            </ScrollView>
        </View>
    );
};

const local_styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },
    topBox: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    datePicker: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
    },
    bckimage: {
        height: 11,
        width: 17,
        resizeMode: 'contain'
    },
    topView: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 32,
        marginLeft: 20,
        marginRight: 24,
        justifyContent: 'space-between'
    },
    inspectionTxt: {
        // ...human.caption2Object,
        fontWeight: '500',
        marginLeft: 18,
        fontSize: 15,
        marginRight: 5
    },
    summaryTxt: {
        // ...human.caption2Object,
        fontWeight: '500',
        fontSize: 15,
    },
    btnStyle: {
        paddingHorizontal: 15
    },
    arrimage: {
        width: 13,
        height: 7
    },
    summaryBox: {
        marginTop: 15,
        marginLeft: 19,
        marginRight: 19,
        paddingVertical: 10
    }
});

const mapStateToProps = (state: any) => ({
    auth: state.auth,
    driver: state.driver
})
export default connect(mapStateToProps)(Dashboard);
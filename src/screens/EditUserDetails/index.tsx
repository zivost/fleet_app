/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useEffect, useState } from "react";
import {
    StyleSheet,
    useColorScheme,
    View,
    Image,
    Text,
    BackHandler,
    ScrollView
} from 'react-native';
import {
    Colors,
    Typography,
    setI18nConfig,
    translate,
    Form,
    goBack
} from "../../config";
import type { StackParamList } from "../../routes.types";
import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import TextInputField from '../../../Components/TextInput/index'
import { connect, useStore, useDispatch } from "react-redux";
import Header from "../../../Components/Header/index"
import Loader from '../../../Components/Loader/index'
import Button from "../../../Components/Button/index"
import constants from '../../constants';
let emailRegx = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;

type Props = NativeStackScreenProps<StackParamList, 'EditUserDetails'>;
const EditUserDetails: FC<Props> = ({ navigation, auth, route }) => {

    const data = route?.params?.user_data || 'nodata';
    const store = useStore();
    const dispatch = useDispatch();

    const [email, setEmail] = useState({ value: "", error: false, msg: "" });
    const [name, setName] = useState({ value: "", error: false, msg: "" });
    const [phone, setPhone] = useState({ value: "", error: false, msg: "" });
    const [id, setUser_id] = useState('');

    useEffect(() => {
        if (data != 'nodata') {
            setEmail({ value: data.email, error: false, msg: "" })
            setName({ value: data.name, error: false, msg: "" })
            setPhone({ value: data.phone, error: false, msg: "" })
            setUser_id(data.user_id)
        }
    }, [data]);

    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };
    }, []);

    const handleBackButtonClick = () => {
        navigation.goBack(null);
        return true;
    }

    const updateProfile = () => {
        if (email.value == "") {
            setEmail({
                ...email,
                error: true,
                msg: "Email is required!"
            })
        }
        if (name.value == "") {
            setName({
                ...name,
                error: true,
                msg: "Name is required!"
            })
        }
        // if (phone.value == "") {
        //     setPhone({
        //         ...phone,
        //         error: true,
        //         msg: "Phone is required!"
        //     })
        // }
        if (email.error || name.error || name.value == "" || email.value == "") {
            return false;
        }

        dispatch({
            type: constants("auth").sagas.editUserDetails,
            payload: {
                data: {
                    "name": name.value,
                    // "phone": phone.value,
                },
                sessionToken: auth.user.token,
                navigation
            },
            getProfileDetails: getProfileDetails,
        })
    }

    const getProfileDetails = () => {
        dispatch({
            type: constants("auth").sagas.getProfile,
            payload: {
                sessionToken: auth.user.token,
                navigation
            }
        })
    }

    let { loading } = auth;
    const isDarkMode = useColorScheme() === 'dark';
    return (
        <View style={[local_styles.mainContainer, { backgroundColor: (isDarkMode) ? "#fff" : "#fff" }]}>
            <Loader loading={loading} />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={local_styles.bottomBox}>
                    <Header
                        title={'Edit User Details'}
                        goBack={navigation.goBack}
                    />

                    <View style={{ marginTop: 50 }}>

                        <TextInputField
                            placeholder={"Name"}
                            styles={[Form.TextInput]}
                            defaultValue={name.value}
                            value={name.value}
                            error={name.error}
                            keyboardType='email-address'
                            errorMsg={name.msg}
                            onChangeText={(text: string) => {
                                setName({ value: text, error: false, msg: "" })
                            }}
                        />

                        <View pointerEvents={data != 'nodata' ? "none" : 'auto'}>
                            <TextInputField
                                placeholder={'Enter your Email Id'}
                                styles={[Form.TextInput]}
                                defaultValue={email.value}
                                value={email.value}
                                keyboardType='email-address'
                                error={email.error}
                                errorMsg={email.msg}
                                onPress={() => setEmail({ value: "", error: false, msg: "" })}
                                onChangeText={(text: string) => {
                                    let obj = { ...email }
                                    if (emailRegx.test(text) === true) {
                                        obj = { value: text, error: false, msg: "" }
                                    } else {
                                        obj = { value: text, error: true, msg: translate('pleaseEnterValidEmail') }
                                    }
                                    setEmail(obj)
                                }}
                            />
                        </View>
                        <View pointerEvents={data != 'nodata' ? "none" : 'auto'}>
                            <TextInputField
                                placeholder={"Phone"}
                                styles={[Form.TextInput, local_styles.textInput]}
                                defaultValue={phone.value}
                                value={phone.value}
                                error={phone.error}
                                keyboardType='email-address'
                                errorMsg={phone.msg}
                                onChangeText={(text: string) => {
                                    setPhone({ value: text, error: false, msg: "" })
                                }}
                            />
                        </View>
                    </View>
                    <View style={local_styles.bottomBoxInner}>
                        <Button styles={{ width: 138 }} onPress={() => updateProfile()} name={"Save"} />
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};


const local_styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },
    headerBox: {
        marginLeft: 15,
        marginTop: 15,
        marginRight: 15
    },
    userBox: {
        marginTop: 30,
        alignItems: 'center'
    },
    userImg: {
        height: 145,
        width: 133,
        resizeMode: 'contain',
        marginBottom: 10
    },
    userTxt: {
        fontSize: 18,
        fontWeight: '500'
    },
    user_info_box: {
        marginBottom: 20,
        marginLeft: 48,
        marginRight: 20,
        flexDirection: 'row'
    },
    menuTxt: {
        fontSize: 14,
        fontWeight: '500'
    },
    smallTxt: {
        fontSize: 12,
        fontWeight: '500',
        textDecorationLine: 'underline'
    },
    bottomBox: {
        width: "100%",
        backgroundColor: "#fff",
        // borderTopLeftRadius: 24,
        // borderTopRightRadius: 24,
        paddingTop: 20,
        paddingLeft: 25,
        paddingRight: 35,
    },
    textInput: {
        marginTop: 10,
    },
    bottomBoxInner: {
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        marginTop: 50,
        width: "100%"
    },
});

const mapStateToProps = (state: any) => ({
    auth: state.auth,
})
export default connect(mapStateToProps)(EditUserDetails);
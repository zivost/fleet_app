/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useState, useEffect } from "react";
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    PermissionsAndroid,
    BackHandler
} from 'react-native';

import {
    Colors,
    Typography,
    setI18nConfig,
    translate,
    Form
} from "../../config";
import Loader from '../../../Components/Loader/index'
import { iOSUIKit, human } from 'react-native-typography'
import TextInputField from '../../../Components/TextInput/index'
import Button from "../../../Components/Button/index"
import Header from "../../../Components/Header/index"
import type { StackParamList } from "../../routes.types";
import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import { useDispatch, useSelector, connect } from 'react-redux'
import constants from '../../constants';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';

let width = Dimensions.get('window').width;
let height = Dimensions.get('window').height;

let emailRegx = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;

type Props = NativeStackScreenProps<StackParamList, 'AddDispatcher'>;
const AddDispatcher: FC<Props> = ({ navigation, auth, driver, route }) => {
    const isDarkMode = useColorScheme() === 'dark';
    const data = route?.params?.data || 'nodata';
    const dispatch = useDispatch();
    console.log('/=====', data)
    const [email, setEmail] = useState({ value: "", error: false, msg: "" });
    const [name, setName] = useState({ value: "", error: false, msg: "" });
    const [phone, setPhone] = useState({ value: "", error: false, msg: "" });
    const [password, setPassword] = useState({ value: "", error: false, msg: "" });
    const [confirmPass, setConfirmPass] = useState({ value: "", error: false, msg: "" });
    const [dispatcherPic, setDispatcherPic] = useState('');
    const [dispid, setId] = useState('');

    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };
    }, []);

    const handleBackButtonClick = () => {
        navigation.goBack(null);
        return true;
    }


    useEffect(() => {
        if (data != 'nodata') {
            setPhone({ value: data.phone, error: false, msg: "" })
            setName({ value: data.name, error: false, msg: "" })
            setId(data.id)
            setEmail({ value: data.email, error: false, msg: "" })
        }
    }, [data])

    const addDriverProfil = () => {
        if (email.value == "") {
            setEmail({
                ...email,
                error: true,
                msg: "Email is required!"
            })
        }
        if (password.value == "") {
            setPassword({
                ...password,
                error: true,
                msg: "Password is required!"
            })
        }
        if (email.error || password.error || password.value == "" || email.value == "") {
            return false;
        }
        let carid = ''
        if (data != 'nodata') {
            carid = dispid
        }
        if (email.value && !email.error && phone.value && password.value && !password.error && confirmPass.value && !confirmPass.error) {
            dispatch({
                type: constants("driver").sagas.createDispatcher,
                payload: {
                    data: {
                        "name": name.value,
                        "email": email.value,
                        "phone": phone.value,
                        "password": password.value,
                        "images": [
                            dispatcherPic
                        ]
                    },
                    id: carid,
                    sessionToken: auth.user.token,
                    navigation
                }
            })
        }
    }

    const imgUpload = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                    title: "App Camera Permission",
                    message: "App needs access to your camera ",
                    buttonNeutral: "Ask Me Later",
                    buttonNegative: "Cancel",
                    buttonPositive: "OK"
                }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                let options = {
                    maxWidth: 1000, //speeds up compressImage to almost no time
                    maxHeight: 1000,
                };
                launchCamera(options, (response) => {
                    if (response && response.assets && response.assets.length > 0) {
                        let img = response.assets[0];
                        // setDamageImg(img)
                        dispatch({
                            type: constants("driver").sagas.uploadDamageImg,
                            payload: {
                                data: {
                                    file: img,
                                },
                                type: 'dispatcherPic',
                                sessionToken: auth.user.token,
                            },
                            setImg: setImg
                        })
                    }
                })
            } else {
                console.log("Camera permission denied");
            }
        } catch (err) {
            console.warn(err);
        }
    }

    const setImg = (data, type) => {
        if (type == 'dispatcherPic') {
            setDispatcherPic(data.data)
        }

    }
    let { loading } = driver;
    return (
        <View style={[local_styles.mainContainer, { backgroundColor: (isDarkMode) ? "#fff" : "#fff" }]}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={local_styles.bottomBox}>
                    <Header
                        title={data != 'nodata' ? translate('editDispatch') : translate('addDispatch')}
                        goBack={navigation.goBack} />
                    <View style={local_styles.inputView}>
                        <View style={{ alignItems: 'center', marginBottom: 40 }}>
                            {dispatcherPic ? (
                                <Image source={{ uri: dispatcherPic }} style={local_styles.imgBox} />
                            ) :
                                (<TouchableOpacity style={local_styles.imgBox} onPress={imgUpload}>
                                    <Image source={require("assets/images/plus.png")} style={local_styles.plusImg} />
                                </TouchableOpacity>)}
                            <Text style={[local_styles.addTxt, { color: Colors.light_gray }]}>{translate('profilePic')}</Text>
                        </View>

                        <TextInputField
                            placeholder={translate('name')}
                            styles={[Form.TextInput, local_styles.textInput]}
                            value={name.value}
                            onChangeText={(text: string) => {
                                setName({ value: text, error: false, msg: "" });
                            }}
                        />

                        <View pointerEvents={data != 'nodata' ? "none" : 'auto'}>
                            <TextInputField
                                placeholder={translate('email')}
                                styles={[Form.TextInput, local_styles.textInput]}
                                defaultValue={email.value}
                                value={email.value}
                                keyboardType='email-address'
                                error={email.error}
                                errorMsg={email.msg}
                                onChangeText={(text: string) => {
                                    let obj = { ...email }
                                    if (emailRegx.test(text) === true) {
                                        obj = { value: text, error: false, msg: "" }
                                    } else {
                                        obj = { value: text, error: true, msg: translate('pleaseEnterValidEmail') }
                                    }
                                    setEmail(obj)
                                }}
                            />
                        </View>

                        <TextInputField
                            placeholder={translate('phoneNo')}
                            styles={[Form.TextInput, local_styles.textInput]}
                            keyboardType='phone-pad'
                            value={phone.value}
                            onChangeText={(text: string) => {
                                setPhone({ value: text, error: false, msg: "" })
                            }}
                        />

                        <TextInputField
                            placeholder={translate('password')}
                            styles={[Form.TextInput, local_styles.textInput]}
                            defaultValue={password.value}
                            secureTextEntry={true}
                            error={password.error}
                            errorMsg={password.msg}
                            onChangeText={(text: string) => {
                                setPassword({ value: text, error: false, msg: "" })
                            }}
                        />
                        <TextInputField
                            placeholder={translate('confirmPass')}
                            styles={[Form.TextInput, local_styles.textInput]}
                            defaultValue={confirmPass.value}
                            secureTextEntry={true}
                            error={confirmPass.error}
                            errorMsg={confirmPass.msg}
                            onChangeText={(text: string) => {
                                let obj = { ...confirmPass }
                                if (text == password.value) {
                                    obj = { value: text, error: false, msg: "" }
                                } else {
                                    obj = { value: text, error: true, msg: translate('passwordDontMatch') }
                                }
                                setConfirmPass(obj)
                            }}
                        />

                    </View>
                    <View style={local_styles.bottomBoxInner}>
                        <Button styles={local_styles.btnStyle} name={data != 'nodata' ? translate('save') : translate('addDispatch')} onPress={() => { addDriverProfil() }} />
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};

const local_styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    imgBox: {
        height: 122,
        width: 122,
        borderRadius: 122 / 2,
        backgroundColor: '#D5D5D5',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 14
    },
    addTxt: {
        fontSize: 16,
        fontWeight: '400'
    },
    plusImg: {
        height: 31,
        width: 31,
        resizeMode: 'contain'
    },
    damageBox: {
        marginTop: 42
    },
    damgTxt: {
        fontSize: 14,
        fontWeight: '400'
    },
    damageInnerBox: {
        marginTop: 31,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 59
    },
    boxView: {
        height: 87,
        width: 70,
        borderRadius: 9,
        backgroundColor: '#C4C4C4',
        alignItems: 'center'
    },
    ovalView: {
        height: 29,
        width: 29,
        borderRadius: 29 / 2,
        marginTop: 18,
        alignItems: 'center',
        justifyContent: 'center'
    },
    checkImg: {
        height: 10,
        width: 14,
        resizeMode: 'contain'
    },
    smallTxt: {
        fontSize: 7,
        fontWeight: '500',
        textAlign: 'center'
    },
    bottomBox: {
        // position:"absolute",
        width: "100%",
        // height: height / 1.7,
        backgroundColor: "#fff",
        borderTopLeftRadius: 24,
        borderTopRightRadius: 24,
        // bottom:0,
        paddingTop: 40,
        paddingLeft: 35,
        paddingRight: 35,
    },
    headingText: {
        ...human.title1Object,
        color: "#4F8BFF",
        fontWeight: "bold"
    },
    inputView: {
        marginTop: 50
    },
    textInput: {
        marginTop: 10,
    },
    bottomBoxInner: {
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        marginTop: 50,
        width: "100%",
        marginBottom: 20
    },
    btnStyle: {
        width: 138
    },
    cmpnyTxt: {
        fontSize: 10,
        fontWeight: '400'
    }
});
const mapStateToProps = (state: any) => ({
    auth: state.auth,
    driver: state.driver
})
export default connect(mapStateToProps)(AddDispatcher);

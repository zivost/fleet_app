/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useState, useEffect } from "react";
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    PermissionsAndroid,
    Modal,
    TextInput,
    FlatList,
    BackHandler
} from 'react-native';

import {
    Colors,
    Typography,
    setI18nConfig,
    translate,
    Form
} from "../../config";
import Loader from '../../../Components/Loader/index'
import { iOSUIKit, human } from 'react-native-typography'
import TextInputField from '../../../Components/TextInput/index'
import Button from "../../../Components/Button/index"
import Header from "../../../Components/Header/index"
import type { StackParamList } from "../../routes.types";
import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import { useDispatch, useSelector, connect } from 'react-redux'
import constants from '../../constants';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
let width = Dimensions.get('window').width;
let height = Dimensions.get('window').height;



type Props = NativeStackScreenProps<StackParamList, 'AddDamage'>;
const AddDamage: FC<Props> = ({ navigation, auth, driver, route }) => {
    const isDarkMode = useColorScheme() === 'dark';
    const data = route?.params?.data || 'nodata';

    const damageList = [{ "backgroungColor": 'red', "name": 'Severely Damaged' },
    { "backgroungColor": 'yellow', "name": 'Moderately Damaged' },
    { "backgroungColor": 'green', "name": 'Good Condition' }]

    const dispatch = useDispatch();
    const [damageDetails, setDamageDetails] = useState('')
    const [damageImg, setDamageImg] = useState('')
    const [vehicleData, setVehicleData] = useState('')
    const [vehicleId, setVehicleId] = useState('')
    const [vehicleName, setVehicleName] = useState('')
    const [flag, setFlag] = useState('')
    const [damgid, setDamageId] = useState('')
    const [vehicleModal, openVehicleModal] = useState(false)

    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };
    }, []);

    const handleBackButtonClick = () => {
        navigation.goBack(null);
        return true;
    }
    
    useEffect(() => {
        let arr = {}
        let secArr = []
        if (driver.vehicleList && driver.vehicleList.length > 0) {
            secArr = driver.vehicleList
            secArr.pop()
            setVehicleData(secArr)
        }
    }, [driver.vehicleList])

    useEffect(() => {
        if (data != "nodata") {
            setFlag(data.flag)
            setDamageDetails(data.detail)
            setDamageImg(data.image[0])
        }
    }, [data])


    const selctGamage = (data) => {
        setFlag(data)
    }

    const uploadDamage = () => {
        let carid = ''
        if (data != 'nodata') {
            carid = damgid
        }
        if (vehicleId && flag && damageDetails && damageImg) {
            dispatch({
                type: constants("driver").sagas.addDamage,
                payload: {
                    data: {
                        "vehicle": vehicleId,
                        "flag": flag,
                        "detail": damageDetails,
                        "images": [
                            damageImg
                        ]
                    },
                    // id:carid,
                    sessionToken: auth.user.token,
                    navigation
                }
            })
        }
    }


    const imgUpload = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                    title: "App Camera Permission",
                    message: "App needs access to your camera ",
                    buttonNeutral: "Ask Me Later",
                    buttonNegative: "Cancel",
                    buttonPositive: "OK"
                }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                let options = {
                    maxWidth: 1000, //speeds up compressImage to almost no time
                    maxHeight: 1000,
                };
                launchCamera(options, (response) => {
                    if (response && response.assets && response.assets.length > 0) {
                        let img = response.assets[0];
                        // setDamageImg(img)
                        dispatch({
                            type: constants("driver").sagas.uploadDamageImg,
                            payload: {
                                data: {
                                    file: img,
                                },
                                type: 'damageImg',
                                sessionToken: auth.user.token,
                            },
                            setImg: setImg
                        })
                    }
                })
            } else {
                console.log("Camera permission denied");
            }
        } catch (err) {
            console.warn(err);
        }
    }

    const setImg = (data, type) => {
        setDamageImg(data.data)
    }

    const filter = (e: any) => {
        const keyword = e;

        if (keyword != '') {
            const results = vehicleData.filter((user) => {
                return user.name.toLowerCase().startsWith(keyword.toLowerCase());
            });
            setVehicleData(results);
        } else {
            setVehicleData(driver.vehicleList);
        }
    }

    const setCarData = (vehicleData) => {
        openVehicleModal(!vehicleModal)
        setVehicleId(vehicleData.id)
        setVehicleName(vehicleData.name)
    }
    let { loading } = driver;
    return (
        <View style={[local_styles.mainContainer, { backgroundColor: (isDarkMode) ? "#fff" : "#fff" }]}>
            <View style={local_styles.bottomBox}>
                <Header
                    title={data != 'nodata' ? translate('editDamge') : translate('addDamage')}
                    goBack={navigation.goBack} />
                <View style={local_styles.inputView}>
                    <View style={{ alignItems: 'center', marginBottom: 30 }}>
                        {damageImg ?
                            <TouchableOpacity onPress={imgUpload}>
                                <Image source={{ uri: damageImg }} style={local_styles.imgBox} />
                            </TouchableOpacity> :
                            (<TouchableOpacity style={local_styles.imgBox} onPress={imgUpload}>
                                <Image source={require("assets/images/plus.png")} style={local_styles.plusImg} />
                            </TouchableOpacity>)}
                        <Text style={[local_styles.addTxt, { color: Colors.light_gray }]}>{translate('damagePhoto')}</Text>
                    </View>

                    <TouchableOpacity style={local_styles.searchBox} onPress={() => openVehicleModal(!vehicleModal)}>
                        <Image style={local_styles.searchImg} source={require('assets/images/Search.png')} />
                        <Text style={[local_styles.damgTxt, { color: Colors.light_gray }]}>{vehicleName == '' ? 'Select vehicle' : vehicleName}</Text>
                    </TouchableOpacity>

                    <TextInputField
                        placeholder={translate('damageDetails')}
                        styles={[Form.TextInput, local_styles.textInput]}
                        value={damageDetails}
                        onChangeText={(text: string) => {
                            setDamageDetails(text)
                        }}
                    />

                    <View style={local_styles.damageBox}>
                        <Text style={[local_styles.damgTxt, { color: Colors.light_gray }]}>{translate('chooseSeverity')}</Text>
                    </View>

                    <View style={local_styles.damageInnerBox}>
                        {damageList.map((item, index) => {
                            return (
                                <TouchableOpacity key={'damage_' + index} style={local_styles.boxView} onPress={() => selctGamage(item.backgroungColor)}>
                                    <View style={[local_styles.ovalView, { backgroundColor: item.backgroungColor }]}>
                                        {flag == item.backgroungColor ?
                                            <Image source={require("assets/images/check.png")} style={local_styles.checkImg} /> : null}
                                    </View>

                                    <View style={{ marginTop: 10, width: 50 }}>
                                        <Text style={[local_styles.smallTxt, { color: Colors.gray }]}>{item.name}</Text>
                                    </View>
                                </TouchableOpacity>
                            )
                        })}
                    </View>
                </View>
                <View style={local_styles.bottomBoxInner}>
                    <Button styles={local_styles.btnStyle} name={data != 'nodata' ? translate('save') : translate('subDamage')} onPress={uploadDamage} />
                </View>
            </View>


            <Modal
                animationType="slide"
                transparent={true}
                visible={vehicleModal}
            >
                <View style={local_styles.centeredView}>
                    <View style={local_styles.driverModalBg}>
                        <View style={local_styles.TophoriView}>
                            <TextInput
                                placeholder="search"
                                placeholderTextColor="#3E4347"
                                // value={''}
                                style={{ borderBottomWidth: 1, width: 170, height: 40 }}
                                onChangeText={(text) => filter(text)}
                            />
                            <TouchableOpacity onPress={() => openVehicleModal(!vehicleModal)} style={{ alignItems: 'flex-end', flex: 1 }}>
                                {/* <Image style={local_styles.closImg} source={require('assets/images/Cancel.png')} /> */}
                                <FontAwesomeIcon icon={faTimes} size={25} color={Colors.primary} />
                            </TouchableOpacity>
                        </View>

                        {vehicleData && vehicleData.length > 0 ? (
                            <FlatList
                                data={driver.vehicleList}
                                renderItem={(item) => {
                                    return (
                                        <TouchableOpacity style={local_styles.nameBox} onPress={() => setCarData(item.item)} >
                                            <Text style={local_styles.driverName} >{item.item.name}</Text>
                                        </TouchableOpacity>
                                    )
                                }} />
                        ) : null}
                    </View>
                </View>
            </Modal>
        </View>
    );
};

const local_styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    imgBox: {
        height: 122,
        width: 122,
        borderRadius: 122 / 2,
        backgroundColor: '#D5D5D5',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 14
    },
    addTxt: {
        fontSize: 16,
        fontWeight: '400'
    },
    plusImg: {
        height: 31,
        width: 31,
        resizeMode: 'contain'
    },
    damageBox: {
        marginTop: 42
    },
    damgTxt: {
        fontSize: 14,
        fontWeight: '400'
    },
    damageInnerBox: {
        marginTop: 31,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 59
    },
    boxView: {
        height: 87,
        width: 70,
        borderRadius: 9,
        backgroundColor: '#C4C4C4',
        alignItems: 'center'
    },
    ovalView: {
        height: 29,
        width: 29,
        borderRadius: 29 / 2,
        marginTop: 18,
        alignItems: 'center',
        justifyContent: 'center'
    },
    checkImg: {
        height: 10,
        width: 14,
        resizeMode: 'contain'
    },
    smallTxt: {
        fontSize: 7,
        fontWeight: '500',
        textAlign: 'center'
    },
    bottomBox: {
        // position:"absolute",
        width: "100%",
        // height: height / 1.7,
        backgroundColor: "#fff",
        borderTopLeftRadius: 24,
        borderTopRightRadius: 24,
        // bottom:0,
        paddingTop: 40,
        paddingLeft: 35,
        paddingRight: 35,
    },
    inputView: {
        marginTop: 50
    },
    textInput: {
        marginTop: 10,
    },
    bottomBoxInner: {
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        // marginTop: 50,
        width: "100%"
    },
    btnStyle: {
        width: 138
    },
    searchBox: {
        height: 40,
        width: 200,
        borderRadius: 20,
        borderWidth: 1,
        borderColor: '#D5D5D5',
        marginBottom: 20,
        alignItems: 'center',
        paddingLeft: 20,
        flexDirection: 'row'
    },
    searchImg: {
        height: 20,
        width: 20,
        marginRight: 15
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: 'center',
        borderWidth: 1,
        backgroundColor: "rgba(0,0,0,0.8)"
    },
    driverModalBg: {
        borderWidth: 1,
        backgroundColor: "#fff",
        width: "90%",
        height: 500,
        borderRadius: 20,
        paddingTop: 35,
        paddingLeft: 25
    },
    TophoriView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginRight: 15,
        alignItems: 'center',
        marginBottom: 20
    },
    nameBox: {
        // alignItems:'center',
        marginBottom: 7,
        marginLeft: 10
    },
    driverName: {
        fontSize: 18,
        fontWeight: '400',
        color: '#3E4347'
    },
    closImg: {
        height: 25,
        width: 25
    }
});
const mapStateToProps = (state: any) => ({
    auth: state.auth,
    driver: state.driver
})
export default connect(mapStateToProps)(AddDamage);

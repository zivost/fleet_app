/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useEffect, useState } from "react";
import {
    StyleSheet,
    useColorScheme,
    View,
    Image,
    Text,
    TouchableOpacity,
    TextInput,
    Modal,
    BackHandler,
    Alert
} from 'react-native';
import { human } from 'react-native-typography'
import Button from "../../../Components/Button/index"
import type { StackParamList } from "../../routes.types";
import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import {
    Colors,
    Typography,
    setI18nConfig,
    translate,
    Form
} from "../../config";
import Header from "../../../Components/Header/index"
import { ScrollView } from 'react-native-gesture-handler';
import ReportCard from '../../../Components/ReportCard/index'
import { DamageFilterModal } from '../../../Components/ModalView/index'
import { useDispatch, useSelector, connect } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faPlus, faMinus, faTimes } from '@fortawesome/free-solid-svg-icons'
import Loader from '../../../Components/Loader/index'
import constants from '../../constants';
import { useIsFocused } from '@react-navigation/native';

let arrList: any[] = [];
type Props = NativeStackScreenProps<StackParamList, 'InspectCar'>;
const InspectCar: FC<Props> = ({ navigation, route, driver, auth }) => {
    const dispatch = useDispatch();
    const date = route?.params?.date || "no date";
    const date_text = route?.params?.date_text || "no date";

    const damges = [{ 'color': 'red', 'damage_name': 'Severely Damaged' },
    { 'color': 'yellow', 'damage_name': 'Moderately Damaged' },
    { 'color': 'green', 'damage_name': 'Good Condition' },
    { 'color': 'black', 'damage_name': 'In Shop' }]
    const [openDriverModal, setopenDriverModal] = useState(false);
    const [driverData, setDriverData] = useState<any[]>([]);
    const [vehicleData, setVehicleData] = useState<any[]>([]);
    const [active_vehicle, setActiveVehicle] = useState<number>();
    const [driverName, setDriverName] = useState('');
    const [driverId, setDriverId] = useState('');
    const [flagTxt, setFlagTxt] = useState('');
    const [flagColor, setFlagColor] = useState('');
    const [van_count, set_van_count] = useState(0);
    const [van_count_limit, set_van_count_limit] = useState(0);
    const [is_checkedout, set_is_checkout] = useState(false);
    const [filterModal, setFilterModal] = useState(false);
    const [stats, set_stats] = useState({
        started: 0,
        ongoing: 0,
        completed: 0,
    });

    const isFocused = useIsFocused();

    const getDriverList = () => {
        let params = '&limit=50';
        dispatch({
            type: constants("driver").sagas.getDriverList,
            payload: {
                data: params,
                sessionToken: auth.user.token,
            }
        })
        dispatch({
            type: constants("driver").sagas.getVehiclesList,
            payload: {
                data: params,
                sessionToken: auth.user.token,
            }
        })
        dispatch({
            type: constants("driver").sagas.getInspectionCount,
            payload: {
                data: {
                    date: date
                },
                sessionToken: auth.user.token,
            }
        })
        dispatch({
            type: constants("driver").sagas.getInspectionForms,
            payload: {
                data: {
                    date: date
                },
                sessionToken: auth.user.token,
            }
        })
    }

    useEffect(() => {
        if (navigation.isFocused()) {
            getDriverList()
        }
    }, [])
    
    useEffect(() => {
        if (navigation.isFocused()) {
            getDriverList()
        }
    }, [isFocused])

    useEffect(() => {
        if (driver.driverList && driver.driverList.length > 0) {
            setDriverData(driver.driverList)
        }
    }, [driver.driverList])

    useEffect(() => {
        if (driver.inspection.eod_checkout) {
            set_is_checkout(true)
        } else {
            set_is_checkout(false)
        }
        set_van_count(driver.inspection.count || 0)
    }, [driver.inspection])

    useEffect(() => {
        let limit = 0;
        try {
            limit = parseInt(driver.vehicleListmetaData.total)
        } catch (e) {
            console.log("limit is invalid")
        }
        if (van_count_limit == 0) {
            set_van_count_limit(limit)
        }
    }, [driver])

    useEffect(() => {
        arrList = []
        let forms: any = {};
        for (let i in driver.inspection_forms) {
            forms[driver.inspection_forms[i].vehicle] = driver.inspection_forms[i];
        }
        if (driver.vehicleList && driver.vehicleList.length > 0) {
            let vehArrList = driver.vehicleList
            for (let i in vehArrList) {
                if (vehArrList[i].name != '') {
                    // if (van_count > parseInt(i)) {
                    arrList.push(vehArrList[i])
                    // }
                }
            }
            // arrList.pop();
            let arr = []
            let st = 0;
            let on = 0;
            let co = 0;
            for (let i in arrList) {
                if (forms[parseInt(arrList[i].id)]) {
                    arr.push({
                        ...arrList[i],
                        completion: forms[parseInt(arrList[i].id)].completion,
                        selected_driver: forms[parseInt(arrList[i].id)].driver,
                    })
                } else {
                    arr.push({
                        ...arrList[i],
                        selected_driver: null
                    })
                }
                if (forms[parseInt(arrList[i].id)]) {
                    let percentage = forms[parseInt(arrList[i].id)].completion || 0
                    if (percentage > 0 && percentage < 100) {
                        on++;
                    }
                    if (percentage >= 100) {
                        co++
                    }
                } else {
                    st++
                }

            }
            setVehicleData(arr)
            if (driver.inspection.eod_checkout) {
                if (is_checkedout) {
                    let check_List = []
                    for (let i in arr) {
                        if (arr[i].name != '') {
                            if (arr[i].completion > 0) {
                                check_List.push(arr[i])
                            }
                        }
                    }
                    setVehicleData(check_List)
                }
            }
            set_stats({
                started: st,
                ongoing: on,
                completed: co,
            })
        }
    }, [driver.vehicleList])


    // const addVehicle = (count) => {
    //     let forms: any = {};
    //     for (let i in driver.inspection_forms) {
    //         forms[driver.inspection_forms[i].vehicle] = driver.inspection_forms[i];
    //     }
    //     if (driver.vehicleList && driver.vehicleList.length > 0) {
    //         let vehArrList = driver.vehicleList
    //         if (vehArrList[count].name != '') {
    //             arrList.push(vehArrList[count])
    //         }

    //         let arr = []
    //         let st = 0;
    //         let on = 0;
    //         let co = 0;
    //         for (let i in arrList) {
    //             if (forms[parseInt(arrList[i].id)]) {
    //                 arr.push({
    //                     ...arrList[i],
    //                     completion: forms[parseInt(arrList[i].id)].completion,
    //                     selected_driver: forms[parseInt(arrList[i].id)].driver,
    //                 })
    //             } else {
    //                 arr.push({
    //                     ...arrList[i],
    //                     selected_driver: null
    //                 })
    //             }
    //             if (forms[parseInt(arrList[i].id)]) {
    //                 let percentage = forms[parseInt(arrList[i].id)].completion || 0
    //                 if (percentage > 0 && percentage < 100) {
    //                     on++;
    //                 }
    //                 if (percentage >= 100) {
    //                     co++
    //                 }
    //             } else {
    //                 st++
    //             }

    //         }
    //         setVehicleData(arr)
    //         set_stats({
    //             started: st,
    //             ongoing: on,
    //             completed: co,
    //         })
    //     }
    // }

    // const removeVehicle = (count: any) => {
    //     let forms: any = {};
    //     for (let i in driver.inspection_forms) {
    //         forms[driver.inspection_forms[i].vehicle] = driver.inspection_forms[i];
    //     }
    //     if (driver.vehicleList && driver.vehicleList.length > 0) {
    //         let vehArrList = driver.vehicleList
    //         arrList.pop()

    //         let arr = []
    //         let st = 0;
    //         let on = 0;
    //         let co = 0;
    //         for (let i in arrList) {
    //             if (forms[parseInt(arrList[i].id)]) {
    //                 arr.push({
    //                     ...arrList[i],
    //                     completion: forms[parseInt(arrList[i].id)].completion,
    //                     selected_driver: forms[parseInt(arrList[i].id)].driver,
    //                 })
    //             } else {
    //                 arr.push({
    //                     ...arrList[i],
    //                     selected_driver: null
    //                 })
    //             }
    //             if (forms[parseInt(arrList[i].id)]) {
    //                 let percentage = forms[parseInt(arrList[i].id)].completion || 0
    //                 if (percentage > 0 && percentage < 100) {
    //                     on++;
    //                 }
    //                 if (percentage >= 100) {
    //                     co++
    //                 }
    //             } else {
    //                 st++
    //             }

    //         }
    //         setVehicleData(arr)
    //         set_stats({
    //             started: st,
    //             ongoing: on,
    //             completed: co,
    //         })
    //     }
    // }

    const selectDriver = (driver_id: number, vehicle_id: number) => {
        let arr = []
        for (let i in vehicleData) {
            if (vehicleData[i].id === vehicle_id) {
                arr.push({
                    ...vehicleData[i],
                    selected_driver: driver_id
                })
            } else {
                arr.push(vehicleData[i])
            }
        }
        setVehicleData(arr)
    }

    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };
    }, []);

    const handleBackButtonClick = () => {
        navigation.goBack(null);
        return true;
    }

    const filter = (e: any) => {
        const keyword = e;

        if (keyword != '') {
            const results = driverData.filter((user) => {
                return user.name.toLowerCase().startsWith(keyword.toLowerCase());
            });
            setDriverData(results);
        } else {
            setDriverData(driver.driverList);
        }
    }

    const opnDriverModal = (data) => {
        setopenDriverModal(!openDriverModal)
    }

    const updateInspection = (driverId: any, vehicle_id: any) => {
        let inspection_object = {
            "driver": driverId,
            "checkout": false,
            "vehicle": vehicle_id,
            "inspection_form": {}
        }
        dispatch({
            type: constants("driver").sagas.updateInspection,
            payload: {
                data: inspection_object,
                date: date,
                id: vehicle_id,
                sessionToken: auth.user.token,
            },
            cllInspection: cllInspection,
            setDriver: true
        })
    }


    const cllInspection = (driverId: any, vehicle_id: any) => {
        getDriverList()
        let arr = []
        for (let i in vehicleData) {
            if (vehicleData[i].id === vehicle_id) {
                arr.push({
                    ...vehicleData[i],
                    selected_driver: driverId
                })
            } else {
                arr.push(vehicleData[i])
            }
        }
        setVehicleData(arr)
    }

    const navToinspect = (vehicleId: number, driverId: number, vehicleName: any, inpect: any) => {
        let inspectobj = []
        if (inpect && inpect.length > 0) {
            for (let i in inpect) {
                if (inpect[i].vehicle == vehicleId) {
                    inspectobj.push(inpect[i])
                }
            }
        }
        let sel_driver: any = {};
        for (let i in driverData) {
            if (driverData[i].id == driverId && driverId) {
                sel_driver = driverData[i]
            }
        }
        navigation.navigate('InspectionForm', {
            date,
            date_text,
            vehicleId,
            driverId,
            sel_driver,
            vehicleName,
            inspectobj
        })
    }
    const filterVehicleList = (e) => {
        const keyword = e;

        if (keyword != '') {
            const results = vehicleData.filter((user) => {
                return user.name.toLowerCase().startsWith(keyword.toLowerCase());
            });
            setVehicleData(results);
        } else {
            let vehArrList = driver.vehicleList
            let arrList = []
            for (let i in vehArrList) {
                if (vehArrList[i].name != '') {
                    arrList.push(vehArrList[i])
                }
            }
            setVehicleData(arrList);
        }
    }

    const navToInspectReport = (inspect_data: any, driverId: number, vehicleName: any) => {
        let sel_driver: any = {};
        for (let i in driverData) {
            if (driverData[i].id == driverId && driverId) {
                sel_driver = driverData[i]
            }
        }
        navigation.navigate('InspectReport', { inspect_data, sel_driver, vehicleName })
    }

    const openModal = () => {
        setFilterModal(!filterModal)
    }

    const selctFlag = (color, flagname) => {
        setFilterModal(!filterModal)
        setFlagTxt(flagname)
        setFlagColor(color)
        let vehArrList = driver.vehicleList
        let filterarr = []
        if (vehArrList && vehArrList.length > 0) {
            for (let i in vehArrList) {
                if (vehArrList[i].name != '') {
                    let flags = vehArrList[i].flags;
                    let flag = (flags.indexOf('red') >= 0 ? 'red' : flags.indexOf('yellow') >= 0 ? 'yellow' : 'green');
                    if (flag == color) {
                        filterarr.push(vehArrList[i])
                    }
                }
            }
        }

        setVehicleData(filterarr)
    }

    const isDarkMode = useColorScheme() === 'dark';
    let { loading } = driver;
    return (
        <View style={[local_styles.mainContainer, { backgroundColor: (isDarkMode) ? "#fff" : "#fff" }]}>
            <Loader loading={loading} />
            <View>
                <View style={{ marginLeft: 23, marginRight: 25 }}>
                    <View style={local_styles.topBox}>
                        <Header
                            title={translate('chooseInsp')}
                            dateView={true}
                            goBack={navigation.goBack}
                            date={date}
                            date_text={date_text}
                            filter={true}
                            filterValue={flagTxt}
                            openFilterModal={openModal}
                        />
                    </View>

                    <View style={local_styles.dispatchRow}>
                        <Text style={[local_styles.dispatchTxt, { color: Colors.gray_txt }]}>{translate('nofVandispatch')}</Text>
                        <View style={local_styles.incrBox}>
                            <TouchableOpacity style={local_styles.minusBox} onPress={() => {
                                if (van_count > 0 && !is_checkedout) {
                                    dispatch({
                                        type: constants("driver").sagas.setInspectionCount,
                                        payload: {
                                            data: {
                                                count: van_count - 1,
                                                date: date
                                            },
                                            sessionToken: auth.user.token,
                                        }
                                    })
                                    set_van_count(van_count - 1)
                                    // removeVehicle(van_count)
                                } else {
                                    Alert.alert(
                                        '',
                                        'Minimum Limit reach',
                                        [
                                            {
                                                text: 'Ok', onPress: () =>
                                                (
                                                    console.log
                                                )
                                            },
                                        ],
                                        { cancelable: false },
                                    );
                                }
                            }}>
                                <FontAwesomeIcon icon={faMinus} size={15} color={Colors.white} />
                            </TouchableOpacity>
                            <View style={local_styles.numberBox}>
                                <Text style={[local_styles.numberTxt, { color: Colors.gray_txt }]}>{van_count}</Text>
                            </View>
                            <TouchableOpacity style={local_styles.minusBox} onPress={() => {
                                // cal API to update to server
                                if (van_count_limit > van_count && !is_checkedout) {
                                    dispatch({
                                        type: constants("driver").sagas.setInspectionCount,
                                        payload: {
                                            data: {
                                                count: van_count + 1,
                                                date: date
                                            },
                                            sessionToken: auth.user.token,
                                        }
                                    });
                                    set_van_count(van_count + 1)
                                    // addVehicle(van_count)
                                } else {
                                    Alert.alert(
                                        '',
                                        'Maximum limit reach',
                                        [
                                            {
                                                text: 'Ok', onPress: () =>
                                                (
                                                    console.log
                                                )
                                            },
                                        ],
                                        { cancelable: false },
                                    );
                                }
                            }}>
                                <FontAwesomeIcon icon={faPlus} size={15} color={Colors.white} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={local_styles.progressBar}>
                        <View style={{ alignItems: 'center' }}>
                            <Text style={[local_styles.numberTxt, { color: Colors.primary }]}>{translate('ntStart')}</Text>
                            <Text style={[local_styles.numberTxt, { color: Colors.gray_txt, paddingTop: 10 }]}>{stats.started}</Text>
                        </View>

                        <View style={{ alignItems: 'center' }}>
                            <Text style={[local_styles.numberTxt, { color: Colors.primary }]}>{translate('inProgress')}</Text>
                            <Text style={[local_styles.numberTxt, { color: Colors.gray_txt, paddingTop: 10 }]}>{stats.ongoing}</Text>
                        </View>

                        <View style={{ alignItems: 'center' }}>
                            <Text style={[local_styles.numberTxt, { color: Colors.primary }]}>{translate('complete')}</Text>
                            <Text style={[local_styles.numberTxt, { color: Colors.gray_txt, paddingTop: 10 }]}>{stats.completed}</Text>
                        </View>
                    </View>


                    <View style={local_styles.searchBox}>
                        {flagTxt ? (
                            <View style={local_styles.filterBox}>
                                <View style={[local_styles.flagOval, { backgroundColor: flagColor ? flagColor : '#fff' }]} />
                                <Text style={[local_styles.flagTxt, { color: Colors.gray_txt }]}>{flagTxt}</Text>
                            </View>
                        ) : (
                            <View style={local_styles.searchContainer}>
                                <Image source={require("assets/images/Search.png")} style={local_styles.searchIcon} />
                                <TextInput
                                    placeholder="Search"
                                    style={local_styles.searchInput}
                                    onChangeText={(text) => filterVehicleList(text)} />
                            </View>
                        )}
                        <Button onPress={() => {
                            if (is_checkedout === true) {
                                Alert.alert(
                                    'Error!',
                                    'Already checked out for the day ',
                                    [
                                        {
                                            text: 'Ok', onPress: () =>
                                            (
                                                console.log
                                            )
                                        },
                                    ],
                                    { cancelable: false },
                                );
                            } else {
                                Alert.alert(
                                    'Confirm',
                                    'Are you sure you want to do EOD Checkout?',
                                    [
                                        {
                                            text: 'Yes', onPress: () => (
                                                dispatch({
                                                    type: constants("driver").sagas.inspectionCheckout,
                                                    payload: {
                                                        data: {
                                                            date: date
                                                        },
                                                        sessionToken: auth.user.token,
                                                    }
                                                })
                                            )
                                        },
                                        {
                                            text: 'No', onPress: () => (
                                                console.log
                                            )
                                        },
                                    ],
                                    { cancelable: false },
                                );
                            }
                        }} styles={local_styles.btnStyle} name={translate('eodCheckout')} />
                    </View>
                </View>
                {(van_count > 0) ?
                    <View style={{ alignItems: 'center', marginTop: 20 }}>
                        <ReportCard
                            drivers={driverData}
                            opnDriverModal={opnDriverModal}
                            navToinspect={navToinspect}
                            data={vehicleData}
                            setActiveVehicle={setActiveVehicle}
                            active_vehicle={active_vehicle}
                            inspection_forms={driver.inspection_forms}
                            stats={stats}
                            is_checkedout={is_checkedout}
                            navToInspectReport={navToInspectReport}
                            van_count={van_count}
                        />
                    </View>
                    :
                    <View style={{ alignItems: 'center', marginTop: 20 }}>
                        <Text>Please select the no. of vans dispatched.</Text>
                    </View>
                }
            </View>
            <Modal
                animationType="slide"
                transparent={true}
                visible={openDriverModal}
            >
                <View style={local_styles.centeredView}>
                    <View style={local_styles.driverModalBg}>
                        <View style={local_styles.TophoriView}>
                            <TextInput
                                placeholder="search"
                                placeholderTextColor="#3E4347"
                                // value={''}
                                style={{ borderBottomWidth: 1, width: 170, height: 40 }}
                                onChangeText={(text) => filter(text)}
                            />
                            <TouchableOpacity onPress={opnDriverModal}>
                                {/* <Image style={local_styles.closImg} source={require('assets/images/Cancel.png')} /> */}
                                <FontAwesomeIcon icon={faTimes} size={25} color={Colors.primary} />
                            </TouchableOpacity>
                        </View>

                        {driverData && driverData.length > 0 ? (
                            driverData.map((item, index) =>
                                <View key={"user" + index}>
                                    <TouchableOpacity onPress={() => {
                                        selectDriver(item.id, active_vehicle);
                                        opnDriverModal();
                                        updateInspection(item.id, active_vehicle)
                                    }}>
                                        <View style={local_styles.nameBox}>
                                            <Text style={local_styles.driverName}>{item.name}</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            )
                        ) : null}
                    </View>
                </View>
            </Modal>
            <DamageFilterModal damges={damges} closeMenuModal={() => setFilterModal(!filterModal)} filterModal={filterModal} selctFlag={selctFlag} />


            {/* <Modal
                animationType="slide"
                transparent={true}
                visible={false}
            >
                <View style={local_styles.centeredView}>
                    <View style={local_styles.modalBg}>
                        <View style={local_styles.warnBox}>
                            <Image style={local_styles.warnimage} source={require("assets/images/warnImg.png")} />
                            <Text style={[local_styles.warnTxt, { color: Colors.red }]}>There are 3 inspections incomplete</Text>
                        </View>
                        <Text style={local_styles.checkoutTxt}>Are you sure you want to checkout?</Text>
                        <View style={local_styles.btnView}>
                            <TouchableOpacity style={local_styles.yesBox}>
                                <Text style={[local_styles.dispatchTxt, { color: Colors.white }]}>Yes</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={local_styles.noBox}>
                                <Text style={[local_styles.dispatchTxt, { color: Colors.primary }]}>No</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal> */}
        </View>
    );
};

const local_styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },
    topBox: {
        marginTop: 30,
    },
    dispatchRow: {
        marginTop: 40,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    incrBox: {
        height: 32,
        width: 90,
        borderRadius: 6,
        borderWidth: 1,
        borderColor: '#577FFF',
        overflow: 'hidden',
        flexDirection: 'row'
    },
    dispatchTxt: {
        fontSize: 13,
        fontWeight: '400'
    },
    minusBox: {
        height: 31,
        width: 30,
        backgroundColor: '#577FFF',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    numberBox: {
        height: 31,
        width: 30,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    plusimage: {
        height: 11,
        width: 21,
        resizeMode: 'contain'
    },
    numberTxt: {
        fontSize: 15,
        fontWeight: '500'
    },
    progressBar: {
        marginTop: 37,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    searchBox: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 15
    },
    searchContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        width: 150,
        height: 36,
        borderRadius: 23,
        borderWidth: 1,
        borderColor: '#4F8BFF'
    },
    filterBox: {
        flexDirection: 'row',
        alignItems: 'center',
        width: 150,
        height: 36,
        borderBottomWidth: 1,
        borderBottomColor: '#C4C4C4'
    },
    btnStyle: {
        width: 150
    },
    searchIcon: {
        height: 13,
        width: 13,
        marginLeft: 15,
        marginRight: 10
    },
    searchInput: {
        height: 40
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: 'center',
        borderWidth: 1,
        backgroundColor: "rgba(0,0,0,0.8)"
    },
    modalBg: {
        borderWidth: 1,
        backgroundColor: "#fff",
        width: "90%",
        height: 158,
        borderRadius: 20,
        paddingTop: 35,
        paddingLeft: 25
    },
    driverModalBg: {
        borderWidth: 1,
        backgroundColor: "#fff",
        width: "90%",
        height: 500,
        borderRadius: 20,
        paddingTop: 35,
        paddingLeft: 25
    },
    warnBox: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10
    },
    warnimage: {
        height: 11,
        width: 11,
        resizeMode: 'contain',
        marginRight: 5
    },
    warnTxt: {
        fontSize: 12,
        fontWeight: '300'
    },
    checkoutTxt: {
        fontSize: 13,
        fontWeight: '400',
        color: "#383838"
    },
    btnView: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        marginRight: 10,
        marginBottom: 10,
        flexDirection: 'row'
    },
    yesBtn: {
        width: 100,
        height: 20,
        marginRight: 20
    },
    yesBox: {
        width: 73,
        height: 36,
        borderRadius: 20,
        backgroundColor: "#4F8BFF",
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 13
    },
    noBox: {
        width: 73,
        height: 36,
        borderRadius: 20,
        borderColor: "#4F8BFF",
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        // marginRight:13
    },
    TophoriView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginRight: 15,
        alignItems: 'center',
        marginBottom: 25
    },
    nameBox: {
        // alignItems:'center',
        marginBottom: 5,
        paddingVertical: 5
    },
    driverName: {
        fontSize: 18,
        fontWeight: '400',
        color: '#3E4347',
    },
    closImg: {
        height: 25,
        width: 25
    },
    editStyle: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)',
        alignItems: 'flex-end',
    },
    flagOval: {
        height: 13,
        width: 13,
        borderRadius: 13 / 2,
        marginRight: 11
    },
    flagTxt: {
        fontSize: 14,
        fontWeight: '700'
    }
});

const mapStateToProps = (state: any) => ({
    auth: state.auth,
    driver: state.driver
})
export default connect(mapStateToProps)(InspectCar);

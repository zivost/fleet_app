/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useState, useEffect } from "react";
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    PermissionsAndroid,
    BackHandler
} from 'react-native';

import {
    Colors,
    Typography,
    setI18nConfig,
    translate,
    Form
} from "../../config";
import Loader from '../../../Components/Loader/index'
import { iOSUIKit, human } from 'react-native-typography'
import TextInputField from '../../../Components/TextInput/index'
import Button from "../../../Components/Button/index"
import Header from "../../../Components/Header/index"
import type { StackParamList } from "../../routes.types";
import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import { useDispatch, useSelector, connect } from 'react-redux'
import constants from '../../constants';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker'

let width = Dimensions.get('window').width;
let height = Dimensions.get('window').height;

let emailRegx = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;

type Props = NativeStackScreenProps<StackParamList, 'AddDriver'>;
const AddDriver: FC<Props> = ({ navigation, auth, driver, route }) => {
    const isDarkMode = useColorScheme() === 'dark';
    const data = route?.params?.data || 'nodata';
    let myDatepicker: { onPressDate: () => void; };
    const dispatch = useDispatch();
    const [damageDetails, setDamageDetails] = useState('')
    const [email, setEmail] = useState({ value: "", error: false, msg: "" });
    const [name, setName] = useState({ value: "", error: false, msg: "" });
    const [phone, setPhone] = useState({ value: "", error: false, msg: "" });
    const [hireDate, setHireDate] = useState('');
    const [driverPic, setDriverPic] = useState('');
    const [damid, setId] = useState('');

    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };
    }, []);

    const handleBackButtonClick = () => {
        navigation.goBack(null);
        return true;
    }


    useEffect(() => {
        if (data != 'nodata') {
            setName({ value: data.name, error: false, msg: "" })
            setEmail({ value: data.email, error: false, msg: "" })
            setPhone({ value: data.phone, error: false, msg: "" })
            setHireDate(data.hire_date)
            setId(data.id)
        }
    }, [data])

    const addDriverProfil = () => {
        let carid = ''
        if (data != 'nodata') {
            carid = damid
        }
        if (email.value && !email.error && phone.value && hireDate) {
            dispatch({
                type: constants("driver").sagas.createDriver,
                payload: {
                    data: {
                        "name": name.value,
                        "email": email.value,
                        "phone": phone.value,
                        "hire_date": hireDate,
                        "images": [
                            driverPic
                        ]
                    },
                    id: damid,
                    sessionToken: auth.user.token,
                    navigation
                }
            })
        }
    }

    const imgUpload = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                    title: "App Camera Permission",
                    message: "App needs access to your camera ",
                    buttonNeutral: "Ask Me Later",
                    buttonNegative: "Cancel",
                    buttonPositive: "OK"
                }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                let options = {
                    maxWidth: 1000, //speeds up compressImage to almost no time
                    maxHeight: 1000,
                };
                launchCamera(options, (response) => {
                    if (response && response.assets && response.assets.length > 0) {
                        let img = response.assets[0];
                        // setDamageImg(img)
                        dispatch({
                            type: constants("driver").sagas.uploadDamageImg,
                            payload: {
                                data: {
                                    file: img,
                                },
                                type: 'driverPic',
                                sessionToken: auth.user.token,
                            },
                            setImg: setImg
                        })
                    }
                })
            } else {
                console.log("Camera permission denied");
            }
        } catch (err) {
            console.warn(err);
        }
    }

    const setImg = (data, type) => {
        if (type == 'driverPic') {
            setDriverPic(data.data)
        }

    }
    let { loading } = driver;
    return (
        <View style={[local_styles.mainContainer, { backgroundColor: (isDarkMode) ? "#fff" : "#fff" }]}>
            <View style={local_styles.bottomBox}>
                <Header
                    title={data != 'nodata' ? translate('editDriver') : translate('addDriver')}
                    goBack={navigation.goBack} />
                <View style={local_styles.inputView}>
                    <View style={{ alignItems: 'center', marginBottom: 50 }}>
                        {driverPic ? (
                            <Image source={{ uri: driverPic }} style={local_styles.imgBox} />
                        ) :
                            (<TouchableOpacity style={local_styles.imgBox} onPress={imgUpload}>
                                <Image source={require("assets/images/plus.png")} style={local_styles.plusImg} />
                            </TouchableOpacity>)}
                        <Text style={[local_styles.addTxt, { color: Colors.light_gray }]}>{translate('profilePic')}</Text>
                    </View>

                    <TextInputField
                        placeholder={translate('name')}
                        styles={[Form.TextInput, local_styles.textInput]}
                        value={name.value}
                        onChangeText={(text: string) => {
                            setName({ value: text, error: false, msg: "" });
                        }}
                    />

                    {/* <TextInputField
                        placeholder={translate('licenNo')}
                        styles={[Form.TextInput, local_styles.textInput]}
                        value={licensNo.value}
                        onChangeText={(text: string) => {
                            setlicensNo({ value: text, error: false, msg: "" });
                        }}
                    /> */}

                    <TouchableOpacity style={local_styles.dateBox} onPress={() => myDatepicker.onPressDate()}>
                        <Text style={[local_styles.dateTxt, { color: Colors.light_gray }]}>{hireDate ? hireDate : 'Hire date'}</Text>
                    </TouchableOpacity>

                    <View>
                        <DatePicker
                            ref={(datepicker: any) => myDatepicker = datepicker}
                            style={{ width: 0, height: 0 }}
                            // date={birthday.value}
                            mode="date"
                            duration={300}
                            format="YYYY-MM-DD"
                            showIcon={false}
                            hideText={true}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateInput: {
                                    borderWidth: 0,
                                    alignItems: "flex-start",
                                },
                                dateText: {
                                    // ...styles.dateFieldText,
                                },
                                btnTextConfirm: {
                                    color: "#000000",
                                },
                            }}
                            onDateChange={(_date: string) => { setHireDate(_date) }}
                        />
                    </View>

                    <TextInputField
                        placeholder={translate('email')}
                        styles={[Form.TextInput, local_styles.textInput]}
                        defaultValue={email.value}
                        value={email.value}
                        keyboardType='email-address'
                        error={email.error}
                        errorMsg={email.msg}
                        onChangeText={(text: string) => {
                            let obj = { ...email }
                            if (emailRegx.test(text) === true) {
                                obj = { value: text, error: false, msg: "" }
                            } else {
                                obj = { value: text, error: true, msg: translate('pleaseEnterValidEmail') }
                            }
                            setEmail(obj)
                        }}
                    />

                    <TextInputField
                        placeholder={translate('phoneNo')}
                        styles={[Form.TextInput, local_styles.textInput]}
                        keyboardType='phone-pad'
                        value={phone.value}
                        onChangeText={(text: string) => {
                            setPhone({ value: text, error: false, msg: "" })
                        }}
                    />

                </View>
                <View style={local_styles.bottomBoxInner}>
                    <Button styles={local_styles.btnStyle} name={data != 'nodata' ? translate('save') : translate('addDriverBtn')} onPress={() => { addDriverProfil() }} />
                </View>
            </View>
        </View>
    );
};

const local_styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    imgBox: {
        height: 122,
        width: 122,
        borderRadius: 122 / 2,
        backgroundColor: '#D5D5D5',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 14
    },
    addTxt: {
        fontSize: 16,
        fontWeight: '400'
    },
    plusImg: {
        height: 31,
        width: 31,
        resizeMode: 'contain'
    },
    damageBox: {
        marginTop: 42
    },
    damgTxt: {
        fontSize: 14,
        fontWeight: '400'
    },
    damageInnerBox: {
        marginTop: 31,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 59
    },
    boxView: {
        height: 87,
        width: 70,
        borderRadius: 9,
        backgroundColor: '#C4C4C4',
        alignItems: 'center'
    },
    ovalView: {
        height: 29,
        width: 29,
        borderRadius: 29 / 2,
        marginTop: 18,
        alignItems: 'center',
        justifyContent: 'center'
    },
    checkImg: {
        height: 10,
        width: 14,
        resizeMode: 'contain'
    },
    smallTxt: {
        fontSize: 7,
        fontWeight: '500',
        textAlign: 'center'
    },
    bottomBox: {
        // position:"absolute",
        width: "100%",
        // height: height / 1.7,
        backgroundColor: "#fff",
        borderTopLeftRadius: 24,
        borderTopRightRadius: 24,
        // bottom:0,
        paddingTop: 40,
        paddingLeft: 35,
        paddingRight: 35,
    },
    headingText: {
        ...human.title1Object,
        color: "#4F8BFF",
        fontWeight: "bold"
    },
    inputView: {
        marginTop: 50
    },
    textInput: {
        marginTop: 10,
    },
    bottomBoxInner: {
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        marginTop: 50,
        width: "100%"
    },
    btnStyle: {
        width: 138
    },
    cmpnyTxt: {
        fontSize: 10,
        fontWeight: '400'
    },
    dateBox: {
        borderBottomWidth: 1,
        height: 40,
        borderColor: '#D5D5D5',
        marginTop: 15
    },
    dateTxt: {
        paddingTop: 5,
        fontSize: 16,
        fontWeight: '400'
    },
});
const mapStateToProps = (state: any) => ({
    auth: state.auth,
    driver: state.driver
})
export default connect(mapStateToProps)(AddDriver);

/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useState, useEffect } from "react";
import {
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    BackHandler,
    Alert
} from 'react-native';

import {
    translate,
    Form,
    Colors
} from "../../config";
import { useDispatch, useSelector, connect } from 'react-redux'
import { human } from 'react-native-typography'
import TextInputField from '../../../Components/TextInput/index'
import type { StackParamList } from "../../routes.types";
import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import auth, { FirebaseAuthTypes } from '@react-native-firebase/auth';
import Loader from '../../../Components/Loader/index'
import constants from '../../constants';
let unsubscribe: any
let width = Dimensions.get('window').width;
let height = Dimensions.get('window').height;

let emailRegx = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;

type Props = NativeStackScreenProps<StackParamList, 'SignIn'>;
const SignIn: FC<Props> = ({ navigation }) => {
    const dispatch = useDispatch();
    const [email, setEmail] = useState({ value: "", error: false, msg: "" });
    const [password, setPassword] = useState({ value: "", error: false, msg: "" });
    const [initializing, setInitializing] = useState(true);
    const [user, setUser] = useState();
    const [token, setToken] = useState('null');
    // const auth = useSelector(state => state.auth)

    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };
    }, []);

    const handleBackButtonClick = () => {
        navigation.goBack(null);
        return false;
    }

    // Handle user state changes
    useEffect(() => {
        auth().signOut();
        unsubscribe = auth().onAuthStateChanged(onAuthStateChanged);
        // unsubscribe to the listener when unmounting
        return () => unsubscribe()
    }, []);
    const onAuthStateChanged = async (user: any) => {
        signin(user)
    }
    // if (initializing) return null; // what is this for?

    const signInFunc = async () => {
        if (email.value == "") {
            setEmail({
                ...email,
                error: true,
                msg: "Email is required!"
            })
        }
        if (password.value == "") {
            setPassword({
                ...password,
                error: true,
                msg: "Password is required!"
            })
        }
        if (email.error || password.error || password.value == "" || email.value == "") {
            return false;
        }
        auth().signInWithEmailAndPassword(email.value, password.value).then((res) => {
            signin(res.user)
            clrTxt()
        }).catch(error => {
            if (error.code === 'auth/email-already-in-use') {
                console.log('That email address is already in use!');
            }
            if (error.code === 'auth/invalid-email') {
                console.log('That email address is invalid!');
            }
            Alert.alert(
                '',
                "Email and Password is invalid.",
                [
                    {
                        text: 'Ok', onPress: () =>
                        (
                            console.log()
                        )
                    },
                ],
                { cancelable: false },
            );
            // console.log("error", error)
            return error
        });
        
    }
    const signin = async function(User: FirebaseAuthTypes.User){
        if (!User) {
            console.log("user not logged in.");
        }
        let token = ""
        try {
            token = await User.getIdToken();
        } catch (err) {
            console.log('err', err)
        }
        if (token) {
            setToken(token)
            // setUser(user);
            dispatch({
                type: constants("auth").sagas.login,
                payload: {
                    data: {
                        "email": email.value, // unique
                        "token": token
                    },
                    navToScreen: navToDashboard
                }
            })
        }
    }
    const clrTxt = () => {
        setEmail({ value: "", error: false, msg: "" })
        setPassword({ value: "", error: false, msg: "" })
    }

    const navToDashboard = (data: any) => {
        if (data && data.data) {
            if (data.data.company) {
                navigation.navigate('Dashboard')
            } else {
                navigation.navigate('RegisterCompany')
            }
        }
    }

    const navTosignUp = () => {
        navigation.navigate('SignUp');
        if (typeof unsubscribe === 'function') {
            unsubscribe()
        }
    }
    return (
        <View style={[local_styles.mainContainer]}>
            <Loader loading={false} />
            <View style={[local_styles.box,]}>
                <Image style={local_styles.image} source={require("assets/images/signinBg.png")} />
            </View>
            <View style={local_styles.bottomBox}>
                <Text style={local_styles.headingText}>{translate("signin")}</Text>
                <View style={local_styles.inputView}>
                    <TextInputField
                        placeholder={translate('email')}
                        styles={[Form.TextInput]}
                        defaultValue={email.value}
                        keyboardType='email-address'
                        error={email.error}
                        errorMsg={email.msg}
                        onPress={() => setEmail({ value: "", error: false, msg: "" })}
                        onChangeText={(text: string) => {
                            let obj = { ...email }
                            if (emailRegx.test(text) === true) {
                                obj = { value: text, error: false, msg: "" }
                            } else {
                                obj = { value: text, error: true, msg: translate('pleaseEnterValidEmail') }
                            }
                            setEmail(obj)
                        }}
                    />
                    <TextInputField
                        placeholder={translate('password')}
                        styles={[Form.TextInput]}
                        defaultValue={password.value}
                        secureTextEntry={true}
                        error={password.error}
                        errorMsg={password.msg}
                        onChangeText={(text: string) => {
                            setPassword({ value: text, error: false, msg: "" })
                        }}
                    />
                    <View style={local_styles.forgotView}>
                        <Text style={local_styles.forgotText}>{translate('forgot')}?</Text>
                    </View>
                </View>
                <View style={local_styles.bottomBoxInner}>
                    <TouchableOpacity style={local_styles.leftSide} onPress={navTosignUp}>
                        <Text style={local_styles.signText}>{translate('signup')}</Text>
                    </TouchableOpacity>
                    <View style={local_styles.rightSide}>
                        <TouchableOpacity disabled={(email.error || password.error)} style={[local_styles.innerCircle, (email.error || password.error) ? local_styles.disabled : {}]} onPress={() => signInFunc()}>
                            <Image source={require("assets/images/rightArrow.png")} />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
    );
};

const local_styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    box: {
        height: height / 1.7,
        width: "100%",
        // backgroundColor: "#fff",
    },
    image: {
        width: "100%",
        resizeMode: "cover",
        height: "100%"
    },
    bottomBox: {
        position: "absolute",
        width: "100%",
        height: height / 1.9,
        backgroundColor: "#fff",
        borderTopLeftRadius: 24,
        borderTopRightRadius: 24,
        bottom: 0,
        paddingTop: 40,
        paddingLeft: 35,
        paddingRight: 35,
    },
    headingText: {
        ...human.title1Object,
        color: "#4F8BFF",
        fontWeight: "bold"
    },
    inputView: {
        marginTop: 30
    },
    textInput: {
        marginTop: 10,
    },
    forgotView: {
        justifyContent: "flex-end",
        width: "100%",
        marginTop: 8
    },
    forgotText: {
        ...human.caption1Object,
        color: "#4F8BFF",
        fontWeight: "500",
        textAlign: "right"
    },
    bottomBoxInner: {
        justifyContent: "space-between",
        flexDirection: 'row',
        alignContent: "center",
        marginTop: 40
        // alignSelf:"center"
    },
    leftSide: {
        // borderWidth:1,
        justifyContent: "center"
    },
    rightSide: {

    },
    signText: {
        ...human.subheadObject,
        color: "#4F8BFF",
        fontWeight: 'bold'
    },
    innerCircle: {
        width: 65,
        height: 65,
        backgroundColor: "#4F8BFF",
        borderRadius: 50,
        justifyContent: "center",
        alignItems: "center"
    },
    disabled: {
        opacity: 0.7
    }
});
const mapStateToProps = (state: any) => ({
    auth: state.auth,
})
export default connect(mapStateToProps)(SignIn);
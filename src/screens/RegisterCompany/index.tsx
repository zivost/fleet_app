/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useState, useEffect } from "react";
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    BackHandler
} from 'react-native';

import {
    Colors,
    Typography,
    setI18nConfig,
    translate,
    Form
} from "../../config";

import { iOSUIKit, human } from 'react-native-typography'
import { useNavigation } from '@react-navigation/native';
import TextInputField from '../../../Components/TextInput/index'
import Button from "../../../Components/Button/index"
import Header from "../../../Components/Header/index"
import Loader from '../../../Components/Loader/index'
import constants from '../../constants';
let width = Dimensions.get('window').width;
let height = Dimensions.get('window').height;

import type { StackParamList } from "../../routes.types";
import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import { useDispatch, useSelector, connect } from 'react-redux'

type Props = NativeStackScreenProps<StackParamList, 'RegisterCompany'>;
const RegisterCompany: FC<Props> = ({ navigation, auth, driver, route }) => {
    const data = route?.params?.data || 'nodata';
    const isDarkMode = useColorScheme() === 'dark';
    const dispatch = useDispatch();
    const [cmpName, setCmpName] = useState({ value: "", error: false, msg: "" });
    const [ownName, setOwnName] = useState({ value: "", error: false, msg: "" });
    const [stationAdd, setStationAdd] = useState({ value: "", error: false, msg: "" });
    const [zipCode, setZipCode] = useState({ value: "", error: false, msg: "" });
    const [dspCode, setDspCode] = useState({ value: "", error: false, msg: "" });
    const [stationCode, setStationCode] = useState({ value: "", error: false, msg: "" });
    const [cmpId, setCmpId] = useState({ value: "", error: false, msg: "" });
    const [city, setCity] = useState({ value: "", error: false, msg: "" });
    const [mileage, setMileage] = useState(false);
    const [fuel, setFuel] = useState(false);
    const [frontside, setFrontSide] = useState(false);
    const [driverside, setDriverSide] = useState(false);
    const [passengerside, setPassengerSide] = useState(false);
    const [rearSide, setrearSide] = useState(false);
    const [handTruck, sethandTruck] = useState(false);
    const [carwash, setCarwash] = useState(false);
    const [phone, setPhone] = useState(false);
    const [charger, setCharger] = useState(false);
    const [rescue, setRescue] = useState(false);
    const [gasCard, setGasCard] = useState(false);
    const [absLight, setAbsLight] = useState(false);
    const [parkingTic, setParkingTic] = useState(false);
    const [wiper, setWiper] = useState(false);

    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };
    }, []);

    useEffect(() => {
        if (data != 'nodata') {
            setOwnName({ value: data.ownName, error: false, msg: "" })
            setStationAdd({ value: data.address, error: false, msg: "" })
            setCmpName({ value: data.cmpName, error: false, msg: "" })
            setZipCode({ value: data.pinCode, error: false, msg: "" })
            setDspCode({ value: data.code1, error: false, msg: "" })
            setStationCode({ value: data.code2, error: false, msg: "" })
            setCmpId({ value: data.cmpId, error: false, msg: "" })
            setCity({ value: data.city, error: false, msg: "" })
        }
    }, [data])

    useEffect(() => {
        if (auth.user && auth.user.company) {
            if (auth.user.company.inspection) {
                let data = auth.user.company.inspection
                setMileage(data.mileage)
                setFuel(data.gas)
                setFrontSide(data.front_view)
                setDriverSide(data.driver_side_view)
                setPassengerSide(data.passenger_side_view)
                setrearSide(data.rear_view)
                sethandTruck(data.handtruck)
                setCarwash(data.clean)
                setPhone(data.phone)
                setCharger(data.phone_charger)
                setRescue(data.rescue)
                setGasCard(data.gas_card)
                setAbsLight(data.engine_light)
                setParkingTic(data.parking_ticket)
                setWiper(data.windshield_fluid)
            }
        }
    }, [auth.user]);

    const handleBackButtonClick = () => {
        navigation.goBack(null);
        return true;
    }

    const registerCompany = () => {
        if (cmpName.value == "") {
            setCmpName({
                ...cmpName,
                error: true,
                msg: "Company Name is required!"
            })
        }
        if (ownName.value == "") {
            setOwnName({
                ...ownName,
                error: true,
                msg: "Owner Name is required!"
            })
        }
        if (stationAdd.value == "") {
            setStationAdd({
                ...stationAdd,
                error: true,
                msg: "Station Address is required!"
            })
        }
        if (zipCode.value == "") {
            setZipCode({
                ...zipCode,
                error: true,
                msg: "Zip Code is required!"
            })
        }
        if (dspCode.value == "") {
            setDspCode({
                ...dspCode,
                error: true,
                msg: "DSP Short Code is required!"
            })
        }
        if (stationCode.value == "") {
            setStationCode({
                ...stationCode,
                error: true,
                msg: "Station Code is required!"
            })
        }
        if (cmpName.value == "" || ownName.value == "" || stationAdd.value == "" || zipCode.value == "" || dspCode.value == "" || stationCode.value == ""
            || cmpName.error || ownName.error || stationAdd.error || zipCode.error || dspCode.error || stationCode.error
        ) {
            return
        }
        let data = {
            company_name: cmpName.value,
            owner_Name: ownName.value,
            address: stationAdd.value,
            zipCode: zipCode.value,
            stationCode: stationCode.value,
            dspCode: dspCode.value
        }
        if (cmpId.value) {
            dispatch({
                type: constants("auth").sagas.updtCompanyInsp,
                payload: {
                    data: {
                        "company_name": cmpName.value,
                        "owner_name": ownName.value,
                        "dsp_short_code": dspCode.value,
                        "address": stationAdd.value,
                        "zipcode": zipCode.value,
                        "station_code": stationCode.value,
                        "city": city.value,
                        "inspection": {
                            "clean": carwash,
                            "driver_side_view": driverside,
                            "engine_light": absLight,
                            "front_view": frontside,
                            "gas": fuel,
                            "gas_card": gasCard,
                            "handtruck": handTruck,
                            "mileage": mileage,
                            "parking_ticket": parkingTic,
                            "passenger_side_view": passengerside,
                            "phone": phone,
                            "phone_charger": charger,
                            "rear_view": rearSide,
                            "rescue": rescue,
                            "windshield_fluid": wiper
                        }
                    },
                    sessionToken: auth.user.token,
                    company_id: cmpId.value,
                    accountId: auth.user.account_id,
                    navigation
                }
            })
        } else {
            navigation.navigate("InitialInspection", { params: data })
        }

    }
    let { loading } = auth;
    return (
        <View style={[local_styles.mainContainer, { backgroundColor: (isDarkMode) ? "#fff" : "#fff" }]}>
            <Loader loading={loading} />
            <View style={local_styles.bottomBox}>
                <Header
                    title={translate("registerYourCom")}
                    goBack={navigation.goBack} />
                <View style={local_styles.inputView}>

                    <TextInputField
                        placeholder={translate('cmpName')}
                        styles={[Form.TextInput]}
                        defaultValue={cmpName.value}
                        value={cmpName.value}
                        error={cmpName.error}
                        keyboardType='email-address'
                        errorMsg={cmpName.msg}
                        onChangeText={(text: string) => {
                            setCmpName({ value: text, error: false, msg: "" })
                        }}
                    />
                    <TextInputField
                        placeholder={translate('owner')}
                        styles={[Form.TextInput, local_styles.textInput]}
                        defaultValue={ownName.value}
                        value={ownName.value}
                        error={ownName.error}
                        keyboardType='email-address'
                        errorMsg={ownName.msg}
                        onChangeText={(text: string) => {
                            setOwnName({ value: text, error: false, msg: "" })
                        }}
                    />
                    <TextInputField
                        placeholder={translate('address')}
                        styles={[Form.TextInput, local_styles.textInput]}
                        defaultValue={stationAdd.value}
                        value={stationAdd.value}
                        error={stationAdd.error}
                        keyboardType='email-address'
                        errorMsg={stationAdd.msg}
                        onChangeText={(text: string) => {
                            setStationAdd({ value: text, error: false, msg: "" })
                        }}
                    />
                    <Text style={[local_styles.cmpnyTxt, { color: Colors.light_grey }]}>Enter address where the company is based</Text>
                    <TextInputField
                        placeholder={translate('city')}
                        styles={[Form.TextInput, local_styles.textInput]}
                        defaultValue={city.value}
                        value={city.value}
                        error={city.error}
                        keyboardType='email-address'
                        errorMsg={city.msg}
                        onChangeText={(text: string) => {
                            setCity({ value: text, error: false, msg: "" })
                        }}
                    />
                    <TextInputField
                        placeholder={translate('pincode')}
                        styles={[Form.TextInput, local_styles.textInput]}
                        defaultValue={zipCode.value}
                        value={zipCode.value}
                        error={zipCode.error}
                        keyboardType='phone-pad'
                        errorMsg={zipCode.msg}
                        onChangeText={(text: string) => {
                            setZipCode({ value: text, error: false, msg: "" })
                        }}
                    />
                    <TextInputField
                        placeholder={translate('dspShortCode')}
                        styles={[Form.TextInput, local_styles.textInput]}
                        defaultValue={dspCode.value}
                        value={dspCode.value}
                        error={dspCode.error}
                        keyboardType='email-address'
                        errorMsg={dspCode.msg}
                        onChangeText={(text: string) => {
                            setDspCode({ value: text, error: false, msg: "" })
                        }}
                    />
                    <TextInputField
                        placeholder={translate('stationCode')}
                        styles={[Form.TextInput, local_styles.textInput]}
                        defaultValue={stationCode.value}
                        value={stationCode.value}
                        error={stationCode.error}
                        keyboardType='email-address'
                        errorMsg={stationCode.msg}
                        onChangeText={(text: string) => {
                            setStationCode({ value: text, error: false, msg: "" })
                        }}
                    />

                </View>
                <View style={local_styles.bottomBoxInner}>
                    <Button disabled={(cmpName.error || ownName.error || stationAdd.error || zipCode.error || dspCode.error || stationCode.error)} styles={[local_styles.btnStyle, (cmpName.error || ownName.error || stationAdd.error || zipCode.error || dspCode.error || stationCode.error) ? local_styles.disabled : {}]} onPress={() => registerCompany()} name={translate("next")} />
                </View>
            </View>
        </View>
    );
};

const local_styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    bottomBox: {
        // position:"absolute",
        width: "100%",
        // height: height / 1.7,
        backgroundColor: "#fff",
        borderTopLeftRadius: 24,
        borderTopRightRadius: 24,
        // bottom:0,
        paddingTop: 40,
        paddingLeft: 35,
        paddingRight: 35,
    },
    headingText: {
        ...human.title1Object,
        color: "#4F8BFF",
        fontWeight: "bold"
    },
    inputView: {
        marginTop: 50
    },
    textInput: {
        marginTop: 10,
    },
    bottomBoxInner: {
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        marginTop: 50,
        width: "100%"
    },
    btnStyle: {
        width: 138
    },
    cmpnyTxt: {
        fontSize: 10,
        fontWeight: '400'
    },
    disabled: {
        opacity: 0.7
    }
});
const mapStateToProps = (state: any) => ({
    auth: state.auth,
})
export default connect(mapStateToProps)(RegisterCompany);

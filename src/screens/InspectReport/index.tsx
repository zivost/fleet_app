/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useEffect, useState } from "react";
import {
    StyleSheet,
    useColorScheme,
    View,
    Image,
    Text,
    TouchableOpacity,
    ScrollView,
    Dimensions,
    BackHandler
} from 'react-native';
import { human } from 'react-native-typography'
import Button from "../../../Components/Button/index"
import {
    Colors,
    Typography,
    setI18nConfig,
    translate,
    Form
} from "../../config";
import Header from "../../../Components/Header/index"
import LinearGradient from 'react-native-linear-gradient';
import { useDispatch, useSelector, connect } from 'react-redux'
import constants from '../../constants';
import Loader from '../../../Components/Loader/index'
import InspectBox from '../../../Components/InspectBox/index'
import TextBox from '../../../Components/TextBox/index'

let width = Dimensions.get('window').width;
let height = Dimensions.get('window').height;

const InspectReport: FC<Props> = ({ navigation, auth, route, driver }) => {

    const data = route?.params?.inspect_data || "no date";
    const sel_driver = route?.params?.sel_driver || "no date";
    const vehicleName = route?.params?.vehicleName || "";
    const dispatch = useDispatch();

    const [vehicleClean, setVehicleClean] = useState(false);
    const [wiper, setWiper] = useState(false);
    const [engineLight, setEngineLight] = useState(false);
    const [gasCard, setGascard] = useState(false);
    const [charger, setCharger] = useState(false);
    const [phone, setPhone] = useState(false);
    const [rescue, setRescue] = useState(false);
    const [isparkingTicket, setIsParkingTicket] = useState(false);
    const [mileage, SetMilege] = useState('');
    const [gas, setGas] = useState('')
    const [frontView, setFrontView] = useState([])
    const [rearView, setRearView] = useState([])
    const [driverView, setDriverView] = useState([])
    const [passengerView, setPassengerView] = useState([])
    const [handtruckView, setHandtruckView] = useState([])
    const [inspectionData, setInspectionData] = useState('')

    useEffect(() => {
        if (data[0].inspection_form) {
            if (data[0].inspection_form.gas) {
                let gasTank = ''
                if (data[0].inspection_form.gas == '0') {
                    gasTank = 0
                } else if (data[0].inspection_form.gas === '25') {
                    gasTank = 1 / 4
                } else if (data[0].inspection_form.gas === '50') {
                    gasTank = 2 / 4
                } else if (data[0].inspection_form.gas === '75') {
                    gasTank = 3 / 4
                } else if (data[0].inspection_form.gas === '100') {
                    gasTank = 4 / 4
                }
                setGas(gasTank)
            }
            if (data[0].inspection_form.mileage) {
                SetMilege(data[0].inspection_form.mileage)
            }
            if (data[0].inspection_form.front_view) {
                setFrontView(data[0].inspection_form.front_view.attachment)
            }
            if (data[0].inspection_form.driver_side_view) {
                setDriverView(data[0].inspection_form.driver_side_view.attachment)
            }
            if (data[0].inspection_form.passenger_side_view) {
                setPassengerView(data[0].inspection_form.passenger_side_view.attachment)
            }
            if (data[0].inspection_form.rear_view) {
                setRearView(data[0].inspection_form.rear_view.attachment)
            }
            if (data[0].inspection_form.handtruck) {
                setHandtruckView(data[0].inspection_form.handtruck.attachment)
            }
            if (data[0].inspection_form.clean) {
                setVehicleClean(data[0].inspection_form.clean.answer)
            }
            if (data[0].inspection_form.parking_ticket) {
                setIsParkingTicket(data[0].inspection_form.parking_ticket.answer)
            }
            if (data[0].inspection_form.phone) {
                setPhone(data[0].inspection_form.phone.answer)
            }
            if (data[0].inspection_form.phone_charger) {
                setCharger(data[0].inspection_form.phone_charger.answer)
            }
            if (data[0].inspection_form.rescue) {
                setRescue(data[0].inspection_form.rescue.answer)
            }
            if (data[0].inspection_form.gas_card) {
                setGascard(data[0].inspection_form.gas_card.answer)
            }
            if (data[0].inspection_form.engine_light) {
                setEngineLight(data[0].inspection_form.engine_light.answer)
            }
            if (data[0].inspection_form.windshield_fluid) {
                setWiper(data[0].inspection_form.windshield_fluid.answer)
            }
        }
    }, [data]);

    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };
    }, []);

    useEffect(() => {
        if (auth.user && auth.user.company && auth.user.company.inspection) {
            setInspectionData(auth.user.company.inspection)
        }
    }, [auth.user]);

    const handleBackButtonClick = () => {
        navigation.goBack(null);
        return true;
    }


    let { loading } = driver;
    const isDarkMode = useColorScheme() === 'dark';
    console.log('////////', rearView)
    return (
        <View style={[local_styles.mainContainer, { backgroundColor: (isDarkMode) ? "#fff" : "#fff" }]}>
            <Loader loading={loading} />
            <ScrollView>
                <View style={local_styles.topBox}>
                    <Header
                        title={translate('report')}
                        inspectForm={true}
                        goBack={navigation.goBack}
                        sel_driver={sel_driver}
                        VehicleName={vehicleName}
                    />
                </View>

                <View style={{ alignItems: 'center', marginTop: 31, marginBottom: 20 }}>
                    {inspectionData.mileage && mileage ? <View style={local_styles.milegeBox}>
                        <LinearGradient
                            colors={['#FFFFFF', '#ABC8FF', '#4F8BFF']}
                            start={{ x: 0, y: 0.5 }}
                            end={{ x: 1, y: 0.5 }}
                            style={local_styles.milegeinnerBox}>
                            <View>
                                <Text style={[local_styles.milegeTxt, { color: Colors.light_black }]}>{translate('mileage')}</Text>
                                <Text style={[local_styles.milegeTxt1, { color: Colors.light_black }]}>{translate('mileagUsed')}</Text>
                            </View>
                            <View>
                                <Text style={[local_styles.milegeTxt, { color: Colors.light_black }]}>{mileage}</Text>
                                <Text style={[local_styles.milegeTxt1, { color: Colors.light_black, textAlign: 'center' }]}>{mileage}</Text>
                            </View>
                            <View>
                                <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                    <Text style={[local_styles.smallTxt, { color: Colors.dark_chocklet }]}>{translate('lastInput')}</Text>
                                    <Text style={[local_styles.smallTxt1, { color: Colors.dark_chocklet }]}>{translate('recordOn')}: Aug 11</Text>
                                    <Text style={[local_styles.smallTxt, { color: Colors.dark_blue }]}>2,564 m</Text>
                                </View>
                            </View>

                        </LinearGradient>
                    </View> : null}

                    {inspectionData.gas && gas ? <View style={local_styles.gasBox}>
                        <LinearGradient
                            colors={['#FFFFFF', '#ABC8FF', '#4F8BFF']}
                            start={{ x: 0, y: 0.5 }}
                            end={{ x: 1, y: 0.5 }}
                            style={local_styles.gasInnerBox}>
                            <View>
                                <Text style={[local_styles.milegeTxt, { color: Colors.light_black }]}>{translate('gas')}</Text>
                            </View>
                            <Text style={[local_styles.milegeTxt, { color: Colors.light_black }]}>{gas}</Text>
                            <View>
                                <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                    <Text style={[local_styles.smallTxt, { color: Colors.dark_chocklet }]}>{translate('lastInput')}</Text>
                                    <Text style={[local_styles.smallTxt, { color: Colors.dark_blue }]}>{gas}</Text>
                                </View>
                            </View>

                        </LinearGradient>
                    </View> : null}

                    {inspectionData.front_view && frontView.length > 0 ?
                        <InspectBox name={translate('frontView')} imgData={frontView} /> : null}

                    {inspectionData.driver_side_view && driverView.length > 0 ?
                        <InspectBox name={translate('driveView')} imgData={driverView} /> : null}

                    {inspectionData.passenger_side_view && passengerView.length > 0 ?
                        <InspectBox name={translate('passengView')} imgData={passengerView} /> : null}

                    {inspectionData.rear_view && rearView.length > 0 ?
                        <InspectBox name={translate('rearView')} imgData={rearView} /> : null}

                    {inspectionData.handtruck && handtruckView.length > 0 ?
                        <InspectBox name={translate('wrkingHandtruck')} imgData={handtruckView} /> : null}

                    {inspectionData.clean ?
                        <TextBox value={vehicleClean} name={translate('vehicleClean')} /> : null}

                    {inspectionData.parking_ticket ?
                        <TextBox value={isparkingTicket} name={translate('parkingTicket')} /> : null}

                    {inspectionData.phone ?
                        <TextBox value={phone} name={translate('phone')} /> : null}

                    {inspectionData.phone_charger ?
                        <TextBox value={charger} name={translate('phoneCharger')} /> : null}

                    {inspectionData.rescue ?
                        <TextBox value={rescue} name={translate('cmpltRescue')} boxName={'rescue'} /> : null}

                    {inspectionData.gas_card ?
                        <TextBox value={gasCard} name={translate('gadCard')} /> : null}

                    {inspectionData.engine_light ?
                        <TextBox value={engineLight} name={translate('engineLight')} /> : null}

                    {inspectionData.windshield_fluid ?
                        <TextBox value={wiper} name={translate('windShield')} /> : null}

                </View>
            </ScrollView>
        </View>
    );
};

const local_styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },
    topBox: {
        marginTop: 20,
        marginRight: 31,
        marginLeft: 17
    },
    milegeBox: {
        height: 73,
        width: width - 14,
        marginBottom: 20,
        elevation: 5
    },
    milegeinnerBox: {
        height: 73,
        width: width - 14,
        borderRadius: 18,
        flexDirection: 'row',
        // alignItems: 'center',
        justifyContent: 'space-between',
        // paddingRight: 10,
        padding: 17
    },
    milegeTxt: {
        fontSize: 16,
        fontWeight: '500'
    },
    milegeTxt1: {
        fontSize: 12,
        fontWeight: '500',
        paddingTop: 8
    },
    smallTxt: {
        fontSize: 10,
        fontWeight: '300',
        paddingBottom: 3
        // textAlign:'left'
    },
    smallTxt1: {
        fontSize: 8,
        fontWeight: '300',
        paddingBottom: 5
    },
    gasBox: {
        width: width - 14,
        height: 53,
        borderRadius: 17,
        // marginTop: 15,
        elevation: 5
    },
    gasInnerBox: {
        width: width - 14,
        height: 53,
        borderRadius: 17,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 17
    },
});

const mapStateToProps = (state: any) => ({
    auth: state.auth,
    driver: state.driver
})
export default connect(mapStateToProps)(InspectReport);

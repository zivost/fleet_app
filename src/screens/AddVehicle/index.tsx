/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useState, useEffect } from "react";
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    PermissionsAndroid,
    BackHandler
} from 'react-native';

import {
    Colors,
    Typography,
    setI18nConfig,
    translate,
    Form
} from "../../config";
import Loader from '../../../Components/Loader/index'
import { iOSUIKit, human } from 'react-native-typography'
import TextInputField from '../../../Components/TextInput/index'
import Button from "../../../Components/Button/index"
import Header from "../../../Components/Header/index"
import type { StackParamList } from "../../routes.types";
import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import { useDispatch, useSelector, connect } from 'react-redux'
import constants from '../../constants';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker'
import moment from "moment";
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { DamageModal } from '../../../Components/ModalView/index'

let width = Dimensions.get('window').width;
let height = Dimensions.get('window').height;

let emailRegx = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;

type Props = NativeStackScreenProps<StackParamList, 'AddVehicle'>;
const AddVehicle: FC<Props> = ({ navigation, auth, driver, route }) => {
    const isDarkMode = useColorScheme() === 'dark';
    const data = route?.params?.data || 'nodata';
    let myDatepicker: { onPressDate: () => void; };
    let myDatepicker_1: { onPressDate: () => void; };
    let myDatepicker_2: { onPressDate: () => void; };
    const dispatch = useDispatch();

    const damageList = [{ "backgroungColor": 'red', "name": 'Severely Damaged' },
    { "backgroungColor": 'yellow', "name": 'Moderately Damaged' },
    { "backgroungColor": 'green', "name": 'Good Condition' }]
    const [vanNo, setVanNo] = useState({ value: "", error: false, msg: "" });
    const [vin, setVin] = useState({ value: "", error: false, msg: "" });
    const [licence, setLicencee] = useState({ value: "", error: false, msg: "" });
    const [liecenState, setLicenceState] = useState({ value: "", error: false, msg: "" });
    const [make, setMake] = useState({ value: "", error: false, msg: "" });
    const [vanModel, setVanModel] = useState({ value: "", error: false, msg: "" });
    const [trim, setTrim] = useState({ value: "", error: false, msg: "" });
    const [Year, setYear] = useState({ value: "", error: false, msg: "" });
    const [dateRec, setDateRec] = useState({ value: "", error: false, msg: "" });
    const [dateInsur, setDateInsur] = useState({ value: "", error: false, msg: "" });
    const [insurExp, setInsurExp] = useState({ value: "", error: false, msg: "" });
    const [gasCard, setGasCard] = useState({ value: "", error: false, msg: "" });
    const [vehiclePic, setVehiclePic] = useState('');
    const [vehid, setId] = useState('');
    const [openDamageModal, setOpenDamageModal] = useState(false);
    const [damageImg, setDamageImg] = useState('');
    const [damageDetails, setDamageDetails] = useState('');
    const [damageFlag, setDamageFlag] = useState('');
    const [damages, setDamages] = useState([]);


    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };
    }, []);

    const handleBackButtonClick = () => {
        navigation.goBack(null);
        return true;
    }


    useEffect(() => {
        if (data != 'nodata') {
            setVanNo({ value: data.name, error: false, msg: "" })
            setVin({ value: data.vin, error: false, msg: "" })
            setLicencee({ value: data.plate, error: false, msg: "" })
            setLicenceState({ value: data.state, error: false, msg: "" })
            setMake({ value: data.make, error: false, msg: "" })
            setVanModel({ value: data.model, error: false, msg: "" })
            setTrim({ value: data.trim, error: false, msg: "" })
            setYear({ value: data.year ? data.year.toString() : '', error: false, msg: "" })
            setDateRec({ value: data.recieved ? moment(data.recieved).format("YYYY-MM-DD") : '', error: false, msg: "" })
            setDateInsur({ value: data.insured ? moment(data.insured).format("YYYY-MM-DD") : '', error: false, msg: "" })
            setInsurExp({ value: data.insurance_expires ? moment(data.insurance_expires).format("YYYY-MM-DD") : '', error: false, msg: "" })
            setGasCard({ value: data.gas_card, error: false, msg: "" })
            setId(data.id)
        }
    }, [data])

    const addVehicleProfil = () => {
        let carid = ''
        if (data != 'nodata') {
            carid = vehid
        }
        if (vanNo.value && vin.value && licence.value && liecenState.value && make.value && vanModel.value) {
            dispatch({
                type: constants("driver").sagas.createVehicleprof,
                payload: {
                    data: {
                        "name": vanNo.value,
                        "vin": vin.value,
                        "plate": licence.value,
                        "state": liecenState.value,
                        "make": make.value,
                        "model": vanModel.value,
                        "trim": trim.value,
                        "year": Year.value,
                        "recieved": moment(dateRec.value).format('YYYY-MM-DD'),
                        "gas_card": gasCard.value,
                        "insured": moment(dateInsur.value).format('YYYY-MM-DD'),
                        "insurance_expires": moment(insurExp.value).format('YYYY-MM-DD'),
                        "images": [
                            vehiclePic
                        ],
                        "damages": damages
                    },
                    id: carid,
                    sessionToken: auth.user.token,
                    navigation
                }
            })
        }
    }

    const imgUpload = async (data) => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                    title: "App Camera Permission",
                    message: "App needs access to your camera ",
                    buttonNeutral: "Ask Me Later",
                    buttonNegative: "Cancel",
                    buttonPositive: "OK"
                }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                let options = {
                    maxWidth: 1000, //speeds up compressImage to almost no time
                    maxHeight: 1000,
                };
                if (data == 'damagePic') {
                    launchCamera(options, (response) => {
                        if (response && response.assets && response.assets.length > 0) {
                            let img = response.assets[0];
                            dispatch({
                                type: constants("driver").sagas.uploadDamageImg,
                                payload: {
                                    data: {
                                        file: img,
                                    },
                                    type: 'damagePic',
                                    sessionToken: auth.user.token,
                                },
                                setImg: setImg
                            })
                        }
                    })
                } else {
                    launchCamera(options, (response) => {
                        if (response && response.assets && response.assets.length > 0) {
                            let img = response.assets[0];
                            // setDamageImg(img)
                            dispatch({
                                type: constants("driver").sagas.uploadDamageImg,
                                payload: {
                                    data: {
                                        file: img,
                                    },
                                    type: 'vehiclePic',
                                    sessionToken: auth.user.token,
                                },
                                setImg: setImg
                            })
                        }
                    })
                }
            } else {
                console.log("Camera permission denied");
            }
        } catch (err) {
            console.warn(err);
        }
    }

    const setImg = (data, type) => {
        if (type == 'vehiclePic') {
            setVehiclePic(data.data)
        } else if (type == 'damagePic') {
            setDamageImg(data.data)
        }

    }

    const selctGamage = (flag) => {
        setDamageFlag(flag)
    }

    const uploadDamage = () => {
        setOpenDamageModal(!openDamageModal)
        damages.push({ flag: damageFlag, detail: damageDetails, images: [damageImg] })
        setDamageImg('')
        setDamageFlag('')
        setDamageDetails('')
    }
    let { loading } = driver;
    return (
        <View style={[local_styles.mainContainer, { backgroundColor: (isDarkMode) ? "#fff" : "#fff" }]}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={local_styles.bottomBox}>
                    <Header
                        title={data != 'nodata' ? translate('editVehicleDetails') : translate('addCar')}
                        goBack={navigation.goBack} />
                    <View style={local_styles.inputView}>
                        <View style={{ alignItems: 'center', marginBottom: 40 }}>
                            {vehiclePic ? (
                                <Image source={{ uri: vehiclePic }} style={local_styles.imgBox} />
                            ) :
                                (<TouchableOpacity style={local_styles.imgBox} onPress={imgUpload}>
                                    <Image source={require("assets/images/plus.png")} style={local_styles.plusImg} />
                                </TouchableOpacity>)}
                            <Text style={[local_styles.addTxt, { color: Colors.light_gray }]}>{translate('carPic')}</Text>
                        </View>

                        <TextInputField
                            placeholder={translate('vanName')}
                            styles={[Form.TextInput, local_styles.textInput]}
                            defaultValue={vanNo.value}
                            value={vanNo.value}
                            keyboardType='phone-pad'
                            onChangeText={(text: string) => {
                                setVanNo({ value: text, error: false, msg: "" });
                            }}
                        />

                        <TextInputField
                            placeholder={translate('VIN')}
                            styles={[Form.TextInput, local_styles.textInput]}
                            defaultValue={vin.value}
                            value={vin.value}
                            keyboardType='email-address'
                            onChangeText={(text: string) => {
                                setVin({ value: text, error: false, msg: "" });
                            }}
                        />

                        <TextInputField
                            placeholder={translate('licenPlate')}
                            styles={[Form.TextInput, local_styles.textInput]}
                            defaultValue={licence.value}
                            value={licence.value}
                            keyboardType='email-address'
                            onChangeText={(text: string) => {
                                setLicencee({ value: text, error: false, msg: "" });
                            }}
                        />

                        <TextInputField
                            placeholder={translate('licenState')}
                            styles={[Form.TextInput, local_styles.textInput]}
                            defaultValue={liecenState.value}
                            value={liecenState.value}
                            keyboardType='email-address'
                            onChangeText={(text: string) => {
                                setLicenceState({ value: text, error: false, msg: "" });
                            }}
                        />

                        <TextInputField
                            placeholder={translate('Make')}
                            styles={[Form.TextInput, local_styles.textInput]}
                            defaultValue={make.value}
                            value={make.value}
                            keyboardType='email-address'
                            onChangeText={(text: string) => {
                                setMake({ value: text, error: false, msg: "" });
                            }}
                        />

                        <TextInputField
                            placeholder={translate('vehiclModel')}
                            styles={[Form.TextInput, local_styles.textInput]}
                            defaultValue={vanNo.value}
                            value={vanModel.value}
                            keyboardType='email-address'
                            onChangeText={(text: string) => {
                                setVanModel({ value: text, error: false, msg: "" });
                            }}
                        />

                        <TextInputField
                            placeholder={translate('trim')}
                            styles={[Form.TextInput, local_styles.textInput]}
                            defaultValue={trim.value}
                            value={trim.value}
                            keyboardType='email-address'
                            onChangeText={(text: string) => {
                                setTrim({ value: text, error: false, msg: "" });
                            }}
                        />

                        <TextInputField
                            placeholder={translate('year')}
                            styles={[Form.TextInput, local_styles.textInput]}
                            defaultValue={Year.value}
                            value={Year.value}
                            keyboardType='phone-pad'
                            onChangeText={(text: string) => {
                                setYear({ value: text, error: false, msg: "" });
                            }}
                        />

                        <TouchableOpacity style={local_styles.dateBox} onPress={() => myDatepicker.onPressDate()}>
                            <Text style={[local_styles.dateTxt, { color: Colors.light_gray }]}>{dateRec.value ? dateRec.value : translate('dateRec')}</Text>
                        </TouchableOpacity>

                        <DatePicker
                            ref={(datepicker: any) => myDatepicker = datepicker}
                            style={{ width: 0, height: 0 }}
                            // date={birthday.value}
                            mode="date"
                            duration={300}
                            format="YYYY-MM-DD"
                            showIcon={false}
                            hideText={true}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateInput: {
                                    borderWidth: 0,
                                    alignItems: "flex-start",
                                },
                                dateText: {
                                    // ...styles.dateFieldText,
                                },
                                btnTextConfirm: {
                                    color: "#000000",
                                },
                            }}
                            onDateChange={(_date: string) => { setDateRec({ value: _date, error: false, msg: "" }) }}
                        />

                        <TouchableOpacity style={local_styles.dateBox} onPress={() => myDatepicker_1.onPressDate()}>
                            <Text style={[local_styles.dateTxt, { color: Colors.light_gray }]}>{dateInsur.value ? dateInsur.value : translate('dateIns')}</Text>
                        </TouchableOpacity>

                        <DatePicker
                            ref={(datepicker: any) => myDatepicker_1 = datepicker}
                            style={{ width: 0, height: 0 }}
                            // date={birthday.value}
                            mode="date"
                            duration={300}
                            format="YYYY-MM-DD"
                            showIcon={false}
                            hideText={true}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateInput: {
                                    borderWidth: 0,
                                    alignItems: "flex-start",
                                },
                                dateText: {
                                    // ...styles.dateFieldText,
                                },
                                btnTextConfirm: {
                                    color: "#000000",
                                },
                            }}
                            onDateChange={(_date: string) => { setDateInsur({ value: _date, error: false, msg: "" }) }}
                        />

                        <TouchableOpacity style={local_styles.dateBox} onPress={() => myDatepicker_2.onPressDate()}>
                            <Text style={[local_styles.dateTxt, { color: Colors.light_gray }]}>{insurExp.value ? insurExp.value : translate('insuExp')}</Text>
                        </TouchableOpacity>

                        <DatePicker
                            ref={(datepicker: any) => myDatepicker_2 = datepicker}
                            style={{ width: 0, height: 0 }}
                            // date={birthday.value}
                            mode="date"
                            duration={300}
                            format="YYYY-MM-DD"
                            showIcon={false}
                            hideText={true}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateInput: {
                                    borderWidth: 0,
                                    alignItems: "flex-start",
                                },
                                dateText: {
                                    // ...styles.dateFieldText,
                                },
                                btnTextConfirm: {
                                    color: "#000000",
                                },
                            }}
                            onDateChange={(_date: string) => { setInsurExp({ value: _date, error: false, msg: "" }) }}
                        />


                        <TextInputField
                            placeholder={translate('gasCard')}
                            styles={[Form.TextInput, local_styles.textInput]}
                            defaultValue={vanNo.value}
                            keyboardType='email-address'
                            value={gasCard.value}
                            onChangeText={(text: string) => {
                                setGasCard({ value: text, error: false, msg: "" });
                            }}
                        />

                        <TouchableOpacity style={local_styles.damageBox} onPress={() => setOpenDamageModal(!openDamageModal)}>
                            <FontAwesomeIcon icon={faPlus} size={20} color={Colors.red} />
                            <Text style={[local_styles.damagTxt, { color: Colors.red }]}>Add Damage</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={local_styles.bottomBoxInner}>
                        <Button styles={local_styles.btnStyle} name={data != 'nodata' ? translate('save') : translate('addVehicle')} onPress={() => { addVehicleProfil() }} />
                    </View>
                </View>
            </ScrollView>
            <DamageModal
                openDamageModal={openDamageModal}
                damageList={damageList}
                closeDamageModal={() => {
                    setOpenDamageModal(!openDamageModal)
                    setDamageImg('')
                    setDamageFlag('')
                    setDamageDetails('')
                }}
                imgUpload={imgUpload}
                damageImg={damageImg}
                setDamageDetails={(text: string) => setDamageDetails(text)}
                selctGamage={selctGamage}
                damageFlag={damageFlag}
                uploadDamage={uploadDamage} />
        </View>
    );
};

const local_styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    imgBox: {
        height: 122,
        width: 122,
        borderRadius: 122 / 2,
        backgroundColor: '#D5D5D5',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 14
    },
    addTxt: {
        fontSize: 16,
        fontWeight: '400'
    },
    plusImg: {
        height: 31,
        width: 31,
        resizeMode: 'contain'
    },
    bottomBox: {
        // position:"absolute",
        width: "100%",
        // height: height / 1.7,
        backgroundColor: "#fff",
        borderTopLeftRadius: 24,
        borderTopRightRadius: 24,
        // bottom:0,
        paddingTop: 40,
        paddingLeft: 35,
        paddingRight: 35,
    },
    inputView: {
        marginTop: 50
    },
    textInput: {
        marginTop: 10,
    },
    bottomBoxInner: {
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        marginTop: 50,
        width: "100%",
        marginBottom: 30
    },
    btnStyle: {
        width: 138
    },
    dateBox: {
        borderBottomWidth: 1,
        height: 40,
        borderColor: '#D5D5D5',
        marginTop: 15
    },
    dateTxt: {
        paddingTop: 5,
        fontSize: 16,
        fontWeight: '400'
    },
    damageBox: {
        height: 40,
        width: width * 160 / 414,
        borderRadius: 20,
        borderWidth: 1,
        borderColor: '#D5D5D5',
        marginTop: 20,
        justifyContent: 'space-evenly',
        alignItems: 'center',
        flexDirection: 'row'
    },
    damagTxt: {
        fontSize: 15,
        fontWeight: '500'
    }
});
const mapStateToProps = (state: any) => ({
    auth: state.auth,
    driver: state.driver
})
export default connect(mapStateToProps)(AddVehicle);

/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useEffect, useState } from "react";
import {
    StyleSheet,
    useColorScheme,
    View,
    Image,
    Text,
    BackHandler,
    ScrollView
} from 'react-native';
import {
    Colors,
    Typography,
    setI18nConfig,
    translate,
    Form,
    goBack
} from "../../config";
import type { StackParamList } from "../../routes.types";
import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import { connect, useStore, useDispatch } from "react-redux";
import Header from "../../../Components/Header/index"
import Loader from '../../../Components/Loader/index'


type Props = NativeStackScreenProps<StackParamList, 'Profile'>;
const UserProfile: FC<Props> = ({ navigation, auth, route }) => {


    const store = useStore();
    const dispatch = useDispatch();
    const [userData, setuserData] = useState('');
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [phone, setPhone] = useState('')
    const [user_id, setUser_id] = useState('')

    useEffect(() => {
        if (auth.user) {
            setName(auth.user.name)
            setEmail(auth.user.email)
            setPhone(auth.user.phone)
            setUser_id(auth.user.id)
            setuserData(auth.user)
        }
    }, []);

    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };
    }, []);

    const handleBackButtonClick = () => {
        navigation.goBack(null);
        return true;
    }

    const navToedit = () => {
        let user_data = {
            name:name,
            email:email,
            phone:phone,
            user_id:user_id
        }
        navigation.navigate('EditUserDetails', {user_data})
    }

    let { loading } = auth;
    const isDarkMode = useColorScheme() === 'dark';
    return (
        <View style={[local_styles.mainContainer, { backgroundColor: (isDarkMode) ? "#fff" : "#fff" }]}>
            <Loader loading={loading} />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={local_styles.headerBox}>
                    <Header
                        title={translate('myAcc')}
                        profileMenu={true}
                        goBack={navigation.goBack}
                        navToEditScreen={navToedit}
                    />
                </View>
                <View style={local_styles.userBox}>
                    <Image source={require('assets/images/user_prof.png')} style={local_styles.userImg} />
                    <Text style={[local_styles.userTxt, { color: Colors.primary }]}>{name}</Text>
                </View>

                <View style={[local_styles.user_info_box, { marginTop: 41 }]}>
                    <Text style={[local_styles.menuTxt, { color: Colors.gray, width: 120 }]}>{translate('phoneNo')}</Text>
                    <Text style={[local_styles.smallTxt, { color: Colors.primary }]}>{phone}</Text>
                </View>

                <View style={[local_styles.user_info_box]}>
                    <Text style={[local_styles.menuTxt, { color: Colors.gray, width: 120 }]}>{translate('emailId')}</Text>
                    <Text style={[local_styles.smallTxt, { color: Colors.primary }]}>{email}</Text>
                </View>

                <View style={[local_styles.user_info_box]}>
                    <Text style={[local_styles.menuTxt, { color: Colors.gray, width: 120 }]}>{translate('id')}</Text>
                    <Text style={[local_styles.smallTxt, { color: Colors.primary }]}>{user_id}</Text>
                </View>
            </ScrollView>
        </View>
    );
};


const local_styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },
    headerBox: {
        marginLeft: 15,
        marginTop: 15,
        marginRight: 15
    },
    userBox: {
        marginTop: 30,
        alignItems: 'center'
    },
    userImg: {
        height: 145,
        width: 133,
        resizeMode: 'contain',
        marginBottom: 10
    },
    userTxt: {
        fontSize: 18,
        fontWeight: '500'
    },
    user_info_box: {
        marginBottom: 20,
        marginLeft: 48,
        marginRight: 20,
        flexDirection: 'row'
    },
    menuTxt: {
        fontSize: 14,
        fontWeight: '500'
    },
    smallTxt: {
        fontSize: 12,
        fontWeight: '500',
        textDecorationLine: 'underline'
    }
});

const mapStateToProps = (state: any) => ({
    auth: state.auth,
})
export default connect(mapStateToProps)(UserProfile);
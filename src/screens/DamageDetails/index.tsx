/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useState, useEffect } from "react";
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    FlatList,
    ImageBackground,
    BackHandler
} from 'react-native';

import {
    Colors,
    Typography,
    setI18nConfig,
    translate,
    Form
} from "../../config";
import type { StackParamList } from "../../routes.types";
import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import { iOSUIKit, human } from 'react-native-typography'
import { useNavigation } from '@react-navigation/native';
import TextInputField from '../../../Components/TextInput/index'
import Button from "../../../Components/Button/index"
import Header from "../../../Components/Header/index"
import AuthHeader from '../../../Components/AuthHeader/index'
import { useDispatch, useSelector, connect } from 'react-redux'
import constants from '../../constants';
import moment from "moment";

let width = Dimensions.get('window').width;
let height = Dimensions.get('window').height;

type Props = NativeStackScreenProps<StackParamList, 'DamageDetails'>;
const DamageDetails: FC<Props> = ({ navigation, driver, route, auth }) => {
    const isDarkMode = useColorScheme() === 'dark';
    const data = route?.params?.data || 'no data';
    // const navigation = useNavigation();
    const dispatch = useDispatch();
    const [damageCarData, setDamageCarData] = useState<any[]>([]);

    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };
    }, []);

    const handleBackButtonClick = () => {
        navigation.goBack(null);
        return true;
    }

    
    useEffect(() => {

        if (data && data.vehicle && data.vehicle.id) {
            dispatch({
                type: constants("driver").sagas.getDamageSpecificCar,
                payload: {
                    id: data.vehicle.id,
                    sessionToken: auth.user.token,
                }
            })
        }
    }, [])

    useEffect(() => {
        if (driver.dmageCarDetails.length > 0) {
            setDamageCarData(driver.dmageCarDetails)
        }

    }, [driver.dmageCarDetails])


    const _damageCarList = ({ item, index }) => {
        return (
            <View style={{ marginTop: 50 }}>
                <View style={local_styles.horiView}>
                    <Text style={[local_styles.bigTxt, { color: Colors.primary }]}>Inspection done {moment(item.created_at).format('DD MMM YYYY')}</Text>
                    <Text style={[local_styles.smallTxt, { color: Colors.primary, textAlign: 'left' }]}>(2 damages)</Text>
                </View>
                <View style={{ marginTop: 23 }}>
                    {item.image.map((item, index) => {
                        return (
                            <View key={"damageimages_" + index} style={local_styles.carBox}>
                                <Image source={{ uri: item }} style={local_styles.carStyle} />
                            </View>
                        )
                    })}
                    <Text style={[local_styles.smallTxt, { color: Colors.primary }]}>{item.detail}</Text>
                    <Text style={[local_styles.smallTxt1, { color: Colors.primary }]}>cost $5678</Text>
                </View>
            </View>
        )
    }


    return (
        <View style={[local_styles.mainContainer, { backgroundColor: (isDarkMode) ? "#fff" : "#fff" }]}>
            <View style={local_styles.headBox}>
                <Header
                    title={data.vehicle.name}
                    goBack={navigation.goBack}
                    damageCount={'5'}
                />

                <View>
                    <FlatList
                        data={damageCarData}
                        renderItem={_damageCarList}
                    />
                </View>


            </View>
        </View>
    );
};

const local_styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    headBox: {
        marginTop: 15,
        marginLeft: 15
    },
    topInnerBox: {
        marginTop: 22
    },
    horiView: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    bigTxt: {
        fontSize: 12,
        fontWeight: '500'
    },
    smallTxt: {
        fontSize: 8,
        fontWeight: '500',
        paddingLeft: 10,
        width: 87,
        textAlign: 'center'
    },
    smallTxt1: {
        fontSize: 5,
        fontWeight: '500',
        width: 87,
        textAlign: 'center'
    },
    carStyle: {
        height: 47,
        width: 87,
        marginBottom: 8
    },
    carBox: {
        marginRight: 28,
        // alignItems: 'center'
    }
});

const mapStateToProps = (state: any) => ({
    auth: state.auth,
    driver: state.driver
})
export default connect(mapStateToProps)(DamageDetails);

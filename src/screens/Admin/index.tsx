/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useState, useEffect } from "react";
import {
    ScrollView,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Dimensions,
    BackHandler
} from 'react-native';

import {
    Colors,
    Typography,
    setI18nConfig,
    translate,
    Form
} from "../../config";
import Loader from '../../../Components/Loader/index'
import { iOSUIKit, human } from 'react-native-typography'
import TextInputField from '../../../Components/TextInput/index'
import Button from "../../../Components/Button/index"
import Header from "../../../Components/Header/index"
import type { StackParamList } from "../../routes.types";
import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import { useDispatch, useSelector, connect } from 'react-redux'
import constants from '../../constants';

let width = Dimensions.get('window').width;
let height = Dimensions.get('window').height;
let emailRegx = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;

type Props = NativeStackScreenProps<StackParamList, 'Admin'>;
const Admin: FC<Props> = ({ navigation, auth, driver }) => {

    const isDarkMode = useColorScheme() === 'dark';
    const dispatch = useDispatch();

    const [email, setEmail] = useState({ value: "", error: false, msg: "" });
    const [password, setPassword] = useState({ value: "", error: false, msg: "" });



    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };
    }, []);

    const handleBackButtonClick = () => {
        navigation.goBack(null);
        return true;
    }



    const register = () => {
        if (email.value == "") {
            setEmail({
                ...email,
                error: true,
                msg: "Email is required!"
            })
        }
        if (password.value == "") {
            setPassword({
                ...password,
                error: true,
                msg: "Password is required!"
            })
        }

        if (email.error || password.error || password.value == "" || email.value == "") {
            return false;
        }
        dispatch({
            type: constants("auth").sagas.AddAdmin,
            payload: {
                data: {
                    "email": email.value,
                    "password": password.value
                },
                sessionToken: auth.user.token,
                navigation
            }
        })
    }


    let { loading } = driver;
    return (
        <View style={[local_styles.mainContainer, { backgroundColor: (isDarkMode) ? "#fff" : "#fff" }]}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={local_styles.bottomBox}>
                    <Header
                        title={'Admin Portal'}
                        goBack={navigation.goBack} />

                    <View style={local_styles.inputView}>

                        <TextInputField
                            placeholder={'Enter your Email Id'}
                            styles={[Form.TextInput]}
                            defaultValue={email.value}
                            keyboardType='email-address'
                            error={email.error}
                            errorMsg={email.msg}
                            onPress={() => setEmail({ value: "", error: false, msg: "" })}
                            onChangeText={(text: string) => {
                                let obj = { ...email }
                                if (emailRegx.test(text) === true) {
                                    obj = { value: text, error: false, msg: "" }
                                } else {
                                    obj = { value: text, error: true, msg: translate('pleaseEnterValidEmail') }
                                }
                                setEmail(obj)
                            }}
                        />
                        <TextInputField
                            placeholder={'Password'}
                            styles={[Form.TextInput]}
                            defaultValue={password.value}
                            secureTextEntry={true}
                            error={password.error}
                            errorMsg={password.msg}
                            onChangeText={(text: string) => {
                                setPassword({ value: text, error: false, msg: "" })
                            }}
                        />
                    </View>
                    <View style={local_styles.bottomBoxInner}>
                        <Button styles={local_styles.btnStyle} name={'Register'} onPress={() => { register() }} />
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};

const local_styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    imgBox: {
        height: 122,
        width: 122,
        borderRadius: 122 / 2,
        backgroundColor: '#D5D5D5',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 14
    },
    addTxt: {
        fontSize: 16,
        fontWeight: '400'
    },
    plusImg: {
        height: 145,
        width: 133
    },
    bottomBox: {
        width: "100%",
        backgroundColor: "#fff",
        borderTopLeftRadius: 24,
        borderTopRightRadius: 24,
        paddingTop: 20,
        paddingLeft: 25,
        paddingRight: 35,
    },
    inputView: {
        marginTop: 52
    },
    textInput: {
        marginTop: 10,
    },
    bottomBoxInner: {
        // justifyContent: "center",
        // alignContent: "center",
        alignItems: "center",
        marginTop: 343,
        width: "100%",
        marginBottom: 30
    },
    btnStyle: {
        width: 138
    }
});
const mapStateToProps = (state: any) => ({
    auth: state.auth,
    driver: state.driver
})
export default connect(mapStateToProps)(Admin);

/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useEffect } from "react";
import {
    StyleSheet,
    useColorScheme,
    View,
    Image
} from 'react-native';

import type { StackParamList } from "../../routes.types";
import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import auth from '@react-native-firebase/auth';
import { connect, useStore } from "react-redux";

const Auth = auth;

type Props = NativeStackScreenProps<StackParamList, 'SplashScreen'>;
const SplashScreen: FC<Props> = ({ navigation, auth }) => {
    const delay = 3;
    const store = useStore();
    useEffect(
        () => {
            let { logged_in, user } = auth;
            let { company } = user
            if(!Auth().currentUser){
                logged_in = false;
            }
            let timer1: any;
            timer1 = setTimeout(() => navigation.navigate(logged_in ? company ? 'Dashboard': 'RegisterCompany' : 'SignIn'), delay * 1000);
            return () => {
                clearTimeout(timer1);
            };
        },
        []
    );
    const isDarkMode = useColorScheme() === 'dark';
    return (
        <View style={[local_styles.mainContainer, { backgroundColor: (isDarkMode) ? "#fff" : "#fff" }]}>
            <View style={local_styles.box}>
                <Image style={local_styles.image} source={require("assets/images/logo.png")} />
                <Image style={local_styles.Lmdimage} source={require("assets/images/LMDmax.png")} />
            </View>
        </View>
    );
};

const local_styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        paddingTop: 50
    },
    box: {
        flex: 1,
        backgroundColor: "#fff",
        justifyContent: "center",
        alignItems: "center",
    },
    image: {
        width: 168,
        height: 104,
        resizeMode: "contain"
    },
    Lmdimage: {
        width: 145,
        height: 48,
        resizeMode: "contain",
    }
});

const mapStateToProps = (state: any) => ({
    auth: state.auth,
})
export default connect(mapStateToProps)(SplashScreen);
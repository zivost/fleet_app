/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useState, useEffect } from "react";
import {
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    BackHandler
} from 'react-native';

import {
    translate,
    Form,
    Colors
} from "../../config";
import { useDispatch, useSelector, connect } from 'react-redux'
import { human } from 'react-native-typography'
import type { StackParamList } from "../../routes.types";
import constants from '../../constants';
import { WorkInProgressModal } from '../../../Components/ModalView/index'
let width = Dimensions.get('window').width;
let height = Dimensions.get('window').height;

type Props = NativeStackScreenProps<StackParamList, 'Menu'>;
const Menu: FC<Props> = ({ navigation, auth }) => {
    const dispatch = useDispatch();
    const [workProgrssModal, setWorkProgrssModal] = useState(false);
    const [userData, setUserData] = useState('');

    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };
    }, []);

    useEffect(() => {
        setUserData(auth.user)
    }, [auth.user]);

    const handleBackButtonClick = () => {
        navigation.goBack(null);
        return true;
    }


    const navToScreen = (screen) => {
        if (screen == 'logout') {
            dispatch({
                type: constants("auth").sagas.logOut,
                payload: {
                    account_id: auth.user.account_id,
                    sessionToken: auth.user.token
                }
            })
            dispatch({
                type: constants("driver").reducers.logOut.set
            })
            navigation.navigate('SignIn')
        } else if (screen == 'editInspectform') {
            navigation.navigate('InitialInspection', { params: 'editInspectData' })
        } else if (screen == 'Profile') {
            navigation.navigate('UserProfile')
        }
    }
    return (
        <View style={[local_styles.mainContainer]}>
            <View style={local_styles.horiView}>
                <View style={local_styles.wholeBox}>
                    <View style={local_styles.userBox}>
                        <Image style={local_styles.userImg} source={require("assets/images/user.png")} />
                    </View>

                    <Text style={[local_styles.nameTxt, { color: Colors.primary }]}>{userData.name}</Text>

                    <View style={local_styles.menuBox}>
                        <TouchableOpacity style={local_styles.menuInnerBox} onPress={() => navToScreen('Profile')}>
                            <Text style={[local_styles.menuTxt, { color: Colors.dark_chocklet }]}>{translate('profile')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={local_styles.menuInnerBox} onPress={() => navigation.navigate('AdminList')}>
                            <Text style={[local_styles.menuTxt, { color: Colors.dark_chocklet }]}>{translate('admin')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={local_styles.menuInnerBox} onPress={() => navigation.navigate('AboutUs')}>
                            <Text style={[local_styles.menuTxt, { color: Colors.dark_chocklet }]}>{translate('abtCmpny')}</Text>
                        </TouchableOpacity>
                        {/* <TouchableOpacity style={local_styles.menuInnerBox} onPress={() => setWorkProgrssModal(!workProgrssModal)}>
                        <Text style={[local_styles.menuTxt, { color: Colors.dark_chocklet }]}>{translate('reports')}</Text>
                    </TouchableOpacity> */}
                        <TouchableOpacity style={local_styles.menuInnerBox} onPress={() => navToScreen('editInspectform')}>
                            <Text style={[local_styles.menuTxt, { color: Colors.dark_chocklet }]}>{translate('editInspectio')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={local_styles.menuInnerBox} onPress={() => navToScreen('logout')}>
                            <Text style={[local_styles.menuTxt, { color: Colors.dark_red }]}>{translate('logout')}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ justifyContent: 'center' }}>
                    <Image source={require("assets/images/drawer_Img.jpeg")} style={local_styles.drawerImg} />
                </View>
            </View>
            <WorkInProgressModal workProgrssModal={workProgrssModal} closeModal={() => setWorkProgrssModal(!workProgrssModal)} />
        </View>
    );
};

const local_styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    userImg: {
        width: 75,
        height: 75
    },
    userBox: {
        marginTop: 54,
        marginBottom: 46
    },
    wholeBox: {
        paddingLeft: 20
    },
    nameTxt: {
        fontSize: 24,
        fontWeight: '500'
    },
    menuBox: {
        marginTop: 33,
        marginLeft: 18
    },
    menuTxt: {
        fontSize: 16,
        fontWeight: '400'
    },
    menuInnerBox: {
        marginBottom: 22,
        width: 150
    },
    horiView: {
        flexDirection: 'row',
        // alignItems:'center',
        justifyContent: 'space-between',
        // backgroundColor:'#E5E5E5',
        height: height
    },
    drawerImg: {
        height: height - 240,
        width: width * 230 / 414,
        resizeMode: 'contain'
    }
});
const mapStateToProps = (state: any) => ({
    auth: state.auth,
})
export default connect(mapStateToProps)(Menu);
/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useState, useEffect, useCallback } from "react";
import {
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    BackHandler,
    Alert
} from 'react-native';
import { useDispatch, useSelector, connect } from 'react-redux'

import {
    translate,
    Form
} from "../../config";
import { human } from 'react-native-typography'
import TextInputField from '../../../Components/TextInput/index'
import Loader from '../../../Components/Loader/index'
import auth, { FirebaseAuthTypes } from '@react-native-firebase/auth';
let width = Dimensions.get('window').width;
let height = Dimensions.get('window').height;


let emailRegx = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;

import constants from '../../constants';
import type { StackParamList } from "../../routes.types";
import type { NativeStackScreenProps } from '@react-navigation/native-stack';

const Auth = auth;

type Props = NativeStackScreenProps<StackParamList, 'SignUp'>;
const SignUp: FC<Props> = ({ navigation }) => {
    const dispatch = useDispatch();
    const [email, setEmail] = useState({ value: "", error: false, msg: "" });
    const [name, setName] = useState({ value: "", error: false, msg: "" });
    const [phone, setPhone] = useState({ value: "", error: false, msg: "" });
    const [password, setPassword] = useState({ value: "", error: false, msg: "" });
    const [confirmPass, setConfirmPass] = useState({ value: "", error: false, msg: "" });
    const isDarkMode = useColorScheme() === 'dark';
    // Set an initializing state whilst Firebase connects
    const [initializing, setInitializing] = useState(true);
    const [user, setUser] = useState();
    const [token, setToken] = useState('null');
    let unsubscribe: any
    // const auth = useSelector(state => state.auth)

    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };
    }, []);

    const handleBackButtonClick = () => {
        navigation.goBack(null);
        return true;
    }


    // Handle user state changes
    useEffect(() => {
        unsubscribe = Auth().onAuthStateChanged(onAuthStateChanged);
        // unsubscribe to the listener when unmounting
        return () => unsubscribe()
    }, [name, email]);

    const onAuthStateChanged = async (user: any) => {
        const dispatch = useDispatch();
        signup(user);
    }

    const signUpFunc = async () => {
        if (name.value == "") {
            setName({
                ...name,
                error: true,
                msg: "Name is required!"
            })
        }
        if (email.value == "") {
            setEmail({
                ...email,
                error: true,
                msg: "Email is required!"
            })
        }
        if (password.value == "") {
            setPassword({
                ...password,
                error: true,
                msg: "Password is required!"
            })
        }
        if (phone.value == "") {
            setPhone({
                ...phone,
                error: true,
                msg: "Phone is required!"
            })
        }
        if (confirmPass.value == "") {
            setConfirmPass({
                ...confirmPass,
                error: true,
                msg: "Confirm Password is required!"
            })
        }
        // console.log(email,password,name,confirmPass,phone)
        if (
            email.error || password.error || confirmPass.error || phone.error || name.error
            || email.value == "" || phone.value == "" || password.value == "" || confirmPass.value == "" || name.value == ""
        ) {
            return false;
        }
        Auth().createUserWithEmailAndPassword(email.value, password.value).then((attempt_signup) => {
            console.log("signup ok",attempt_signup)
            Auth().signInWithEmailAndPassword(email.value, password.value).then((attempt_signin) => {
                signup(attempt_signin.user);
            }).catch((err) => {
                console.log("error", err)
            })
        }).catch((err) => {
            console.log("signup not ok",err)
            Auth().signInWithEmailAndPassword(email.value, password.value).then((attempt_signin) => {
                signup(attempt_signin.user);
            }).catch((err) => {
                // console.log("error", err)
                Alert.alert(
                    '',
                    "Something Went Wrong",
                    [
                        {
                            text: 'Ok', onPress: () =>
                            (
                                console.log()
                            )
                        },
                    ],
                    { cancelable: false },
                );
                // console.log("error", error)
                return err
            // });
        })
    });

    // Auth().createUserWithEmailAndPassword(email.value, password.value).then((res) => {
    //     console.log(res);
    //     Auth().signInWithEmailAndPassword(email.value, password.value).then((res2) => {
    //         console.log(res2);
    //     }).catch(err => {
    // if (err.code === 'auth/email-already-in-use') {
    //     console.log('That email address is already in use!');
    // }
    // if (err.code === 'auth/invalid-email') {
    //     console.log('That email address is invalid!');
    // }
    // console.log("error", err)
    //     });
    // });
    // clrTxt()
}
const signup = async function (User: FirebaseAuthTypes.User) {
    console.log("signin --");
    if (!User) {
        console.log("no user")
        return false;
    }
    let token = ""
    try {
        token = await User.getIdToken();
    } catch (err) {
        console.log('err', err)
    }
    if (token) {
        console.log("token ok")
        setToken(token)
        setUser(user);
        dispatch({
            type: constants("auth").sagas.signup,
            payload: {
                data: {
                    "email": email.value, // unique
                    "name": name.value,
                    "phone": phone.value, // unique
                    "token": token
                }
            }
        })
    }
}
const clrTxt = () => {
    setEmail({ value: "", error: false, msg: "" })
    setPassword({ value: "", error: false, msg: "" })
    setName({ value: "", error: false, msg: "" })
    setPhone({ value: "", error: false, msg: "" })
    setConfirmPass({ value: "", error: false, msg: "" })
}
return (
    <View style={[local_styles.mainContainer, { backgroundColor: (isDarkMode) ? "#fff" : "#fff" }]}>
        <View style={local_styles.box}>
            <Image style={local_styles.image} source={require("assets/images/signinBg.png")} />
        </View>
        <View style={local_styles.bottomBox}>
            <Text style={local_styles.headingText}>{translate("getStarted")}</Text>
            <View style={local_styles.inputView}>
                <TextInputField
                    placeholder={translate('name')}
                    styles={[Form.TextInput]}
                    defaultValue={name.value}
                    error={name.error}
                    errorMsg={name.msg}
                    onChangeText={(text: string) => {
                        setName({ value: text, error: false, msg: "" });
                    }}
                />
                <TextInputField
                    placeholder={translate('email')}
                    styles={[Form.TextInput, local_styles.textInput]}
                    defaultValue={email.value}
                    keyboardType='email-address'
                    error={email.error}
                    errorMsg={email.msg}
                    onChangeText={(text: string) => {
                        let obj = { ...email }
                        if (emailRegx.test(text) === true) {
                            obj = { value: text, error: false, msg: "" }
                        } else {
                            obj = { value: text, error: true, msg: translate('pleaseEnterValidEmail') }
                        }
                        setEmail(obj)
                    }}
                />
                <TextInputField
                    placeholder={translate('phoneNo')}
                    styles={[Form.TextInput, local_styles.textInput]}
                    defaultValue={phone.value}
                    keyboardType='phone-pad'
                    error={phone.error}
                    errorMsg={phone.msg}
                    onChangeText={(text: string) => {
                        let obj = { ...phone };
                        if (text.length < 10) {
                            obj = { value: text, error: true, msg: "Enter a valid Phone number!" }
                        } else {
                            obj = { value: text, error: false, msg: "" }
                        }
                        setPhone(obj);
                    }}
                />
                <TextInputField
                    placeholder={translate('password')}
                    styles={[Form.TextInput, local_styles.textInput]}
                    defaultValue={password.value}
                    secureTextEntry={true}
                    error={password.error}
                    errorMsg={password.msg}
                    onChangeText={(text: string) => {
                        setPassword({ value: text, error: false, msg: "" })
                    }}
                />
                <TextInputField
                    placeholder={translate('confirmPass')}
                    styles={[Form.TextInput, local_styles.textInput]}
                    defaultValue={confirmPass.value}
                    secureTextEntry={true}
                    error={confirmPass.error}
                    errorMsg={confirmPass.msg}
                    onChangeText={(text: string) => {
                        let obj = { ...confirmPass }
                        if (text == password.value) {
                            obj = { value: text, error: false, msg: "" }
                        } else {
                            obj = { value: text, error: true, msg: translate('passwordDontMatch') }
                        }
                        setConfirmPass(obj)
                    }}
                />
            </View>
            <View style={local_styles.bottomBoxInner}>
                <TouchableOpacity style={local_styles.leftSide} onPress={() => navigation.navigate('SignIn')}>
                    <Text style={local_styles.smallText}>{translate('alreadyHav')}</Text>
                    <Text style={local_styles.signText}>{translate('signin')}</Text>
                </TouchableOpacity>
                <View style={local_styles.rightSide}>
                    <TouchableOpacity disabled={(email.error || password.error || confirmPass.error || phone.error || name.error)} style={[local_styles.innerCircle, (email.error || password.error || confirmPass.error || phone.error || name.error) ? local_styles.disabled : {}]} onPress={() => { signUpFunc() }}>
                        <Image source={require("assets/images/rightArrow.png")} />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    </View>
);
};

const local_styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    box: {
        height: height / 1.7,
        width: "100%",
        backgroundColor: "#fff",
    },
    image: {
        width: "100%",
        resizeMode: "cover",
        height: "100%"
    },
    bottomBox: {
        position: "absolute",
        width: "100%",
        height: height / 1.3,
        backgroundColor: "#fff",
        borderTopLeftRadius: 24,
        borderTopRightRadius: 24,
        bottom: 0,
        paddingTop: 40,
        paddingLeft: 35,
        paddingRight: 35,
    },
    headingText: {
        ...human.title1Object,
        color: "#4F8BFF",
        fontWeight: "bold"
    },
    inputView: {
        marginTop: 30
    },
    textInput: {
        marginTop: 10,
    },
    forgotView: {
        justifyContent: "flex-end",
        width: "100%",
        marginTop: 8
    },
    forgotText: {
        ...human.caption1Object,
        color: "#4F8BFF",
        fontWeight: "500",
        textAlign: "right"
    },
    bottomBoxInner: {
        justifyContent: "space-between",
        flexDirection: 'row',
        alignContent: "center",
        marginTop: 40
        // alignSelf:"center"
    },
    leftSide: {
        // borderWidth:1,
        justifyContent: "center",
        flexDirection: "row",
        alignItems: "center"
    },
    rightSide: {

    },
    signText: {
        ...human.subheadObject,
        color: "#4F8BFF",
        fontWeight: 'bold'
    },
    smallText: {
        ...human.caption2Object,
        color: "#A7A8A9",
        fontWeight: '500',
        marginRight: 5
    },
    innerCircle: {
        width: 65,
        height: 65,
        backgroundColor: "#4F8BFF",
        borderRadius: 50,
        justifyContent: "center",
        alignItems: "center"
    },
    disabled: {
        opacity: 0.7
    }
});

const mapStateToProps = (state: any) => ({
    auth: state.auth,
})
export default connect(mapStateToProps)(SignUp);
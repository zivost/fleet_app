/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useEffect, useState } from "react";
import {
    StyleSheet,
    useColorScheme,
    View,
    Image,
    FlatList,
    Text,
    TouchableOpacity,
    ActivityIndicator,
    ScrollView,
    BackHandler
} from 'react-native';
import {
    Colors,
    Typography,
    setI18nConfig,
    translate,
    Form
} from "../../config";
// import LinearGradient from 'react-native-linear-gradient';
// import Loader from '../../../Components/Loader/index'
import Header from "../../../Components/Header/index"
import { DriverBox, DamageBox } from '../../../Components/DriverBox/index'
import { useDispatch, useSelector, connect } from 'react-redux'
import constants from '../../constants';
import { useIsFocused } from '@react-navigation/core';

const VehicleList: FC<Props> = ({ navigation, driver, route, auth }) => {

    const screen = route?.params.params;

    const dispatch = useDispatch();
    const [damageData, setDamageData] = useState<any[]>([]);
    const [headerTxt, setHeaderTxt] = useState('');
    const [offset, setOffset] = useState<number>(0);
    const [currLength, setCurrLength] = useState(0);
    const [damageCount, setDamageCount] = useState(0);
    const [activeCount, setActiveCount] = useState(0);
    const [inActiveCount, setInActiveCount] = useState(0);
    const [loader, setLoading] = useState(false)



    useEffect(() => {
        if (driver.getAllData && driver.getAllData.length > 0) {
            if (screen == 'Vehicle') {
                let arrList = driver.getAllData
                let active = 0;
                let inactive = 0;
                for (let i in arrList) {
                    if (arrList[i].is_active) {
                        active++;
                    } else {
                        inactive++;
                    }
                }
                setActiveCount(active)
                setInActiveCount(inactive)
            }
            setDamageData(driver.getAllData)
        }
        setDamageCount(driver.damageListmetaData.total)
    }, [driver.getAllData])

    const act_vehicle = () => {
        let arrList = driver.getAllData
        let active_list = []
        for (let i in arrList) {
            if (arrList[i].is_active) {
                active_list.push(arrList[i])
            }
        }
        setDamageData(active_list)
    }
    const inAct_vehicle = () => {
        let arrList = driver.getAllData
        let active_list = []
        for (let i in arrList) {
            if (!arrList[i].is_active) {
                active_list.push(arrList[i])
            }
        }
        setDamageData(active_list)
    }

    useEffect(() => {
        let paramsTrending = '&limit=50&offset=0'
        if (screen == 'Damage') {
            dispatch({
                type: constants("driver").sagas.getDamagesList,
                payload: {
                    data: paramsTrending,
                    sessionToken: auth.user.token,
                    vehiclelist: true
                }
            })
        } else if (screen == 'Vehicle') {
            dispatch({
                type: constants("driver").sagas.getVehiclesList,
                payload: {
                    data: paramsTrending,
                    sessionToken: auth.user.token,
                    vehiclelist: true
                }
            })
        } else if (screen == 'Dispatchers') {
            dispatch({
                type: constants("driver").sagas.getDispatchersList,
                payload: {
                    data: paramsTrending,
                    sessionToken: auth.user.token,
                    vehiclelist: true
                }
            })
        } else if (screen == 'Drivers') {
            dispatch({
                type: constants("driver").sagas.getDriverList,
                payload: {
                    data: paramsTrending,
                    sessionToken: auth.user.token,
                    vehiclelist: true
                }
            })
        }
    }, [])

    const isFocused = useIsFocused();
    
    useEffect(() => {
        // console.log("driver.last_update ---- triggered", screen, navigation.isFocused())
        // if (navigation.isFocused()) {
            let paramsTrending = '&limit=50&offset=0'
            
            if (screen == 'Damage') {
                dispatch({
                    type: constants("driver").sagas.getDamagesList,
                    payload: {
                        data: paramsTrending,
                        sessionToken: auth.user.token,
                        vehiclelist: true
                    }
                })
            } else if (screen == 'Vehicle') {
                dispatch({
                    type: constants("driver").sagas.getVehiclesList,
                    payload: {
                        data: paramsTrending,
                        sessionToken: auth.user.token,
                        vehiclelist: true
                    }
                })
            } else if (screen == 'Dispatchers') {
                dispatch({
                    type: constants("driver").sagas.getDispatchersList,
                    payload: {
                        data: paramsTrending,
                        sessionToken: auth.user.token,
                        vehiclelist: true
                    }
                })
            } else if (screen == 'Drivers') {
                dispatch({
                    type: constants("driver").sagas.getDriverList,
                    payload: {
                        data: paramsTrending,
                        sessionToken: auth.user.token,
                        vehiclelist: true
                    }
                })
            }
        // }
    }, [isFocused]);

    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };
    }, []);

    const handleBackButtonClick = () => {
        dispatch({
            type: constants('driver').reducers.getAllData.set
        })
        navigation.goBack(null);
        return true;
    }


    const _driverList = ({ item, index }) => {
        return (
            <DriverBox data={item} screen={screen} navToprofile={navToProfile} />
        )
    }

    const _damageList = ({ item, index }) => {
        return (
            <DamageBox data={item} navToDetails={navToDetails} />
        )
    }

    const navToDetails = (data) => {
        navigation.navigate('DamageDetails', { data })
    }

    const navToProfile = (data) => {

        navigation.navigate('Profile', {
            data,
            screen
        })
    }

    const fetchCategory = () => {
        let oldOffset: number = offset;
        let currLen = driver.getAllData.length;
        if (currLen > currLength) {
            let offset: number = oldOffset + 20;
            let paramsTrending = 'limit=50&offset=' + offset
            if (screen == 'Damage') {
                dispatch({
                    type: constants("driver").sagas.getDamagesList,
                    payload: {
                        data: paramsTrending,
                        sessionToken: auth.user.token,
                        vehiclelist: true
                    }
                })
                setOffset(oldOffset + 20)
                setLoading(true)
                setCurrLength(currLen)
            } else if (screen == 'Vehicle') {
                dispatch({
                    type: constants("driver").sagas.getVehiclesList,
                    payload: {
                        data: paramsTrending,
                        sessionToken: auth.user.token,
                        vehiclelist: true
                    }
                })
                setOffset(oldOffset + 20)
                setLoading(true)
                setCurrLength(currLen)
            } else if (screen == 'Dispatchers') {
                dispatch({
                    type: constants("driver").sagas.getDispatchersList,
                    payload: {
                        data: paramsTrending,
                        sessionToken: auth.user.token,
                        vehiclelist: true
                    }
                })
                setOffset(oldOffset + 20)
                setLoading(true)
                setCurrLength(currLen)
            } else if (screen == 'Drivers') {
                dispatch({
                    type: constants("driver").sagas.getDriverList,
                    payload: {
                        data: paramsTrending,
                        sessionToken: auth.user.token,
                        vehiclelist: true
                    }
                })
                setOffset(oldOffset + 20)
                setLoading(true)
                setCurrLength(currLen)
            }
        }

        setTimeout(() => { setLoading(false) }, 4000)
    }

    const _footerLoader = () => {
        if (loading) {
            return (
                <View style={local_styles.loaderView}>
                    <ActivityIndicator size="large" color={Colors.primary} />
                    <Text style={[local_styles.loadingText, { color: Colors.primary }]}>Loading…</Text>
                </View>
            )
        } else {
            return (
                <View style={local_styles.loaderView}>

                </View>
            )
        }
    }

    const _loadingOval = () => {
        if (!loading) {
            return (
                <View>
                    <Text style={local_styles.emptMsg}>No Data found!</Text>
                </View>
            )
        } else {
            return (<View />)
        }
    }


    const navScree = () => {
        if (screen == "Drivers") {
            navigation.navigate('AddDriver')
        } else if (screen == "Vehicle") {
            navigation.navigate('AddVehicle')
        } else if (screen == "Dispatchers") {
            navigation.navigate('AddDispatcher')
        }
    }

    let { loading } = driver;
    const isDarkMode = useColorScheme() === 'dark';
    return (
        <View style={[local_styles.mainContainer, { backgroundColor: (isDarkMode) ? "#fff" : "#fff" }]}>
            <ScrollView showsVerticalScrollIndicator={false}>
                {/* <View style={local_styles.listView}> */}
                {screen == "Drivers" ? (
                    <View>
                        <View style={local_styles.topBox}>
                            <Header
                                title={screen}
                                goBack={handleBackButtonClick}
                                addBtn={true}
                                btnTxt={screen}
                                navScree={navScree}
                            />
                        </View>
                        <View style={local_styles.listView}>
                            <FlatList
                                data={damageData}
                                renderItem={_driverList}
                                numColumns={3}
                                keyExtractor={(item, index) => index.toString()}
                                onEndReached={fetchCategory}
                                onEndReachedThreshold={0.9}
                                initialNumToRender={50}
                                ListFooterComponent={_footerLoader}
                                scrollEnabled={true}
                                style={{ paddingTop: 20 }}
                                showsVerticalScrollIndicator={false}
                                ListEmptyComponent={_loadingOval} />
                        </View>
                    </View>
                ) : null}
                {screen == "Vehicle" ? (
                    <View>
                        <View style={local_styles.topBox}>
                            <Header
                                title={screen}
                                goBack={handleBackButtonClick}
                                addBtn={true}
                                btnTxt={screen}
                                navScree={navScree}
                            />
                        </View>
                        <View style={local_styles.vehicleTopBox}>
                            <TouchableOpacity style={[local_styles.boxStyle, { backgroundColor: '#2DA04E' }]} onPress={act_vehicle}>
                                <Text style={[local_styles.actTxt, { color: Colors.white }]}>{translate('active')}</Text>
                                <Text style={[local_styles.noTxt, { color: Colors.white }]}>{activeCount}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[local_styles.boxStyle, { backgroundColor: '#F4B455' }]} onPress={inAct_vehicle}>
                                <Text style={[local_styles.actTxt, { color: Colors.white }]}>{translate('inActive')}</Text>
                                <Text style={[local_styles.noTxt, { color: Colors.white }]}>{inActiveCount}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ marginLeft: 10 }}>
                            <FlatList
                                data={damageData}
                                renderItem={_driverList}
                                numColumns={3}
                                keyExtractor={(item, index) => index.toString()}
                                onEndReached={fetchCategory}
                                onEndReachedThreshold={0.9}
                                initialNumToRender={50}
                                ListFooterComponent={_footerLoader}
                                scrollEnabled={true}
                                style={{ paddingTop: 20 }}
                                showsVerticalScrollIndicator={false}
                                ListEmptyComponent={_loadingOval} />
                        </View>
                    </View>
                ) : null}
                {screen == "Dispatchers" ? (
                    <View>
                        <View style={local_styles.topBox}>
                            <Header
                                title={screen}
                                goBack={handleBackButtonClick}
                                addBtn={true}
                                btnTxt={screen}
                                navScree={navScree}
                                role={auth.user.role}
                            />
                        </View>
                        <View style={{ marginLeft: 10 }}>
                            <FlatList
                                data={damageData}
                                renderItem={_driverList}
                                numColumns={3}
                                keyExtractor={(item, index) => index.toString()}
                                onEndReached={fetchCategory}
                                onEndReachedThreshold={0.9}
                                initialNumToRender={50}
                                ListFooterComponent={_footerLoader}
                                scrollEnabled={true}
                                style={{ paddingTop: 20 }}
                                showsVerticalScrollIndicator={false}
                                ListEmptyComponent={_loadingOval} />
                        </View>
                    </View>
                ) : null}
                {screen == "Damage" ? (
                    <View>
                        <View style={local_styles.topBox}>
                            <Header
                                title={screen}
                                goBack={handleBackButtonClick}
                                damageCount={damageCount}
                            />
                        </View>
                        <View style={{}}>
                            <FlatList
                                data={damageData}
                                renderItem={_damageList}
                                numColumns={20}
                                keyExtractor={(item, index) => index.toString()}
                                onEndReached={fetchCategory}
                                onEndReachedThreshold={0.5}
                                initialNumToRender={50}
                                ListFooterComponent={_footerLoader}
                                scrollEnabled={true}
                                style={{ paddingTop: 20 }}
                                showsVerticalScrollIndicator={false}
                                ListEmptyComponent={_loadingOval} />
                        </View>
                    </View>
                ) : null}
                {/* </View> */}
            </ScrollView>
        </View>
    );
};

const local_styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },
    listView: {
        // alignItems: 'center'
    },
    outerBox: {
        width: 150,
        height: 180,
        borderRadius: 10,
        marginBottom: 15,
        alignItems: 'center'
    },
    avtarBox: {
        marginTop: 15,
        marginBottom: 15
    },
    bckimage: {
        height: 100,
        width: 100,
        borderRadius: 20,
        resizeMode: 'contain'
    },
    userTxt: {
        fontSize: 15,
        fontWeight: '500'
    },
    damgBox: {
        marginTop: 3,
        alignItems: 'center'
    },
    damagTxt: {
        fontSize: 12,
        fontWeight: '300'
    },
    topBox: {
        marginTop: 15,
        marginLeft: 15,
        marginBottom: 10,
        marginRight: 24
    },
    loaderView: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center"
    },
    loadingText: {
        fontSize: 18,
        fontWeight: "500",
        fontStyle: "normal",
        textAlign: "left",
        marginLeft: 9
    },
    emptMsg: {
        fontSize: 10,
        fontWeight: "600",
        fontStyle: "normal",
        lineHeight: 18,
        letterSpacing: 0,
        textAlign: "center",
        color: "#000",
    },
    vehicleTopBox: {
        marginTop: 39,
        marginLeft: 27,
        marginRight: 27,
        marginBottom: 26,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    boxStyle: {
        height: 54,
        width: 139,
        borderRadius: 20,
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 15,
        paddingRight: 15,
        justifyContent: 'space-between'
    },
    actTxt: {
        fontSize: 14,
        fontWeight: '500'
    },
    noTxt: {
        fontSize: 20,
        fontWeight: '700'
    }
});

const mapStateToProps = (state: any) => ({
    auth: state.auth,
    driver: state.driver
})
export default connect(mapStateToProps)(VehicleList);

/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useState, useEffect } from "react";
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    FlatList,
    ImageBackground,
} from 'react-native';

import {
    Colors,
    Typography,
    setI18nConfig,
    translate,
    Form
} from "../../config";

import { iOSUIKit, human } from 'react-native-typography'
import { useNavigation } from '@react-navigation/native';
import TextInputField from '../../../Components/TextInput/index'
import Button from "../../../Components/Button/index"
import Header from "../../../Components/Header/index"
import AuthHeader from '../../../Components/AuthHeader/index'

let width = Dimensions.get('window').width;
let height = Dimensions.get('window').height;

export type SectionProps = {
    children: number;
}

export type FlatListProps = {
    item: any;
}

const horizontalListView: FC<SectionProps> = (children) => {
    return (
        <View style={local_styles.sectionContainer}>
            <ImageBackground style={local_styles.imgBg1} resizeMode='cover' source={require("assets/images/dispatchPmg.png")}>

            </ImageBackground>
        </View>
    );
}

const Home: FC<FlatListProps> = () => {
    const isDarkMode = useColorScheme() === 'dark';
    const navigation = useNavigation();
    return (
        <View style={[local_styles.mainContainer, { backgroundColor: (isDarkMode) ? "#fff" : "#fff" }]}>
            <AuthHeader name={translate("addInspection")} />
            <View style={local_styles.listView}>
                <View style={local_styles.listHeadView}>
                    <Text style={local_styles.leftTitle}>{translate("dispatchers")}</Text>
                    <Text style={local_styles.rightTitle}>{translate("seeAll")}</Text>
                </View>
                <View style={local_styles.flatListView}>
                    <FlatList
                        showsHorizontalScrollIndicator={false}
                        horizontal={true}
                        data={[1,2,3,4,5,6,7,8,9]}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={(item:any) => horizontalListView(item)}
                        initialNumToRender={5}
                    />
                </View>
            </View>
        </View>
    );
};

const local_styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    listHeadView: {
        flexDirection: "row",
        justifyContent: "space-between",
        paddingLeft: 28,
        paddingRight: 28,
        alignItems: "center"
    },
    headingText: {
        ...human.title1Object,
        color: "#4F8BFF",
        fontWeight: "bold"
    },
    leftTitle: {
        ...human.calloutObject,
        color: "#4F8BFF",
        fontWeight: "bold"
    },
    rightTitle: {
        ...human.caption2Object,
        color: "#4F8BFF",
        fontWeight: "bold",
        textDecorationLine: "underline",
    },
    listView: {

    },
    flatListView:{
        marginTop:16,
    },
    imgBg1:{
        width:"100%",
        height:"100%"
    },
    sectionContainer:{
        width:85,
        height:100,
        // borderWidth:1,
        borderRadius:10,
        marginRight:20
    }
});

export default Home;

/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useEffect, useState } from "react";
import {
    StyleSheet,
    useColorScheme,
    View,
    Image,
    Text,
    BackHandler,
    ScrollView
} from 'react-native';
import {
    Colors,
    Typography,
    setI18nConfig,
    translate,
    Form,
    goBack
} from "../../config";
import type { StackParamList } from "../../routes.types";
import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import { connect, useStore, useDispatch } from "react-redux";
import Header from "../../../Components/Header/index"
import Loader from '../../../Components/Loader/index'


type Props = NativeStackScreenProps<StackParamList, 'Profile'>;
const AboutUs: FC<Props> = ({ navigation, auth, route }) => {


    const store = useStore();
    const dispatch = useDispatch();
    const [userData, setuserData] = useState('');
    const [cmpName, setCmpName] = useState('');
    const [ownName, setOwnName] = useState('');
    const [address, setAddress] = useState('');
    const [city, setCity] = useState('');
    const [pinCode, setPinCode] = useState('');
    const [code1, setCode1] = useState('');
    const [code2, setCode2] = useState('');
    const [cmpId, setCmpId] = useState('');

    useEffect(() => {
        if (auth.user && auth.user.company) {
            setCmpName(auth.user.company.company_name)
            setOwnName(auth.user.company.owner_name)
            setAddress(auth.user.company.address)
            setCity(auth.user.company.city)
            setPinCode(auth.user.company.zipcode)
            setCode1(auth.user.company.station_code)
            setCode2(auth.user.company.dsp_short_code)
            setCmpId(auth.user.company.company_id)
        }
    }, []);

    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };
    }, []);

    const handleBackButtonClick = () => {
        navigation.goBack(null);
        return true;
    }

    const navToedit = () => {
        let data = {
            cmpName: cmpName,
            ownName: ownName,
            address: address,
            city: city,
            pinCode: pinCode,
            code1: code1,
            code2: code2,
            cmpId: cmpId
        }
        navigation.navigate('RegisterCompany', { data });
    }

    let { loading } = auth;
    const isDarkMode = useColorScheme() === 'dark';
    return (
        <View style={[local_styles.mainContainer, { backgroundColor: (isDarkMode) ? "#fff" : "#fff" }]}>
            <Loader loading={loading} />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={local_styles.headerBox}>
                    <Header
                        title={'About Us'}
                        profileMenu={auth.user.role === 'owner' ? true : false}
                        goBack={navigation.goBack}
                        navToEditScreen={navToedit}
                    />
                </View>
                <View style={local_styles.userBox}>
                    <Image source={require('assets/images/Company_Img.png')} style={local_styles.userImg} />
                    <Text style={[local_styles.userTxt, { color: Colors.primary }]}>{cmpName}</Text>
                </View>

                <View style={[local_styles.user_info_box, { marginTop: 41 }]}>
                    <Text style={[local_styles.menuTxt, { color: Colors.gray, width: 140 }]}>Owner’s name</Text>
                    <Text style={[local_styles.smallTxt, { color: Colors.primary }]}>{ownName}</Text>
                </View>

                <View style={[local_styles.user_info_box]}>
                    <Text style={[local_styles.menuTxt, { color: Colors.gray, width: 140 }]}>Address</Text>
                    <Text style={[local_styles.smallTxt, { color: Colors.primary }]}>{address}</Text>
                </View>

                <View style={[local_styles.user_info_box]}>
                    <Text style={[local_styles.menuTxt, { color: Colors.gray, width: 140 }]}>City</Text>
                    <Text style={[local_styles.smallTxt, { color: Colors.primary }]}>{city}</Text>
                </View>

                <View style={[local_styles.user_info_box]}>
                    <Text style={[local_styles.menuTxt, { color: Colors.gray, width: 140 }]}>Pincode</Text>
                    <Text style={[local_styles.smallTxt, { color: Colors.primary }]}>{pinCode}</Text>
                </View>

                <View style={[local_styles.user_info_box]}>
                    <Text style={[local_styles.menuTxt, { color: Colors.gray, width: 140 }]}>Code 1</Text>
                    <Text style={[local_styles.smallTxt, { color: Colors.primary }]}>{code1}</Text>
                </View>

                <View style={[local_styles.user_info_box]}>
                    <Text style={[local_styles.menuTxt, { color: Colors.gray, width: 140 }]}>Code 2</Text>
                    <Text style={[local_styles.smallTxt, { color: Colors.primary }]}>{code2}</Text>
                </View>
            </ScrollView>
        </View>
    );
};


const local_styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },
    headerBox: {
        marginLeft: 15,
        marginTop: 15,
        marginRight: 15
    },
    userBox: {
        marginTop: 30,
        alignItems: 'center'
    },
    userImg: {
        height: 145,
        width: 133,
        resizeMode: 'contain',
        marginBottom: 10
    },
    userTxt: {
        fontSize: 18,
        fontWeight: '500'
    },
    user_info_box: {
        marginBottom: 20,
        marginLeft: 48,
        marginRight: 20,
        flexDirection: 'row',
        // justifyContent:'space-between'
    },
    menuTxt: {
        fontSize: 14,
        fontWeight: '400'
    },
    smallTxt: {
        fontSize: 12,
        fontWeight: '500',
        textDecorationLine: 'underline',
        width: 190
    }
});

const mapStateToProps = (state: any) => ({
    auth: state.auth,
})
export default connect(mapStateToProps)(AboutUs);
/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type { FC } from 'react';
import { useEffect, useState } from "react";
import {
    StyleSheet,
    useColorScheme,
    View,
    Image,
    Text,
    TouchableOpacity,
    BackHandler
} from 'react-native';
import { human } from 'react-native-typography'
import Button from "../../../Components/Button/index"
import {
    Colors,
    Typography,
    setI18nConfig,
    translate,
    Form
} from "../../config";
import Header from "../../../Components/Header/index"
import constants from '../../constants';
import { useDispatch, useSelector, connect } from 'react-redux'
import Loader from '../../../Components/Loader/index'

const InitialInspection: FC<Props> = ({ navigation, route, auth, driver }) => {

    const { data } = route.params.params;

    useEffect(() => {
        if (route.params && route.params.params) {
            setCompnyInfo(route.params.params);
        }
    }, [route.params]);

    useEffect(() => {
        if (auth.user && auth.user.company) {
            if (auth.user.company.inspection) {
                let data = auth.user.company.inspection
                console.log(data)
                setMileage(data.mileage)
                setFuel(data.gas)
                setFrontSide(data.front_view)
                setDriverSide(data.driver_side_view)
                setPassengerSide(data.passenger_side_view)
                setrearSide(data.rear_view)
                sethandTruck(data.handtruck)
                setCarwash(data.clean)
                setPhone(data.phone)
                setCharger(data.phone_charger)
                setRescue(data.rescue)
                setGasCard(data.gas_card)
                setAbsLight(data.engine_light)
                setParkingTic(data.parking_ticket)
                setWiper(data.windshield_fluid)
                setCompanyName(auth.user.company.company_name)
                setOwnerName(auth.user.company.owner_name)
                setAddress(auth.user.company.address)
                setDspCode(auth.user.company.dsp_short_code)
                setZipCode(auth.user.company.zipcode)
                setStationCode(auth.user.company.station_code)
                setCompanyId(auth.user.company.company_id)
            }
        }
    }, [auth.user]);

    const dispatch = useDispatch();
    const [mileage, setMileage] = useState(false);
    const [fuel, setFuel] = useState(false);
    const [frontside, setFrontSide] = useState(false);
    const [driverside, setDriverSide] = useState(false);
    const [passengerside, setPassengerSide] = useState(false);
    const [rearSide, setrearSide] = useState(false);
    const [handTruck, sethandTruck] = useState(false);
    const [carwash, setCarwash] = useState(false);
    const [phone, setPhone] = useState(false);
    const [charger, setCharger] = useState(false);
    const [rescue, setRescue] = useState(false);
    const [gasCard, setGasCard] = useState(false);
    const [absLight, setAbsLight] = useState(false);
    const [parkingTic, setParkingTic] = useState(false);
    const [wiper, setWiper] = useState(false);
    const [compnyInfo, setCompnyInfo] = useState(data)
    const [companyId, setCompanyId] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [ownerName, setOwnerName] = useState('');
    const [address, setAddress] = useState('');
    const [dspCode, setDspCode] = useState('');
    const [zipCode, setZipCode] = useState('');
    const [stationCode, setStationCode] = useState('');

    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };
    }, []);

    const handleBackButtonClick = () => {
        navigation.goBack(null);
        return true;
    }

    const setInspectCar = (name: string) => {
        if (name == 'mileage') {
            setMileage(!mileage)
        } else if (name == 'fuel') {
            setFuel(!fuel)
        } else if (name == 'frontView') {
            setFrontSide(!frontside)
        } else if (name == 'driverSide') {
            setDriverSide(!driverside)
        } else if (name == 'passengerSide') {
            setPassengerSide(!passengerside)
        } else if (name == 'rearSide') {
            setrearSide(!rearSide)
        } else if (name == 'handtruck') {
            sethandTruck(!handTruck)
        } else if (name == 'washcar') {
            setCarwash(!carwash)
        } else if (name == 'phone') {
            setPhone(!phone)
        } else if (name == 'charger') {
            setCharger(!charger)
        } else if (name == 'rescue') {
            setRescue(!rescue)
        } else if (name == 'gascard') {
            setGasCard(!gasCard)
        } else if (name == 'engine') {
            setAbsLight(!absLight)
        } else if (name == 'parkingTicket') {
            setParkingTic(!parkingTic)
        } else if (name == 'wiper') {
            setWiper(!wiper)
        }

    }

    const registerCompany = () => {

        if (!auth.user.company) {
            dispatch({
                type: constants("auth").sagas.registerCompany,
                payload: {
                    data: {
                        "company_name": compnyInfo.company_name,
                        "owner_name": compnyInfo.owner_Name,
                        "dsp_short_code": compnyInfo.dspCode,
                        "address": compnyInfo.address,
                        "zipcode": compnyInfo.zipCode,
                        "station_code": compnyInfo.stationCode,
                        "inspection": {
                            "clean": carwash,
                            "driver_side_view": driverside,
                            "engine_light": absLight,
                            "front_view": frontside,
                            "gas": fuel,
                            "gas_card": gasCard,
                            "handtruck": handTruck,
                            "mileage": mileage,
                            "parking_ticket": parkingTic,
                            "passenger_side_view": passengerside,
                            "phone": phone,
                            "phone_charger": charger,
                            "rear_view": rearSide,
                            "rescue": rescue,
                            "windshield_fluid": wiper
                        }
                    },
                    sessionToken: auth.user.token,
                    accountId: auth.user.account_id,
                    navigation
                }
            })
        } else {
            dispatch({
                type: constants("auth").sagas.updtCompanyInsp,
                payload: {
                    data: {
                        "company_name": companyName,
                        "owner_name": ownerName,
                        // "dsp_short_code": dspCode,
                        "address": address,
                        "zipcode": zipCode,
                        "station_code": stationCode,
                        "inspection": {
                            "clean": carwash,
                            "driver_side_view": driverside,
                            "engine_light": absLight,
                            "front_view": frontside,
                            "gas": fuel,
                            "gas_card": gasCard,
                            "handtruck": handTruck,
                            "mileage": mileage,
                            "parking_ticket": parkingTic,
                            "passenger_side_view": passengerside,
                            "phone": phone,
                            "phone_charger": charger,
                            "rear_view": rearSide,
                            "rescue": rescue,
                            "windshield_fluid": wiper
                        }
                    },
                    company_id:companyId,
                    sessionToken: auth.user.token,
                    accountId: auth.user.account_id,
                    navigation
                }
            })
        }

    }
    let { loading } = auth;
    const isDarkMode = useColorScheme() === 'dark';
    return (
        <View style={[local_styles.mainContainer, { backgroundColor: (isDarkMode) ? "#fff" : "#fff" }]}>
            <Loader loading={loading}/>
            <View style={local_styles.topBox}>
                <Header
                    title={translate('chooseInsp')}
                    goBack={navigation.goBack} />
            </View>


            <View style={{ marginTop: 34, marginLeft: 24, marginRight: 24 }}>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 20 }}>
                    <TouchableOpacity style={[local_styles.outerBox, { backgroundColor: mileage ? "#4F8BFF" : "#fff" }]} onPress={() => setInspectCar('mileage')}>
                        <View style={local_styles.boxTopiew}>
                            <Image style={local_styles.iconimage} source={require("assets/images/meter.png")} />
                        </View>
                        <Text style={[local_styles.iconTxt, { color: !mileage ? Colors.primary : '#fff' }]}>{translate('mileage')}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={[local_styles.outerBox, { backgroundColor: fuel ? "#4F8BFF" : "#fff" }]} onPress={() => setInspectCar('fuel')}>
                        <View style={local_styles.boxTopiew}>
                            <Image style={local_styles.iconimage} source={require("assets/images/pump.png")} />
                        </View>
                        <Text style={[local_styles.iconTxt, { color: !fuel ? Colors.primary : '#fff' }]}>{translate('gas')}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={[local_styles.outerBox, { backgroundColor: frontside ? "#4F8BFF" : "#fff" }]} onPress={() => setInspectCar('frontView')}>
                        <View style={local_styles.boxTopiew}>
                            <Image style={local_styles.iconimage} source={require("assets/images/car.png")} />
                        </View>
                        <Text style={[local_styles.iconTxt, { color: !frontside ? Colors.primary : '#fff' }]}>{translate('frontViewVeh')}</Text>
                    </TouchableOpacity>
                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 20 }}>
                    <TouchableOpacity style={[local_styles.outerBox, { backgroundColor: driverside ? "#4F8BFF" : "#fff" }]} onPress={() => setInspectCar('driverSide')}>
                        <View style={local_styles.boxTopiew}>
                            <Image style={local_styles.iconimage} source={require("assets/images/driverside.png")} />
                        </View>
                        <Text style={[local_styles.iconTxt, { color: !driverside ? Colors.primary : '#fff' }]}>{translate('driverside')}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={[local_styles.outerBox, { backgroundColor: passengerside ? "#4F8BFF" : "#fff" }]} onPress={() => setInspectCar('passengerSide')}>
                        <View style={local_styles.boxTopiew}>
                            <Image style={local_styles.iconimage} source={require("assets/images/passengerside.png")} />
                        </View>
                        <Text style={[local_styles.iconTxt, { color: !passengerside ? Colors.primary : '#fff' }]}>{translate('passengerSideView')}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={[local_styles.outerBox, { backgroundColor: rearSide ? "#4F8BFF" : "#fff" }]} onPress={() => setInspectCar('rearSide')}>
                        <View style={local_styles.boxTopiew}>
                            <Image style={local_styles.iconimage} source={require("assets/images/backside.png")} />
                        </View>
                        <Text style={[local_styles.iconTxt, { color: !rearSide ? Colors.primary : '#fff' }]}>{translate('rearSideView')}</Text>
                    </TouchableOpacity>
                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 20 }}>
                    <TouchableOpacity style={[local_styles.outerBox, { backgroundColor: handTruck ? "#4F8BFF" : "#fff" }]} onPress={() => setInspectCar('handtruck')}>
                        <View style={local_styles.boxTopiew}>
                            <Image style={local_styles.iconimage} source={require("assets/images/handtruck.png")} />
                        </View>
                        <Text style={[local_styles.iconTxt, { color: !handTruck ? Colors.primary : '#fff' }]}>{translate('handtruck')}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={[local_styles.outerBox, { backgroundColor: carwash ? "#4F8BFF" : "#fff" }]} onPress={() => setInspectCar('washcar')}>
                        <View style={local_styles.boxTopiew}>
                            <Image style={local_styles.iconimage} source={require("assets/images/wash_car.png")} />
                        </View>
                        <Text style={[local_styles.iconTxt, { color: !carwash ? Colors.primary : '#fff' }]}>{translate('vehicleClean')}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={[local_styles.outerBox, { backgroundColor: phone ? "#4F8BFF" : "#fff" }]} onPress={() => setInspectCar('phone')}>
                        <View style={local_styles.boxTopiew}>
                            <Image style={local_styles.iconimage} source={require("assets/images/phone.png")} />
                        </View>
                        <Text style={[local_styles.iconTxt, { color: !phone ? Colors.primary : '#fff' }]}>{translate('phone')}</Text>
                    </TouchableOpacity>
                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 20 }}>
                    <TouchableOpacity style={[local_styles.outerBox, { backgroundColor: charger ? "#4F8BFF" : "#fff" }]} onPress={() => setInspectCar('charger')}>
                        <View style={local_styles.boxTopiew}>
                            <Image style={local_styles.iconimage} source={require("assets/images/charger.png")} />
                        </View>
                        <Text style={[local_styles.iconTxt, { color: !charger ? Colors.primary : '#fff' }]}>{translate('phoneCharger')}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={[local_styles.outerBox, { backgroundColor: rescue ? "#4F8BFF" : "#fff" }]} onPress={() => setInspectCar('rescue')}>
                        <View style={local_styles.boxTopiew}>
                            <Image style={local_styles.iconimage} source={require("assets/images/rescue.png")} />
                        </View>
                        <Text style={[local_styles.iconTxt, { color: !rescue ? Colors.primary : '#fff' }]}>{translate('rescue')}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={[local_styles.outerBox, { backgroundColor: gasCard ? "#4F8BFF" : "#fff" }]} onPress={() => setInspectCar('gascard')}>
                        <View style={local_styles.boxTopiew}>
                            <Image style={local_styles.iconimage} source={require("assets/images/gas_card.png")} />
                        </View>
                        <Text style={[local_styles.iconTxt, { color: !gasCard ? Colors.primary : '#fff' }]}>{translate('gadCard')}</Text>
                    </TouchableOpacity>
                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 20 }}>
                    <TouchableOpacity style={[local_styles.outerBox, { backgroundColor: absLight ? "#4F8BFF" : "#fff" }]} onPress={() => setInspectCar('engine')}>
                        <View style={local_styles.boxTopiew}>
                            <Image style={local_styles.iconimage} source={require("assets/images/abs_light.png")} />
                        </View>
                        <Text style={[local_styles.iconTxt, { color: !absLight ? Colors.primary : '#fff' }]}>{translate('absLight')}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={[local_styles.outerBox, { backgroundColor: parkingTic ? "#4F8BFF" : "#fff" }]} onPress={() => setInspectCar('parkingTicket')}>
                        <View style={local_styles.boxTopiew}>
                            <Image style={local_styles.iconimage} source={require("assets/images/parking_ticket.png")} />
                        </View>
                        <Text style={[local_styles.iconTxt, { color: !parkingTic ? Colors.primary : '#fff' }]}>{translate('parkingTicket')}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={[local_styles.outerBox, { backgroundColor: wiper ? "#4F8BFF" : "#fff" }]} onPress={() => setInspectCar('wiper')}>
                        <View style={local_styles.boxTopiew}>
                            <Image style={local_styles.iconimage} source={require("assets/images/wiper.png")} />
                        </View>
                        <Text style={[local_styles.iconTxt, { color: !wiper ? Colors.primary : '#fff' }]}>{translate('washFluid')}</Text>
                    </TouchableOpacity>
                </View>

                <View style={local_styles.btnView}>
                    <Button styles={local_styles.btnStyle} name={auth.user.company == null ? translate('register') : translate('save')} onPress={() => registerCompany()} />
                </View>
            </View>
        </View>
    );
};

const local_styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },
    topBox: {
        marginTop: 30,
        marginLeft: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    bckimage: {
        height: 17,
        width: 17,
        resizeMode: 'contain'
    },
    inspectionTxt: {
        // ...human.caption2Object,
        fontWeight: '500',
        marginLeft: 20,
        fontSize: 20
    },
    outerBox: {
        height: 87,
        width: 97,
        borderWidth: 1,
        borderColor: '#4F8BFF',
        alignItems: 'center',
        padding: 5
    },
    iconimage: {
        height: 40,
        width: 37,
        resizeMode: 'contain'
    },
    iconTxt: {
        fontSize: 12,
        fontWeight: '400'
    },
    boxTopiew: {
        marginTop: 5,
        height: 40,
        marginBottom: 5
    },
    btnStyle: {
        width: 138
    },
    btnView: {
        alignItems: 'center',
        marginTop: 46
    }
});

const mapStateToProps = (state: any) => ({
    auth: state.auth,
    driver: state.driver
})
export default connect(mapStateToProps)(InitialInspection);

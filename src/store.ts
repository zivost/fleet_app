import AsyncStorage from '@react-native-async-storage/async-storage';
import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { composeWithDevTools } from 'redux-devtools-extension'
import {
    persistStore,
    persistCombineReducers,
    persistReducer
  } from 'redux-persist';
import {createLogger} from 'redux-logger';
import rootReducer from './reducers'
import sagas from './sagas'

import logger from 'redux-logger'


const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    debug: true,
    whitelist: ['auth'] 
};

const persistedReducers = persistReducer(persistConfig, rootReducer);


// define middlewares
let middlewares = [];

// create and add the saga middleware
const sagaMiddleware = createSagaMiddleware();
middlewares.push(sagaMiddleware);

//add the freeze and logger dev middleware
if (process.env.NODE_ENV !== 'production') {
    middlewares.push(createLogger());
}

// apply the middleware
const middleware = applyMiddleware(...middlewares);

// let store = createStore(persistedReducers, undefined, applyMiddleware(...middlewares));
// let persistor = persistStore(store);
// sagaMiddleware.run(sagas);


export default () => {
  let store = createStore(persistedReducers, undefined, middleware);
  let persistor = persistStore(store);
  sagaMiddleware.run(sagas);
  return {store, persistor};;
};;
  
// export {
//     store, persistor
// }
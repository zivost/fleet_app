/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { Provider } from 'react-redux'
import type { FC } from 'react';
import {
  SafeAreaView,
  StatusBar,
  useColorScheme,
} from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { PersistGate } from 'redux-persist/es/integration/react'
// import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { navigationRef } from './config/navigator';
import 'react-native-gesture-handler';
import { request, check, PERMISSIONS, RESULTS } from 'react-native-permissions';

import {
  setI18nConfig,
} from "./config";

import createStore from './store';

import SplashScreen from './screens/SplashScreen/index'
import SignIn from './screens/SignIn/index'
import SignUp from './screens/SignUp/index'
import RegisterCompany from './screens/RegisterCompany/index'
import Home from './screens/Home/index'
import InitialInspection from './screens/InitialInspection/index'
import Dashboard from './screens/Dashboard/index'
import Vehiclelist from './screens/VehicleList/index'
import InspectCar from './screens/InspectCar/index'
import InspectionForm from './screens/InspectionForm/index'
import Menu from './screens/Menu/index'
import AddDamage from './screens/AddDamage/index'
import AddDriver from './screens/AddDriver/index'
import AddDispatcher from './screens/AddDispatcher/index';
import AddVehicle from './screens/AddVehicle/index'
import Profile from './screens/Profile/index'
import InspectReport from './screens/InspectReport/index'
import DamageDetails from './screens/DamageDetails/index'
import UserProfile from './screens/UserProfile/index'
import AboutUs from './screens/AboutUs/index'
import Admin from './screens/Admin/index'
import EditUserDetails from './screens/EditUserDetails/index'
import AdminList from './screens/AdminList/index'

const { store, persistor } = createStore();

const app: FC = () => {
  setI18nConfig();
  const isDarkMode = useColorScheme() === 'dark';
  const Stack = createNativeStackNavigator();
  check(PERMISSIONS.ANDROID.CAMERA)
    .then((result) => {
      switch (result) {
        case RESULTS.UNAVAILABLE:
          console.log('This feature is not available (on this device / in this context)');
          break;
        case RESULTS.DENIED:
          console.log('The permission has not been requested / is denied but requestable');
          request(PERMISSIONS.ANDROID.CAMERA).then(console.log)
          break;
        case RESULTS.LIMITED:
          console.log('The permission is limited: some actions are possible');
          break;
        case RESULTS.GRANTED:
          console.log('The permission is granted');
          break;
        case RESULTS.BLOCKED:
          console.log('The permission is denied and not requestable anymore');
          break;
      }
    })
    .catch((error) => {
      // …
    });
  return (
    <Provider store={store}>
      <PersistGate
        persistor={persistor}>
        <SafeAreaView style={{ flex: 1 }}>
          <StatusBar barStyle={isDarkMode ? 'dark-content' : 'dark-content'} />
          <NavigationContainer ref={navigationRef}>
            <Stack.Navigator
              initialRouteName="SplashScreen"
              screenOptions={{
                headerShown: false,
              }}
            >
              <Stack.Screen
                name="SplashScreen"
                component={SplashScreen}
              />
              <Stack.Screen
                name="SignIn"
                component={SignIn}
              />
              <Stack.Screen
                name="SignUp"
                component={SignUp}
              />
              <Stack.Screen
                name="RegisterCompany"
                component={RegisterCompany}
              />
              <Stack.Screen
                name="Home"
                component={Home}
              />
              <Stack.Screen
                name="InitialInspection"
                component={InitialInspection}
              />
              <Stack.Screen
                name="Dashboard"
                component={Dashboard}
              />
              <Stack.Screen
                name="Vehiclelist"
                component={Vehiclelist}
              />
              <Stack.Screen
                name="InspectCar"
                component={InspectCar}
              />
              <Stack.Screen
                name="InspectionForm"
                component={InspectionForm}
              />
              <Stack.Screen
                name="Menu"
                component={Menu}
              />
              <Stack.Screen
                name="AddDamage"
                component={AddDamage}
              />
              <Stack.Screen
                name="AddDriver"
                component={AddDriver}
              />
              <Stack.Screen
                name="AddDispatcher"
                component={AddDispatcher}
              />
              <Stack.Screen
                name="AddVehicle"
                component={AddVehicle}
              />
              <Stack.Screen
                name="Profile"
                component={Profile}
              />
              <Stack.Screen
                name="InspectReport"
                component={InspectReport}
              />
              <Stack.Screen
                name="DamageDetails"
                component={DamageDetails}
              />
              <Stack.Screen
                name="UserProfile"
                component={UserProfile}
              />
              <Stack.Screen
                name="AboutUs"
                component={AboutUs}
              />
              <Stack.Screen
                name="Admin"
                component={Admin}
              />
              <Stack.Screen
                name="EditUserDetails"
                component={EditUserDetails}
              />
              <Stack.Screen
                name="AdminList"
                component={AdminList}
              />

            </Stack.Navigator>
          </NavigationContainer>
        </SafeAreaView>
      </PersistGate>
    </Provider>

  );
};

export default app;
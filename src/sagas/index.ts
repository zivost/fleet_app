import { takeLatest } from 'redux-saga/effects'

import { signup, login, registerCompany, updtCompanyInsp, logOut, editUserDetails, getProfile, AddAdmin, getAdminList } from "./auth";
import {
    getDriverList, getDispatchersList, getDamagesList, getVehiclesList, uploadDamageImg
    , updateInspection, addDamage, createDriver, createDispatcher, createVehicleprof, getDamageSpecificCar, setInspectionCount, getInspectionCount,
    getInspectionForms, inspectionCheckout, fetchPrevInspection, getStats
} from './driver';
import constants from '../constants';
function* sagas() {
    yield takeLatest(constants("auth").sagas.login, login);
    yield takeLatest(constants("auth").sagas.signup, signup);
    yield takeLatest(constants("auth").sagas.registerCompany, registerCompany);
    yield takeLatest(constants("auth").sagas.updtCompanyInsp, updtCompanyInsp);
    yield takeLatest(constants("auth").sagas.logOut, logOut);
    yield takeLatest(constants("auth").sagas.editUserDetails, editUserDetails);
    yield takeLatest(constants("auth").sagas.getProfile, getProfile);
    yield takeLatest(constants("auth").sagas.AddAdmin, AddAdmin);
    yield takeLatest(constants("auth").sagas.getAdminList, getAdminList);

    yield takeLatest(constants("driver").sagas.getDriverList, getDriverList);
    yield takeLatest(constants("driver").sagas.getDispatchersList, getDispatchersList);
    yield takeLatest(constants("driver").sagas.getDamagesList, getDamagesList);
    yield takeLatest(constants("driver").sagas.getVehiclesList, getVehiclesList);
    yield takeLatest(constants("driver").sagas.uploadDamageImg, uploadDamageImg);
    yield takeLatest(constants("driver").sagas.updateInspection, updateInspection);
    yield takeLatest(constants("driver").sagas.setInspectionCount, setInspectionCount);
    yield takeLatest(constants("driver").sagas.getInspectionForms, getInspectionForms);
    yield takeLatest(constants("driver").sagas.inspectionCheckout, inspectionCheckout);
    yield takeLatest(constants("driver").sagas.getInspectionCount, getInspectionCount);
    yield takeLatest(constants("driver").sagas.addDamage, addDamage);
    yield takeLatest(constants("driver").sagas.createDriver, createDriver);
    yield takeLatest(constants("driver").sagas.createDispatcher, createDispatcher);
    yield takeLatest(constants("driver").sagas.createVehicleprof, createVehicleprof);
    yield takeLatest(constants("driver").sagas.getDamageSpecificCar, getDamageSpecificCar);
    yield takeLatest(constants("driver").sagas.fetchPrevInspection, fetchPrevInspection);
    yield takeLatest(constants("driver").sagas.getStats, getStats);
}

export default sagas;
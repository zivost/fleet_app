import { call, put, takeEvery, takeLatest } from 'redux-saga/effects'

import constants from "../constants";
import { Alert } from "react-native";
import driver from "../api/driver";
import { navigate, goBack } from "../config";

const delay = (ms: number) => new Promise(res => setTimeout(res, ms));

export interface ResponseGenerator {
    config?: any,
    data?: any,
    headers?: any,
    request?: any,
    status?: number,
    statusText?: string
}

export function* getDriverList(action: any) {
    yield load("getDriverList");
    const response: ResponseGenerator = yield call(driver.getDriverList, action.payload);
    if (response.status >= 200 && response.status < 210) {
        if (action.payload.vehiclelist) {
            yield put({
                type: constants("driver").reducers.getAllData.success,
                payload: response.data,
            });
            // console.log('/////////',response.data)
        } else {
            yield put({
                type: constants("driver").reducers.getDriverList.success,
                payload: response.data,
            });
        }
        yield unload("getDriverList");
    } else {
        yield error("getDriverList", response, null);
        yield unload("getDriverList");
    }
}



export function* getDispatchersList(action: any) {
    yield load("getDispatchersList");
    const response: ResponseGenerator = yield call(driver.getDispatchersList, action.payload);
    if (response.status >= 200 && response.status < 210) {
        if (action.payload.vehiclelist) {
            yield put({
                type: constants("driver").reducers.getAllData.success,
                payload: response.data,
            });
        } else {
            yield put({
                type: constants("driver").reducers.getDispatchersList.success,
                payload: response.data,
            });
        }
        yield unload("getDispatchersList");
    } else {
        yield error("getDispatchersList", response, null);
        yield unload("getDispatchersList");
    }
}

// get damage vehicle list getDispatchersList
export function* getDamagesList(action: any) {
    yield load("getDamagesList");
    const response: ResponseGenerator = yield call(driver.getDamagesList, action.payload);
    if (response.status >= 200 && response.status < 210) {
        if (action.payload.vehiclelist) {
            yield put({
                type: constants("driver").reducers.getAllData.success,
                payload: response.data,
            });
        } else {
            yield put({
                type: constants("driver").reducers.getDamagesList.success,
                payload: response.data,
            });
        }
        yield unload("getDamagesList");
    } else {
        yield error("getDamagesList", response, null);
        yield unload("getDamagesList");
    }
}

// get vehicle list 
export function* getVehiclesList(action: any) {
    yield load("getVehiclesList");
    const response: ResponseGenerator = yield call(driver.getVehiclesList, action.payload);
    if (response.status >= 200 && response.status < 210) {
        if (action.payload.vehiclelist) {
            yield put({
                type: constants("driver").reducers.getAllData.success,
                payload: response.data,
            });
        } else {
            yield put({
                type: constants("driver").reducers.getVehiclesList.success,
                payload: response.data,
            });
        }
        yield unload("getVehiclesList");

    } else {
        yield error("getVehiclesList", response, null);
        yield unload("getVehiclesList");
    }
}

// upload damages images 
export function* uploadDamageImg(action: any) {
    yield load("uploadDamageImg");
    const response: ResponseGenerator = yield call(driver.uploadDamageImg, action.payload);
    if (response.status >= 200 && response.status < 210) {
        yield put({
            type: constants("driver").reducers.uploadDamageImg.success,
            payload: response.data,
        });
        yield unload("uploadDamageImg");
        if (action.payload.type == 'damageImg') {
            action.setImg(response.data, action.payload.type)
        } else if (action.payload.type == 'frontView') {
            action.setImg(response.data, action.payload.type)
        } else if (action.payload.type == 'driverView') {
            action.setImg(response.data, action.payload.type)
        } else if (action.payload.type == 'passengerView') {
            action.setImg(response.data, action.payload.type)
        } else if (action.payload.type == 'rearView') {
            action.setImg(response.data, action.payload.type)
        } else if (action.payload.type == 'handtruckView') {
            action.setImg(response.data, action.payload.type)
        } else if (action.payload.type == 'parkingTicket') {
            action.setImg(response.data, action.payload.type)
        } else if (action.payload.type == 'driverPic') {
            action.setImg(response.data, action.payload.type)
        } else if (action.payload.type == 'dispatcherPic') {
            action.setImg(response.data, action.payload.type)
        } else if (action.payload.type == 'vehiclePic') {
            action.setImg(response.data, action.payload.type)
        } else if (action.payload.type == 'damagePic') {
            action.setImg(response.data, action.payload.type)
        }


    } else {
        yield error("uploadDamageImg", response, null);
        yield unload("uploadDamageImg");
    }
}


// set inspection count by date 
export function* setInspectionCount(action: any) {
    yield load("setInspectionCount");
    const response: ResponseGenerator = yield call(driver.setInspectionCount, action.payload);
    if (response.status >= 200 && response.status < 210) {
        yield put({
            type: constants("driver").reducers.setInspectionCount.success,
            payload: response.data,
        });
        yield unload("setInspectionCount");

    } else {
        yield error("setInspectionCount", response, null);
        yield unload("setInspectionCount");
    }
}

// get inspection count by date 
export function* getInspectionCount(action: any) {
    yield load("getInspectionCount");
    const response: ResponseGenerator = yield call(driver.getInspectionCount, action.payload);
    if (response.status >= 200 && response.status < 210) {
        yield put({
            type: constants("driver").reducers.getInspectionCount.success,
            payload: response.data,
        });
        yield unload("getInspectionCount");

    } else {
        yield error("getInspectionCount", response, null);
        yield unload("getInspectionCount");
    }
}

// get inspection forms by date 
export function* getInspectionForms(action: any) {
    yield load("getInspectionForms");
    const response: ResponseGenerator = yield call(driver.getInspectionForms, action.payload);
    if (response.status >= 200 && response.status < 210) {
        yield put({
            type: constants("driver").reducers.getInspectionForms.success,
            payload: response.data,
        });
        yield unload("getInspectionForms");

    } else {
        yield error("getInspectionForms", response, null);
        yield unload("getInspectionForms");
    }
}

// checkout inspection forms by date 
export function* inspectionCheckout(action: any) {
    yield load("inspectionCheckout");
    const response: ResponseGenerator = yield call(driver.inspectionCheckout, action.payload);
    if (response.status >= 200 && response.status < 210) {
        yield put({
            type: constants("driver").reducers.inspectionCheckout.success,
            payload: response.data,
        });
        yield unload("inspectionCheckout");
        goBack();
    } else {
        yield error("inspectionCheckout", response, null);
        yield unload("inspectionCheckout");
    }
}


// update inspection by date 
export function* updateInspection(action: any) {
    yield load("updateInspection");
    const response: ResponseGenerator = yield call(driver.updateInspection, action.payload);
    if (response.status >= 200 && response.status < 210) {
        yield put({
            type: constants("driver").reducers.updateInspection.success,
            payload: response.data,
        });
        yield unload("updateInspection");
        if (action.payload.data.checkout) {
            goBack();
        }
        if (action.setDriver) {
            let vehicle_id = action.payload.data.vehicle
            let driver_id = action.payload.data.driver
            action.cllInspection(driver_id,vehicle_id)
        }


    } else {
        yield error("updateInspection", response, null);
        yield unload("updateInspection");
    }
}

// add damage 
export function* addDamage(action: any) {
    yield load("addDamage");
    const response: ResponseGenerator = yield call(driver.addDamage, action.payload);
    if (response.status >= 200 && response.status < 210) {
        yield put({
            type: constants("driver").reducers.addDamage.success,
            payload: response.data,
        });
        yield unload("addDamage");
        //refreshDriver(action.payload.sessionToken);
        if (!action.payload.data.inspection) {
            Alert.alert(
                '',
                "Congratulations, Damage vehicle has been successfully created.",
                [
                    {
                        text: 'Ok', onPress: () =>
                        (
                            goBack()
                        )
                    },
                ],
                { cancelable: false },
            );
        }
        // After Login Navigation....
        // const { navigation } = action.payload;
        // navigation.navigate("Dashboard");

    } else {
        // if(response.status === 401){
        //     navigation.navigate('SignIn');
        // }
        Alert.alert(
            'Error!',
            (response.data.message) ? response.data.message : 'We ran into some issues and are looking into it.',
            [
                {
                    text: 'Ok', onPress: () =>
                    (
                        console.log
                    )
                },
            ],
            { cancelable: false },
        );
        yield error("addDamage", response, null);
        yield unload("addDamage");
    }
}

// create new driver 
export function* createDriver(action: any) {
    yield load("createDriver");
    const response: ResponseGenerator = yield call(driver.createDriver, action.payload);
    if (response.status >= 200 && response.status < 210) {
        yield put({
            type: constants("driver").reducers.createDriver.success,
            payload: response.data,
        });
        yield unload("createDriver");
        // refreshDriver(action.payload.sessionToken);
        const { navigation } = action.payload;
        if (action.payload.id) {
            let data = response.data.data
            navigation.navigate('Profile', { data, screen: 'Drivers' })
        } else {
            Alert.alert(
                '',
                "Congratulations, Driver has been successfully created.",
                [
                    {
                        text: 'Ok', onPress: () =>
                        (
                            goBack()
                        )
                    },
                ],
                { cancelable: false },
            );
        }
        // After Login Navigation....
        // const { navigation } = action.payload;
        // navigation.navigate("Dashboard");

    } else {
        // if(response.status === 401){
        //     navigation.navigate('SignIn');
        // }
        Alert.alert(
            'Error!',
            (response.data.message) ? response.data.message : 'We ran into some issues and are looking into it.',
            [
                {
                    text: 'Ok', onPress: () =>
                    (
                        console.log
                    )
                },
            ],
            { cancelable: false },
        );
        yield error("createDriver", response, null);
        yield unload("createDriver");
    }
}

// create new dispatcher 
export function* createDispatcher(action: any) {
    yield load("createDispatcher");
    const response: ResponseGenerator = yield call(driver.createDispatcher, action.payload);
    if (response.status >= 200 && response.status < 210) {
        yield put({
            type: constants("driver").reducers.createDispatcher.success,
            payload: response.data,
        });
        yield unload("createDispatcher");
        // refreshDriver(action.payload.sessionToken);
        const { navigation } = action.payload;
        if (action.payload.id) {
            let data = response.data.data
            navigation.navigate('Profile', { data, screen: 'Dispatchers' })
        } else {
            Alert.alert(
                '',
                "Congratulations, Dispatcher has been successfully created.",
                [
                    {
                        text: 'Ok', onPress: () =>
                        (
                            goBack()
                        )
                    },
                ],
                { cancelable: false },
            );
        }

        // After Login Navigation....
        // const { navigation } = action.payload;
        // navigation.navigate("Dashboard");

    } else {
        // if(response.status === 401){
        //     navigation.navigate('SignIn');
        // }
        Alert.alert(
            'Error!',
            (response.data.message) ? response.data.message : 'We ran into some issues and are looking into it.',
            [
                {
                    text: 'Ok', onPress: () =>
                    (
                        console.log
                    )
                },
            ],
            { cancelable: false },
        );
        yield error("createDispatcher", response, null);
        yield unload("createDispatcher");
    }
}

// create vehicle 
export function* createVehicleprof(action: any) {
    yield load("createVehicleprof");
    const response: ResponseGenerator = yield call(driver.createVehicleprof, action.payload);
    if (response.status >= 200 && response.status < 210) {
        yield put({
            type: constants("driver").reducers.createVehicleprof.success,
            payload: response.data,
        });
        yield unload("createVehicleprof");
        //refreshDriver(action.payload.sessionToken);
        // goBack();
        // After Login Navigation....
        const { navigation } = action.payload;
        if (action.payload.id) {
            let data = response.data.data
            navigation.navigate('Profile', { data, screen: 'Vehicle' })
        } else {
            Alert.alert(
                '',
                "Congratulations, vehicle has been successfully created.",
                [
                    {
                        text: 'Ok', onPress: () =>
                        (
                            goBack()
                        )
                    },
                ],
                { cancelable: false },
            );
        }
        // navigation.navigate("Dashboard");

    } else {
        // if(response.status === 401){
        //     navigation.navigate('SignIn');
        // }
        Alert.alert(
            'Error!',
            (response.data.message) ? response.data.message : 'We ran into some issues and are looking into it.',
            [
                {
                    text: 'Ok', onPress: () =>
                    (
                        console.log
                    )
                },
            ],
            { cancelable: false },
        );
        yield error("createVehicleprof", response, null);
        yield unload("createVehicleprof");
    }
}

// get damage list specific car 
export function* getDamageSpecificCar(action: any) {
    yield load("getDamageSpecificCar");
    const response: ResponseGenerator = yield call(driver.getDamageSpecificCar, action.payload);
    if (response.status >= 200 && response.status < 210) {
        yield put({
            type: constants("driver").reducers.getDamageSpecificCar.success,
            payload: response.data,
        });
        yield unload("getDamageSpecificCar");
    } else {
        Alert.alert(
            'Error!',
            (response.data.message) ? response.data.message : 'We ran into some issues and are looking into it.',
            [
                {
                    text: 'Ok', onPress: () =>
                    (
                        console.log
                    )
                },
            ],
            { cancelable: false },
        );
        yield error("getDamageSpecificCar", response, null);
        yield unload("getDamageSpecificCar");
    }
}

// fetch inspection vehicle 
export function* fetchPrevInspection(action: any) {
    yield load("fetchPrevInspection");
    const response: ResponseGenerator = yield call(driver.fetchPrevInspection, action.payload);
    if (response.status >= 200 && response.status < 210) {
        yield put({
            type: constants("driver").reducers.fetchPrevInspection.success,
            payload: response.data,
        });
        yield unload("fetchPrevInspection");
    } else {
        Alert.alert(
            'Error!',
            (response.data.message) ? response.data.message : 'We ran into some issues and are looking into it.',
            [
                {
                    text: 'Ok', onPress: () =>
                    (
                        console.log
                    )
                },
            ],
            { cancelable: false },
        );
        yield error("fetchPrevInspection", response, null);
        yield unload("fetchPrevInspection");
    }
}

// get stats 
export function* getStats(action: any) {
    yield load("getStats");
    const response: ResponseGenerator = yield call(driver.getStats, action.payload);
    console.log('////////reponse', response)
    if (response.status >= 200 && response.status < 210) {
        yield put({
            type: constants("driver").reducers.getStats.success,
            payload: response.data,
        });
        yield unload("getStats");
    } else {
        Alert.alert(
            'Error!',
            (response.data.message) ? response.data.message : 'We ran into some issues and are looking into it.',
            [
                {
                    text: 'Ok', onPress: () =>
                    (
                        console.log
                    )
                },
            ],
            { cancelable: false },
        );
        yield error("getStats", response, null);
        yield unload("getStats");
    }
}
function* error(type: string, response: any, message: any, popup: boolean = false) {

    let status = 0;
    if (response) {
        status = response.status || 0
    }
    if (popup && status != 401) {
        Alert.alert(
            'Error!',
            (message) ? message : 'We ran into some issues and are looking into it.',
            [
                {
                    text: 'Ok', onPress: () =>
                    (
                        console.log
                    )
                },
            ],
            { cancelable: false },
        );
    }
    yield put({
        type: constants("driver").reducers[type].error,
        payload: {
            status: status,
            message: message || "We ran into some issues and are looking into it."
        },
    });
    if (status === 401) {
        navigate('SignIn', {});
    }
}

// function *refreshDriver(token: string){
//     let params = '&limit=4&offset=0';
//         yield put({
//             type: constants("driver").sagas.getDriverList,
//             payload: {
//                 data:params,
//                 sessionToken: token,
//             }
//         })
//         yield put({
//             type: constants("driver").sagas.getDispatchersList,
//             payload: {
//                 data:params,
//                 sessionToken: token,
//             }
//         })
//         yield put({
//             type: constants("driver").sagas.getDamagesList,
//             payload: {
//                 data:params,
//                 sessionToken: token,
//             }
//         })
//         yield put({
//             type: constants("driver").sagas.getVehiclesList,
//             payload: {
//                 data:params,
//                 sessionToken: token,
//             }
//         })
// }

function* load(type: any) {
    yield put({
        type: constants("driver").reducers[type].load
    });
}

function* unload(type: any) {
    yield put({
        type: constants("driver").reducers[type].unload
    });
}

import { call, put, takeEvery, takeLatest } from 'redux-saga/effects'

import constants from "../constants";
import { Alert } from "react-native"
import Auth from "../api/auth"
import { navigate } from "../config";

const delay = (ms: number) => new Promise(res => setTimeout(res, ms));

export interface ResponseGenerator {
    config?: any,
    data?: any,
    headers?: any,
    request?: any,
    status?: number,
    statusText?: string
}

export function* login(action: any) {
    yield load("login");
    const response: ResponseGenerator = yield call(Auth.login, action.payload);
    if (response.status === 200) {
        yield put({
            type: constants("auth").reducers.login.success,
            payload: response.data,
        });
        yield unload("login");
        action.payload.navToScreen(response.data)
        // After Login Navigation....
        yield delay(200);
    } else {
        Alert.alert(
            'Error!',
            (response.data.message) ? response.data.message : 'We ran into some issues and are looking into it.',
            [
                {
                    text: 'Ok', onPress: () =>
                    (
                        console.log
                    )
                },
            ],
            { cancelable: false },
        );
        yield error("login", response, null);
        yield unload("login");
    }
}

export function* signup(action: any) {
    yield load("signup");
    const response: ResponseGenerator = yield call(Auth.signup, action.payload);
    if (response.status >= 200 && response.status < 210) {
        yield put({
            type: constants("auth").reducers.signup.success,
            payload: response.data,
        });
        yield unload("signup");
        // After Login Navigation....
        // const { navigation } = action.payload;
        // navigation.navigate('RegisterCompany')
        Alert.alert(
            '',
            "Congratulations, Your account has been successfully created.",
            [
                {
                    text: 'Ok', onPress: () =>
                    (
                        navigate("SignIn")
                    )
                },
            ],
            { cancelable: false },
        );
    } else {
        Alert.alert(
            'Error!',
            (response.data.message) ? response.data.message : 'We ran into some issues and are looking into it.',
            [
                {
                    text: 'Ok', onPress: () =>
                    (
                        console.log
                    )
                },
            ],
            { cancelable: false },
        );
        yield error("signup", response, null);
        yield unload("signup");
    }
}

export function* registerCompany(action: any) {
    yield load("registerCompany");
    const response: ResponseGenerator = yield call(Auth.registerCompany, action.payload);
    console.log("res", response);
    if (response.status >= 200 && response.status < 210) {
        yield put({
            type: constants("auth").reducers.registerCompany.success,
            payload: response.data,
        });
        yield unload("registerCompany");
        // After Login Navigation....
        // const { navigation } = action.payload;
        Alert.alert(
            '',
            "Congratulations, Inspection created successfully.",
            [
                {
                    text: 'Ok', onPress: () =>
                    (
                        navigate("Dashboard")
                    )
                },
            ],
            { cancelable: false },
        );

    } else {
        Alert.alert(
            'Error!',
            (response.data.message) ? response.data.message : 'We ran into some issues and are looking into it.',
            [
                {
                    text: 'Ok', onPress: () =>
                    (
                        console.log
                    )
                },
            ],
            { cancelable: false },
        );
        yield error("registerCompany", response, null);
        yield unload("registerCompany");
    }
}

// update company inspection 
export function* updtCompanyInsp(action: any) {
    yield load("updtCompanyInsp");
    const response: ResponseGenerator = yield call(Auth.updtCompanyInsp, action.payload);
    console.log("res", response);
    if (response.status >= 200 && response.status < 210) {
        yield put({
            type: constants("auth").reducers.updtCompanyInsp.success,
            payload: response.data,
        });
        yield unload("updtCompanyInsp");
        // After Login Navigation....
        // const { navigation } = action.payload;
        Alert.alert(
            '',
            "Congratulations, Company edited successfully.",
            [
                {
                    text: 'Ok', onPress: () =>
                    (
                        navigate("Dashboard")
                    )
                },
            ],
            { cancelable: false },
        );
    } else {
        Alert.alert(
            'Error!',
            (response.data.message) ? response.data.message : 'We ran into some issues and are looking into it.',
            [
                {
                    text: 'Ok', onPress: () =>
                    (
                        console.log
                    )
                },
            ],
            { cancelable: false },
        );
        yield error("updtCompanyInsp", response, null);
        yield unload("updtCompanyInsp");
    }
}

// logout 
export function* logOut(action: any) {
    yield put({
        type: constants("auth").reducers.logOut.success
    });
    navigate("SignIn");
    // yield load("logOut");
    // // const response: ResponseGenerator = yield call(Auth.logOut, action.payload);
    // // console.log("res", response);
    // if (response.status >= 200 && response.status < 210) {
    //     yield put({
    //         type: constants("auth").reducers.logOut.success,
    //         payload: response.data,
    //     });
    //     yield unload("logOut");
    //     // After Login Navigation....
    //     // const { navigation } = action.payload;
    //     navigate("SignIn");

    // } else {
    //     Alert.alert(
    //         'Error!',
    //         (response.data.message) ? response.data.message : 'We ran into some issues and are looking into it.',
    //         [
    //             {
    //                 text: 'Ok', onPress: () =>
    //                 (
    //                     console.log
    //                 )
    //             },
    //         ],
    //         { cancelable: false },
    //     );
    //     yield error("logOut", response, null);
    //     yield unload("logOut");
    // }
}

// edit profile details 
export function* editUserDetails(action: any) {
    yield load("editUserDetails");
    const response: ResponseGenerator = yield call(Auth.editUserDetails, action.payload);
    // console.log("res", response);
    if (response.status >= 200 && response.status < 210) {
        yield put({
            type: constants("auth").reducers.editUserDetails.success,
            payload: response.data,
        });
        yield unload("editUserDetails");
        action.getProfileDetails()
        const { navigation } = action.payload;

        Alert.alert(
            '',
            "Congratulations, Profile Updated successfully.",
            [
                {
                    text: 'Ok', onPress: () =>
                    (
                        navigation.navigate("Dashboard")
                    )
                },
            ],
            { cancelable: false },
        );
        // After Login Navigation....
        // const { navigation } = action.payload;
        // navigation.navigate("SignIn");

    } else {
        Alert.alert(
            'Error!',
            (response.data.message) ? response.data.message : 'We ran into some issues and are looking into it.',
            [
                {
                    text: 'Ok', onPress: () =>
                    (
                        console.log
                    )
                },
            ],
            { cancelable: false },
        );
        yield error("editUserDetails", response, null);
        yield unload("editUserDetails");
    }
}

// get Prfile details 
export function* getProfile(action: any) {
    yield load("getProfile");
    const response: ResponseGenerator = yield call(Auth.getProfile, action.payload);
    if (response.status >= 200 && response.status < 210) {
        yield put({
            type: constants("auth").reducers.getProfile.success,
            payload: response.data,
        });
        yield unload("getProfile");

    } else {
        Alert.alert(
            'Error!',
            (response.data.message) ? response.data.message : 'We ran into some issues and are looking into it.',
            [
                {
                    text: 'Ok', onPress: () =>
                    (
                        console.log
                    )
                },
            ],
            { cancelable: false },
        );
        yield error("getProfile", response, null);
        yield unload("getProfile");
    }
}

// Add new Admin 
export function* AddAdmin(action: any) {
    yield load("AddAdmin");
    const response: ResponseGenerator = yield call(Auth.AddAdmin, action.payload);
    if (response.status >= 200 && response.status < 210) {
        yield put({
            type: constants("auth").reducers.AddAdmin.success,
            payload: response.data,
        });
        yield unload("AddAdmin");
        const { navigation } = action.payload;
        Alert.alert(
            '',
            "Congratulations, Admin has been successfully created.",
            [
                {
                    text: 'Ok', onPress: () =>
                    (
                        console.log()
                    )
                },
            ],
            { cancelable: false },
        );
    } else {
        Alert.alert(
            'Error!',
            (response.data.message) ? response.data.message : 'We ran into some issues and are looking into it.',
            [
                {
                    text: 'Ok', onPress: () =>
                    (
                        console.log
                    )
                },
            ],
            { cancelable: false },
        );
        yield error("AddAdmin", response, null);
        yield unload("AddAdmin");
    }
}

// get admin list 
export function* getAdminList(action: any) {
    yield load("getAdminList");
    const response: ResponseGenerator = yield call(Auth.getAdminList, action.payload);
    console.log('response', response)
    if (response.status >= 200 && response.status < 210) {
        yield put({
            type: constants("auth").reducers.getAdminList.success,
            payload: response.data,
        });
        yield unload("getAdminList");
    } else {
        Alert.alert(
            'Error!',
            (response.data.message) ? response.data.message : 'We ran into some issues and are looking into it.',
            [
                {
                    text: 'Ok', onPress: () =>
                    (
                        console.log
                    )
                },
            ],
            { cancelable: false },
        );
        yield error("getAdminList", response, null);
        yield unload("getAdminList");
    }
}
function* error(type: string, response: any, message: any) {
    let status = 0;
    if (response) {
        status = response.status || 0
    }
    yield put({
        type: constants("auth").reducers[type].error,
        payload: {
            status: status,
            message: message || "We ran into some issues and are looking into it."
        },
    });
}

function* load(type: any) {
    yield put({
        type: constants("auth").reducers[type].load
    });
}

function* unload(type: any) {
    yield put({
        type: constants("auth").reducers[type].unload
    });
}

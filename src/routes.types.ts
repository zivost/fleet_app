export type StackParamList = {
    SignIn: undefined;
    SplashScreen: undefined;
    SignUp: undefined;
    RegisterCompany: undefined;
    Dashboard: undefined;
    InspectCar: object;
    InspectionForm: object;
};
  
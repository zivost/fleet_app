import { combineReducers } from 'redux'

import auth from "./auth";
import driver from './driver'

// export default {
//     auth,
// }
  

const rootReducer = combineReducers({
    auth:auth,
    driver:driver
})

export default rootReducer

export type RootState = ReturnType<typeof rootReducer>

import constants from "../constants"
import sortBy from 'lodash/sortBy';

const initial_state = {
    success: false,
    loading: false,
    last_update: Date.now(),
    user: {},
    driverList: {},
    dispatcherList: {},
    damagesList: {},
    vehicleList: {},
    uploadPic: {},
    getAllData: [],
    dmageCarDetails: {},
    driverListMetaData: {},
    vehicleListmetaData: {},
    damageListmetaData: {},
    dispatcherListmetaData: {},
    inspection: {
        count: 0,
        id: null
    },
    inspection_forms: [],
    prevInspection_form: [],
    statsData: {}
}

export default function driver(state = initial_state, action: any): any {
    switch (action.type) {
        case constants('driver').reducers.getDriverList.load:
            return {
                ...state,
                loading: true
            }
        case constants('driver').reducers.getDriverList.success:
            return {
                ...state,
                last_update: state.last_update,
                loading: false,
                success: true,
                driverList: action.payload.data,
                driverListMetaData: action.payload.metadata,
            }
        case constants('driver').reducers.getDriverList.error:
            return {
                ...state,
                loading: false,
                success: false,
            }
        case constants('driver').reducers.getDriverList.unload:
            return {
                ...state,
                loading: false
            }
        case constants('driver').reducers.getAllData.success:
            console.log("get-getAllData", action)
            let oldCatData1 = [...state.getAllData];
            if(action.payload?.metadata?.offset === 0){
                oldCatData1 = []
            }
            let newCatData1 = []
            if (action.payload.data) {
                if (action.payload.data.length > 0) {
                    if (oldCatData1.length > 0) {
                        newCatData1 = [...oldCatData1, ...action.payload.data]
                    } else {
                        newCatData1 = action.payload.data
                    }
                } else {
                    newCatData1 = [...state.getAllData]
                }
            }
            return {
                ...state,
                loading: false,
                success: true,
                getAllData: newCatData1
            }
        case constants('driver').reducers.getAllData.set:
            return {
                ...state,
                getAllData: []
            }
        case constants('driver').reducers.getDispatchersList.load:
            return {
                ...state,
                loading: true
            }
        case constants('driver').reducers.getDispatchersList.success:
            return {
                ...state,
                loading: false,
                success: true,
                dispatcherList: action.payload.data
            }
        case constants('driver').reducers.getDispatchersList.error:
            return {
                ...state,
                loading: false,
                success: false,
            }
        case constants('driver').reducers.getDispatchersList.unload:
            return {
                ...state,
                loading: false
            }
        case constants('driver').reducers.getDamagesList.load:
            return {
                ...state,
                loading: true
            }
        case constants('driver').reducers.getDamagesList.success:
            return {
                ...state,
                loading: false,
                success: true,
                damagesList: action.payload.data,
                damageListmetaData: action.payload.metadata,
                last_update: state.last_update,
            }
        case constants('driver').reducers.getDamagesList.error:
            return {
                ...state,
                loading: false,
                success: false,
            }
        case constants('driver').reducers.getDamagesList.unload:
            return {
                ...state,
                loading: false
            }
        case constants('driver').reducers.getVehiclesList.load:
            return {
                ...state,
                loading: true
            }
        case constants('driver').reducers.getVehiclesList.success:
            // sort vehicles by flag
            let sort_index = [
                'green',
                'yellow',
                'red',
                'black'
            ]
            let vlist = sortBy(action.payload.data, (o) => { return sort_index.indexOf(o.flag) })
            return {
                ...state,
                loading: false,
                success: true,
                last_update: state.last_update,
                vehicleList: vlist,
                vehicleListmetaData: action.payload.metadata
            }
        case constants('driver').reducers.getVehiclesList.error:
            return {
                ...state,
                loading: false,
                success: false,
            }
        case constants('driver').reducers.getVehiclesList.unload:
            return {
                ...state,
                loading: false
            }
        case constants('driver').reducers.uploadDamageImg.load:
            return {
                ...state,
                loading: true
            }
        case constants('driver').reducers.uploadDamageImg.success:
            return {
                ...state,
                loading: false,
                success: true
            }
        case constants('driver').reducers.uploadDamageImg.error:
            return {
                ...state,
                loading: false,
                success: false,
            }
        case constants('driver').reducers.uploadDamageImg.unload:
            return {
                ...state,
                loading: false
            }
        case constants('driver').reducers.updateInspection.load:
            return {
                ...state,
                loading: true
            }
        case constants('driver').reducers.updateInspection.success:
            return {
                ...state,
                loading: false,
                success: true,
                last_update: Date.now()
            }
        case constants('driver').reducers.getInspectionForms.success:
            return {
                ...state,
                loading: false,
                success: true,
                last_update: Date.now(),
                inspection_forms: action.payload.data
            }
        case constants('driver').reducers.inspectionCheckout.success:
            return {
                ...state,
                loading: false,
                success: true,
                last_update: Date.now(),
                inspection: action.payload.data
            }
        case constants('driver').reducers.getInspectionCount.success:
            if (action.payload.data.length > 0) {
                return {
                    ...state,
                    loading: false,
                    success: true,
                    last_update: Date.now(),
                    inspection: {
                        ...action.payload.data[0],
                        count: action.payload.data[0].count,
                        id: action.payload.data[0].uuid,
                        eod_checkout: action.payload.data[0].eod_checkout || null
                    }

                }
            } else {
                return {
                    ...state,
                    loading: false,
                    success: true,
                    last_update: Date.now(),
                    inspection: {}
                }
            }

        case constants('driver').reducers.updateInspection.error:
            return {
                ...state,
                loading: false,
                success: false,
                last_update: Date.now()
            }
        case constants('driver').reducers.updateInspection.unload:
            return {
                ...state,
                loading: false
            }
        case constants('driver').reducers.addDamage.load:
            return {
                ...state,
                loading: true
            }
        case constants('driver').reducers.addDamage.success:
            return {
                ...state,
                loading: false,
                success: true,
                last_update: Date.now()
            }
        case constants('driver').reducers.addDamage.error:
            return {
                ...state,
                loading: false,
                success: false,
                last_update: Date.now()
            }
        case constants('driver').reducers.addDamage.unload:
            return {
                ...state,
                loading: false
            }
        case constants('driver').reducers.createDriver.load:
            return {
                ...state,
                loading: true
            }
        case constants('driver').reducers.createDriver.success:
            return {
                ...state,
                loading: false,
                success: true,
                last_update: Date.now()
            }
        case constants('driver').reducers.createDriver.error:
            return {
                ...state,
                loading: false,
                success: false,
                last_update: Date.now()
            }
        case constants('driver').reducers.createDriver.unload:
            return {
                ...state,
                loading: false
            }
        case constants('driver').reducers.createDispatcher.load:
            return {
                ...state,
                loading: true
            }
        case constants('driver').reducers.createDispatcher.success:
            return {
                ...state,
                loading: false,
                success: true,
                last_update: Date.now()
            }
        case constants('driver').reducers.createDispatcher.error:
            return {
                ...state,
                loading: false,
                success: false,
                last_update: Date.now()
            }
        case constants('driver').reducers.createDispatcher.unload:
            return {
                ...state,
                loading: false
            }
        case constants('driver').reducers.createVehicleprof.load:
            return {
                ...state,
                loading: true
            }
        case constants('driver').reducers.createVehicleprof.success:
            return {
                ...state,
                loading: false,
                success: true,
                last_update: Date.now()
            }
        case constants('driver').reducers.createVehicleprof.error:
            return {
                ...state,
                loading: false,
                success: false,
                last_update: Date.now()
            }
        case constants('driver').reducers.createVehicleprof.unload:
            return {
                ...state,
                loading: false
            }
        case constants('driver').reducers.getDamageSpecificCar.load:
            return {
                ...state,
                loading: true
            }
        case constants('driver').reducers.getDamageSpecificCar.success:
            return {
                ...state,
                loading: false,
                success: true,
                dmageCarDetails: action.payload.data
            }
        case constants('driver').reducers.getDamageSpecificCar.error:
            return {
                ...state,
                loading: false,
                success: false,
            }
        case constants('driver').reducers.getDamageSpecificCar.unload:
            return {
                ...state,
                loading: false
            }
        case constants('driver').reducers.fetchPrevInspection.load:
            return {
                ...state,
                loading: true
            }
        case constants('driver').reducers.fetchPrevInspection.success:
            return {
                ...state,
                loading: false,
                success: true,
                prevInspection_form: action.payload.data
            }
        case constants('driver').reducers.fetchPrevInspection.error:
            return {
                ...state,
                loading: false,
                success: false,
            }
        case constants('driver').reducers.fetchPrevInspection.unload:
            return {
                ...state,
                loading: false
            }
        case constants('driver').reducers.getStats.load:
            return {
                ...state,
                loading: true
            }
        case constants('driver').reducers.getStats.success:
            return {
                ...state,
                loading: false,
                success: true,
                statsData: action.payload.data
            }
        case constants('driver').reducers.getStats.error:
            return {
                ...state,
                loading: false,
                success: false,
            }
        case constants('driver').reducers.getStats.unload:
            return {
                ...state,
                loading: false
            }
        case constants('driver').reducers.logOut.set:
            return {
                ...state,
                success: false,
                loading: false,
                last_update: '',
                user: {},
                driverList: {},
                dispatcherList: {},
                damagesList: {},
                vehicleList: {},
                uploadPic: {},
                getAllData: [],
                dmageCarDetails: {},
                driverListMetaData: {},
                vehicleListmetaData: {},
                damageListmetaData: {},
                dispatcherListmetaData: {},
                inspection: {
                    count: 0,
                    id: null
                },
                inspection_forms: []
            }
        case constants('driver').reducers.setDashboardData.set:
            return {
                ...state,
                driverList: {},
                dispatcherList: {},
                damagesList: {},
                vehicleList: {},
                driverListMetaData: {},
                vehicleListmetaData: {},
                damageListmetaData: {},
                dispatcherListmetaData: {},
            }
        default:
            return state
    }
}
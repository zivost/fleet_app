import constants from "../constants"

const initial_state = {
    logged_in: false,
    loading: false,
    user: {},
    signup: {},
    getAdminList: []
}

export default function auth(state = initial_state, action: any): any {
    switch (action.type) {
        case constants('auth').reducers.login.load:
            return {
                ...state,
                loading: true
            }
        case constants('auth').reducers.login.success:
            return {
                ...state,
                loading: false,
                logged_in: true,
                user: action.payload.data
            }
        case constants('auth').reducers.login.error:
            return {
                ...state,
                loading: false
            }
        case constants('auth').reducers.login.set:
            return {
                ...state,
                // logged_in: false,
                // loading: false,
                // user: {},
                // signup: {}
            }
        case constants('auth').reducers.signup.load:
            return {
                ...state,
                loading: true
            }
        case constants('auth').reducers.signup.success:
            return {
                ...state,
                loading: false,
                signup: action.payload.data
            }
        case constants('auth').reducers.signup.error:
            return {
                ...state,
                loading: false
            }
        case constants('auth').reducers.signup.unload:
            return {
                ...state,
                loading: false
            }
        case constants('auth').reducers.registerCompany.load:
            return {
                ...state,
                loading: true
            }
        case constants('auth').reducers.registerCompany.success:
            return {
                ...state,
                loading: false,
                user: {
                    ...state.user,
                    company: action.payload.data
                }
            }
        case constants('auth').reducers.registerCompany.error:
            return {
                ...state,
                loading: false
            }
        case constants('auth').reducers.registerCompany.unload:
            return {
                ...state,
                loading: false
            }
        case constants('auth').reducers.updtCompanyInsp.load:
            return {
                ...state,
                loading: true
            }
        case constants('auth').reducers.updtCompanyInsp.success:
            return {
                ...state,
                loading: false,
                user: {
                    ...state.user,
                    company: {
                        ...state.user.company,
                        company_name: action.payload.data.company_name,
                        owner_name: action.payload.data.owner_name,
                        address: action.payload.data.address,
                        dsp_short_code: action.payload.data.dsp_short_code,
                        station_code: action.payload.data.station_code,
                        zipcode: action.payload.data.zipcode,
                        inspection: action.payload.data.inspection,
                    }
                }
            }
        case constants('auth').reducers.updtCompanyInsp.error:
            return {
                ...state,
                loading: false
            }
        case constants('auth').reducers.updtCompanyInsp.unload:
            return {
                ...state,
                loading: false
            }
        case constants('auth').reducers.logOut.load:
            return {
                ...state,
                loading: true
            }
        case constants('auth').reducers.logOut.success:
            return {
                ...state,
                logged_in: false,
                loading: false,
                user: {},
                signup: {}
            }
        case constants('auth').reducers.logOut.error:
            return {
                ...state,
                logged_in: false,
                loading: false,
                user: {},
                signup: {}
            }
        case constants('auth').reducers.logOut.unload:
            return {
                ...state,
                loading: false
            }
        case constants('auth').reducers.editUserDetails.load:
            return {
                ...state,
                loading: true
            }
        case constants('auth').reducers.editUserDetails.success:
            return {
                ...state,
                loading: false
            }
        case constants('auth').reducers.editUserDetails.error:
            return {
                ...state,
                loading: false
            }
        case constants('auth').reducers.editUserDetails.unload:
            return {
                ...state,
                loading: false
            }
        case constants('auth').reducers.getProfile.load:
            return {
                ...state,
                loading: true
            }
        case constants('auth').reducers.getProfile.success:
            return {
                ...state,
                loading: false,
                user: {
                    ...state.user,
                    name: action.payload.data.name,
                    phone: action.payload.data.phone
                }
            }
        case constants('auth').reducers.getProfile.error:
            return {
                ...state,
                loading: false
            }
        case constants('auth').reducers.getProfile.unload:
            return {
                ...state,
                loading: false
            }
        case constants('auth').reducers.AddAdmin.load:
            return {
                ...state,
                loading: true
            }
        case constants('auth').reducers.AddAdmin.success:
            return {
                ...state,
                loading: false,
            }
        case constants('auth').reducers.AddAdmin.error:
            return {
                ...state,
                loading: false
            }
        case constants('auth').reducers.AddAdmin.unload:
            return {
                ...state,
                loading: false
            }
        case constants('auth').reducers.getAdminList.load:
            return {
                ...state,
                loading: true
            }
        case constants('auth').reducers.getAdminList.success:
            let oldCatData1 = [...state.getAdminList];
            let newCatData1 = []
            if (action.payload.data) {
                if (action.payload.data.length > 0) {
                    if (oldCatData1.length > 0) {
                        newCatData1 = [...oldCatData1, ...action.payload.data]
                    } else {
                        newCatData1 = action.payload.data
                    }
                } else {
                    newCatData1 = [...state.getAdminList]
                }
            }
            return {
                ...state,
                loading: false,
                success: true,
                getAdminList: newCatData1
            }
        case constants('auth').reducers.getAdminList.error:
            return {
                ...state,
                loading: false
            }
        case constants('auth').reducers.getAdminList.unload:
            return {
                ...state,
                loading: false
            }
        case constants('auth').reducers.getAdminList.set:
            return {
                ...state,
                loading: false,
                getAdminList: []
            }
        default:
            return state
    }
}
var _Environments = {
    staging: {
        env: "test",
        USER_URL:'https://fleet.client.zivost.com/',
        LOGS: false
    },
    production: {
        env: "production",
        USER_URL: 'https://fleet.client.zivost.com/',
        LOGS: true
    },
    development: {
        env: "development",
        USER_URL: 'https://fleet.client.zivost.com/',
        LOGS: true
    },
}

function getEnvironment() {
    var env = "development";
    return _Environments[env]
}

var Environment = getEnvironment()
module.exports = Environment